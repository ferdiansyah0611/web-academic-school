<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * System aplikasi school management
 *
 * @author Ferdiansyah
 * @link https://github.com/ferdiansyah0611/Website-Manajemen-Sekolahan
 * @copyright Copyright (c) 2014-2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license http://opensource.org/licenses/MIT MIT License
 * @link https://codeigniter.com
 */
class Profile extends CI_Controller{
	/**
    * Construct Class Profile
    */
	function __construct()
	{
		parent::__construct();
		$this->load->model('allprogress');
		$this->load->library('form_validation');
		$this->post = $this->input->post();
		$this->timestamp = date("Y:m:d h:i:s");
		$this->random = rand(1000000,10000000);
		$this->layout = $this->db->where(array('user_id'=>$this->session->userdata("id")))->from('layout')->get()->result();
		$this->lang->load('menu', $this->session->userdata('languange'));
		$this->jquery = '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>';
	}
	function dashboard()
	{
		$data['title'] = 'Dashboard';
		$data['users'] = $this->layout;
		$data['jquery'] = $this->jquery;
		if($this->session->userdata('role') == 'admin')
		{
			$data['count_mahasiswa'] = $this->db->where(array('role'=>'mahasiswa'))->from('users')->get()->num_rows();
			$data['count_dosen'] = $this->db->where(array('role'=>'dosen'))->from('users')->get()->num_rows();
			$data['count_class'] = $this->db->from('class')->get()->num_rows();
			$this->load->view('layout/header', $data);
			$this->load->view('page/dashboard', $data);
			$this->load->view('layout/footer');
		}
	}
	/**
    * Setting data account with uploaded avatar
    * @return page/setting/setting
    */
	function setting()
	{
		//Validation
		$this->form_validation->set_rules('name', 'Nama', 'required|min_length[5]');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('born', 'Tanggal Lahir', 'required');
		$this->form_validation->set_rules('nisnik', 'NIS/NIK/NIP', 'required');
		//Method Request Post
		if($this->form_validation->run() == true)
		{
			//Array Users to update
			$users = array(
				'name' => $this->post['name'],
				'email' => $this->post['email'],
				'born' => $this->post['born'],
				'nisnik' => $this->post['nisnik'],
				'locate' => $this->post['locate'],
				'updated_at' => $this->timestamp,
			);
			//Insert users
			$this->db->where('id',$this->session->userdata("id"))->update('users', $users);
			//Validation Upload
			$config['upload_path'] = './storage/img';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['file_name'] = 'avatar-'.rand(1000000,10000000);
			$config['overwrite'] = true;
			$config['max_size'] = 10000;
			//Loaded Library
			$this->load->library('upload', $config);
			//Mengupload File dengan library
			if($this->upload->do_upload('image'))
			{
				//Get data user
				$data = $this->db->where(array('id'=>$this->session->userdata("id")))
				->from('users')->get()->result();
				//Looping to get data user in variable data
				foreach($data as $get)
				{
					//if avatar user not null
					if($get->avatar !== null)
					{
						$file = 'storage/img/'.$get->avatar;
						if (is_readable($file) && unlink($file)) {
							//process uploading
							$this->upload->data();
							//Update data user
							$this->db->where('id',$this->session->userdata("id"))->update('users', array('avatar' => $this->upload->data()['file_name']));
							/**
							* @return page/setting/setting
							**/
							
							$data['title'] = 'Setting account';
							$data['users'] = $this->layout;
							$data['success_form'] = 'Berhasil diubah';
							$this->load->view('layout/header', $data);
							$this->load->view('page/setting/setting', $data);
							$this->load->view('layout/footer');
						}
						//If file not founds in directory
						else {
							/**
							* @return page/setting/setting
							**/
						    
						    $data['error_form'] = "The file was not found or not readable and could not be deleted";
							$data['title'] = 'Setting account';
							$data['users'] = $this->layout;
							$this->load->view('layout/header', $data);
							$this->load->view('page/setting/setting', $data);
							$this->load->view('layout/footer');
						}
					}
					//If avatar null|undefined|not have value
					else{
						/**
						* @return page/setting/setting
						**/
						$this->upload->data();
						$this->db->where('id',$this->session->userdata("id"))->update('users', array('avatar' => $this->upload->data()['file_name']));
						
						$data['users'] = $this->layout;
						$data['title'] = 'Setting account';
						$data['success_form'] = 'Berhasil diubah';
						$this->load->view('layout/header', $data);
						$this->load->view('page/setting/setting', $data);
						$this->load->view('layout/footer');
					}
				}
				
			}
		}
		//Method Request Get
		else{
			/**
			* @return page/setting/setting
			**/
			
			$data['title'] = 'Setting account';
			$data['users'] = $this->layout;
			$this->load->view('layout/header', $data);
			$this->load->view('page/setting/setting');
			$this->load->view('layout/footer');
		}
	}
	function layout_nav()
	{
		$data = $this->db->where('user_id', $this->session->userdata("id"))->from('layout')->get();
		if($data->result() == false)
		{
			$this->allprogress->insdata('layout', array(
				'id' => $this->random,
				'user_id' => $this->session->userdata("id"),
				'nav' => $this->post['nav'],
				'created_at' => $this->timestamp,
				'updated_at' => $this->timestamp,
			), 'success');
		}
		elseif($data->result() == true)
		{
			$this->allprogress->upddata('user_id',$this->session->userdata("id"),'layout',array('nav'=>$this->post['nav']),'Success');
		}
	}
	function layout_sidebar()
	{
		$data = $this->db->where('user_id', $this->session->userdata("id"))->from('layout')->get();
		$user = $this->db->where(array('id', $this->session->userdata("id")))->from('users')->get();
		if($data->result() == false)
		{
			$this->allprogress->insdata('layout', array(
				'id' => $this->random,
				'user_id' => $this->session->userdata("id"),
				'sidebar' => $this->post['sidebar'],
				'created_at' => $this->timestamp,
				'updated_at' => $this->timestamp,
			), 'success');
		}
		elseif($data->result() == true)
		{
			$this->allprogress->upddata('user_id', $this->session->userdata("id"),'layout',array('sidebar' => $this->post['sidebar']), 'Success');
		}
	}
}