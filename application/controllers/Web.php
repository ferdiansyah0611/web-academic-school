<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * System aplikasi school management
 *
 * @author Ferdiansyah
 * @link https://github.com/ferdiansyah0611/Website-Manajemen-Sekolahan
 * @copyright Copyright (c) 2014-2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license http://opensource.org/licenses/MIT MIT License
 * @link https://codeigniter.com
 */
class Web extends CI_Controller{
	/**
    * Construct Class Web
    */
	function __construct()
	{
		parent::__construct();
		$this->load->library(array('session', 'form_validation'));
		$this->layout = $this->db->where(array('user_id'=>$this->session->userdata("id")))->from('layout')->get()->result();
		$this->load->model('user');
		$this->load->model('allprogress');
		$this->random = rand(1000000,10000000);
		$this->post = $this->input->post();
		$this->timestamp = date("Y:m:d h:i:s");
		$this->lang->load('menu', $this->session->userdata('languange'));
	}
	function config()
	{
		header('Content-Type: text/html');
		$data['title'] = 'Configuration Website';
		$data['users'] = $this->layout;
		$this->load->view('layout/header', $data);
		$this->load->view('page/setting/config');
		$this->load->view('layout/footer');
	}
	function tiny()
	{
		$config['upload_path'] = './storage/img';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['max_size'] = 10000;
        $this->load->library('upload', $config);
        if ( ! $this->upload->do_upload('file')) {
            $this->output->set_header('HTTP/1.0 500 Server Error');
            exit;
        } else {
            $file = $this->upload->data();
            $this->output
                ->set_content_type('application/json', 'utf-8')
                ->set_header('HTTP/1.0 200 OK')
                ->set_output(json_encode(['location' => base_url().'/storage/img/'.$file['file_name']]))
                ->_display();
            exit;
        }
	}
	function menu()
	{
		if($this->post['type'] == 'menu')
		{
			$this->allprogress->insdata('menu', array(
				'id' => $this->random,
				'menu' => $this->post['menu'],
				'type' => $this->post['type'],
				'created_at' => $this->timestamp,
				'updated_at' => $this->timestamp
			), 'Success');
		}
		if($this->post['type'] == 'content')
		{
			$this->allprogress->insdata('menu', array(
				'id' => $this->random,
				'menu' => $this->post['menu'],
				'type' => $this->post['content'],
				'content' => $ths->post['content'],
				'link' => $this->post['link'],
				'created_at' => $this->timestamp,
				'updated_at' => $this->timestamp
			), 'Success');
		}
	}
	function sub_menu()
	{
		if($this->post['type'] == 'menu')
		{
			$this->allprogress->insdata('sub_menu', array(
				'id' => $this->random,
				'menu_id' => $this->post['menu_id'],
				'sub_menu' => $this->post['sub_menu'],
				'type' => $this->post['type'],
				'created_at' => $this->timestamp,
				'updated_at' => $this->timestamp
			), 'Success');
		}
		if($this->post['type'] == 'content')
		{
			$this->allprogress->insdata('sub_menu', array(
				'id' => $this->random,
				'menu_id' => $this->post['menu_id'],
				'sub_menu' => $this->post['sub_menu'],
				'type' => $this->post['content'],
				'content' => $ths->post['content'],
				'link' => $this->post['link'],
				'created_at' => $this->timestamp,
				'updated_at' => $this->timestamp
			), 'Success');
		}
	}
}