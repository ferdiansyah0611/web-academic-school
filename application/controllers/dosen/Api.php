<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * System aplikasi school management
 *
 * @author Ferdiansyah
 * @link https://github.com/ferdiansyah0611/Website-Manajemen-Sekolahan
 * @copyright Copyright (c) 2014-2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license http://opensource.org/licenses/MIT MIT License
 * @link https://codeigniter.com
 */
class Api extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->helper('security');
		$this->load->library(array('session', 'form_validation'));
		$this->load->model(array('user', 'allprogress'));
		$this->random = rand(1000000,10000000);
		$this->post = $this->input->post();
		$this->timestamp = date("Y:m:d h:i:s");
		$this->layout = $this->db->where(array('user_id'=>$this->session->userdata("id")))->from('layout')->get()->result();
		$this->lang->load('menu', $this->session->userdata('languange'));
	}
	function input_pg()
	{
		$array = array(
			'id' => $this->random,
			'title_task' => $this->post['title'],
			'type_task' => $this->post['type_task'],
			'pelajaran' => $this->post['choose_study'],
			'count_pg' => $this->post['count_pg'],
			'score_pg' => $this->post['score_pg'],
			'mahasiswa_id' => $this->post['mahasiswa'],
			'created_at' => $this->timestamp,
			'updated_at' => $this->timestamp,
		);
		$array = $this->security->xss_clean($array);
	}
}