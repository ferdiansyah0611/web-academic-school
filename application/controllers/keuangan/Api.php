<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Api extends CI_Controller{
	function __construct()
	{
		parent::__construct();
		$this->load->library(array('session', 'form_validation', 'email'));
		$this->load->helper('security');
		$this->load->model(array('user', 'allprogress'));
		$this->random = rand(1000000,10000000);
		$this->post = $this->input->post();
		$this->timestamp = date("Y:m:d h:i:s");
	}
	///////////////////////////////TYPE PEMBAYARAN////////////////////////////////////
	function DatatableTypePembayaran()
	{
		$this->load->library('datatables');
        $this->datatables->select('type_pembayaran.id, class.kelas, class.jurusan, type_pembayaran.type, type_pembayaran.price, type_pembayaran.end_date');
        $this->datatables->from('type_pembayaran');
        $this->datatables->join('class','type_pembayaran.class_id = class.id');
        $this->datatables->add_column('action', '<button class="btn btn-info type-pembayaran-edit" data-id="$1" data-class="$2"><i class="fa fa-pencil-square-o "></i></button><button class="btn btn-danger type-pembayaran-delete" data-id="$1"><i class="fa fa-trash"></i></button>', 'id,class_id');
        return print_r($this->datatables->generate());
	}
	function AddTypePembayaran()
	{
		$siswainclass = $this->db->get_where('mahasiswa', array('class_id'=>$this->post['class']));
		/*for ($i=0; $i < $siswainclass->num_rows(); $i++) { */
			foreach ($siswainclass->result() as $key => $va) {
				if($this->post['month'] == date('F'))
				{
					$array = array(
    					'id' => rand(1000000,10000000),
    					'mahasiswa_id' => $va->id,
    					'type_pembayaran_id' => $this->random,
    					'tgl_pembayaran' => '',
    					'price' => '',
    					'month' => date('F'),
    					'status' => 'Belum Lunas',
    					'tahun_ajaran' => date('y'),
    					'created_at' => $this->timestamp,
						'updated_at' => $this->timestamp,
    				);
    				$array = $this->security->xss_clean($array);
					$this->db->insert('pembayaran',$array);
				}
				else{
					$array = array(
    					'id' => rand(1000000,10000000),
    					'mahasiswa_id' => $va->id,
    					'type_pembayaran_id' => $this->random,
    					'tgl_pembayaran' => '',
    					'price' => '',
    					'month' => date('F'),
    					'status' => 'Belum Terlaksana',
    					'tahun_ajaran' => date('y'),
    					'created_at' => $this->timestamp,
						'updated_at' => $this->timestamp,
    				);
    				$array = $this->security->xss_clean($array);
					$this->db->insert('pembayaran',$array);
				}
				
			}
		/*}*/
		$this->form_validation->set_rules('class_id', 'Kelas', 'required');
    	$typepembayaran = array(
    		'id' => $this->random,
    		'class_id' => $this->post['class'],
    		'type' => $this->post['type'],
    		'price' => $this->post['price'],
    		'end_date' => $this->post['end_date'],
    		'created_at' => $this->timestamp,
			'updated_at' => $this->timestamp,
    	);
    	$typepembayaran = $this->security->xss_clean($typepembayaran);
    	$this->allprogress->insdata('type_pembayaran', $typepembayaran, 'success add');
	}
	function UpdateTypePembayaran()
	{
		$this->form_validation->set_rules('class', 'Kelas', 'required');
    	$typepembayaran = array(
    		'class_id' => $this->post['class'],
    		'type' => $this->post['type'],
    		'price' => $this->post['price'],
    		'event' => $this->post['event'],
    		'dated_event' => $this->post['dated_event'],
    		'tahun_ajaran' => $this->post['tahun'],
			'updated_at' => $this->timestamp,
    	);
    	$typepembayaran = $this->security->xss_clean($typepembayaran);
    	$this->allprogress->upddata('id', $this->post['id'],'type_pembayaran', $typepembayaran, 'success add');
	}
	function DeleteTypePembayaran()
	{
		$this->db->where(array('type_pembayaran_id'=>$this->post['data']))->delete('pembayaran');
		$this->allprogress->deldata('id', $this->post['data'], 'type_pembayaran', 'success');
	}
	///////////////////////////////END TYPE PEMBAYARAN////////////////////////////////////
	///////////////////////////////TYPE PEMBAYARAN////////////////////////////////////
	function DatatablePembayaran()
	{
		$this->load->library('datatables');
        $this->datatables->select('pembayaran.id as id, mahasiswa.class_id,users.name, type_pembayaran.type, type_pembayaran.price as total, pembayaran.price, pembayaran.tgl_pembayaran, pembayaran.status, type_pembayaran.end_date');
        $this->datatables->from('pembayaran');
        $this->datatables->join('type_pembayaran','pembayaran.type_pembayaran_id = type_pembayaran.id');
        $this->datatables->join('mahasiswa','pembayaran.mahasiswa_id = mahasiswa.id');
        $this->datatables->join('users','mahasiswa.user_id = users.id');
        $this->datatables->add_column('action', '<button class="btn btn-info pembayaran-edit" data-id="$1"><i class="fa fa-pencil-square-o "></i></button><button class="btn btn-danger pembayaran-delete" data-id="$1"><i class="fa fa-trash"></i></button>', 'id');
        return print_r($this->datatables->generate());
	}
	function AddPembayaran()
	{
		$this->form_validation->set_rules('mahasiswa', 'Mahasiswa', 'required');
		$this->form_validation->set_rules('pembayaran', 'Pembayaran', 'required');
		$this->form_validation->set_rules('tgl_payment', 'Tanggal Pembayaran', 'required');
		$this->form_validation->set_rules('price', 'Harga', 'required');
		$this->form_validation->set_rules('month', 'Bulan', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('tahun', 'Tahun Ajaran', 'required');
    	$typepembayaran = array(
    		'id' => $this->random,
    		'mahasiswa_id' => $this->post['mahasiswa'],
    		'type_pembayaran_id' => $this->post['pembayaran'],
    		'tgl_pembayaran' => $this->post['tgl_payment'],
    		'price' => $this->post['price'],
    		'month' => $this->post['month'],
    		'status' => $this->post['status'],
    		'tahun_ajaran' => $this->post['tahun'],
    		'created_at' => $this->timestamp,
			'updated_at' => $this->timestamp,
    	);
    	$typepembayaran = $this->security->xss_clean($typepembayaran);
    	$this->allprogress->insdata('pembayaran', $typepembayaran, 'success add');
	}
	function UpdatePembayaran()
	{
		$this->form_validation->set_rules('mahasiswa', 'Mahasiswa', 'required');
		$this->form_validation->set_rules('pembayaran', 'Pembayaran', 'required');
		$this->form_validation->set_rules('tgl_payment', 'Tanggal Pembayaran', 'required');
		$this->form_validation->set_rules('price', 'Harga', 'required');
		$this->form_validation->set_rules('month', 'Bulan', 'required');
		$this->form_validation->set_rules('status', 'Status', 'required');
		$this->form_validation->set_rules('tahun', 'Tahun Ajaran', 'required');
    	$pembayaran = array(
    		'mahasiswa_id' => $this->post['mahasiswa'],
    		'type_pembayaran_id' => $this->post['pembayaran'],
    		'tgl_pembayaran' => $this->post['tgl_payment'],
    		'price' => $this->post['price'],
    		'month' => $this->post['month'],
    		'status' => $this->post['status'],
    		'tahun_ajaran' => $this->post['tahun'],
    		'created_at' => $this->timestamp,
			'updated_at' => $this->timestamp,
    	);
    	$pembayaran = $this->security->xss_clean($pembayaran);
    	$this->allprogress->upddata('id', $this->post['id'],'pembayaran', $pembayaran, 'success add');
	}
	function DeletePembayaran()
	{
		$this->allprogress->deldata('id', $this->post['data'], 'pembayaran', 'success');
	}
	///////////////////////////////END TYPE PEMBAYARAN////////////////////////////////////
	/*
	tabel class - -- -- - 
	click view => show data mahasiswa berdasarkan kelas
	data where mahasiswa.id = 
	*/
	function DatatableTagihan()
	{
		$this->load->library('datatables');
        $this->datatables->select('id, kelas, jurusan');
        $this->datatables->from('class');
        $this->datatables->add_column('action', '<button class="btn btn-primary  view-mahasiswa mr-1" data-id="$1" data-class="$2 $3">Lihat Tagihan</button>', 'id,kelas,jurusan');
        /**
        * @return json
        **/
        return print_r($this->datatables->generate());
	}
}