<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * System aplikasi school management
 *
 * @author Ferdiansyah
 * @link https://github.com/ferdiansyah0611/Website-Manajemen-Sekolahan
 * @copyright Copyright (c) 2014-2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license http://opensource.org/licenses/MIT MIT License
 * @link https://codeigniter.com
 */
class Api extends CI_Controller{
	function userid($id)
	{
		header('Content-Type: application/json');
    	$get = $this->db->get_where('users', array('id'=>$id))->result();
    		print_r(json_encode($get));
    		//redirect(base_url('404'));
	}
    /*DATA WITH ID*/
	function classid($id)
	{
		header('Content-Type: application/json');
    	$get = $this->db->get_where('class', array('id'=>$id))->result();
    	//MIDDLEWARE AUTENTIKASI USER
    	if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
    	{
    		print_r(json_encode($get));
    	}
    	else{
    		redirect(base_url('404'));
    	}
	}
    function dosenid($id)
    {
        header('Content-Type: application/json');
        $get = $this->db->get_where('dosen', array('id'=>$id))->result();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($get));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function mahasiswaid($id)
    {
        header('Content-Type: application/json');
        $get = $this->db->get_where('mahasiswa', array('id'=>$id))->result();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
    $color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
            print_r(json_encode($get));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function mahasiswauserid($id)
    {
        header('Content-Type: application/json');
        $get = $this->db->get_where('mahasiswa', array('user_id'=>$id))->result();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($get));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function mahasiswainclassid($id)
    {
        $data = $this->db->select('users.id, users.name, users.nisnik, users.locate,mahasiswa.asal_sekolah,mahasiswa.no_father,mahasiswa.no_mother,users.active')->where(array('class_id'=>$id))->from('mahasiswa')->join('users','mahasiswa.user_id = users.id')->get();
        print_r(json_encode($data->result()));
    }
    /*function mahasiswainclassidtagihan($id)
    {
        $data = $this->db->select('users.id, users.name, users.nisnik, users.locate,mahasiswa.asal_sekolah,mahasiswa.no_father,mahasiswa.no_mother,users.active, type_pembayaran.price')->where(array('mahasiswa.class_id'=>$id))->from('mahasiswa')->join('users','mahasiswa.user_id = users.id')->join('pembayaran', 'mahasiswa.id = pembayaran.mahasiswa_id')->join('type_pembayaran', 'pembayaran.type_pembayaran_id = type_pembayaran.id')->get();
        print_r(json_encode($data->result()));
    }*/
    function tagihanmahasiswaid($id)
    {
        header('Content-Type: application/json');
        $get = $this->db->select('SUM(type_pembayaran.price) as tagihan')->where(array('pembayaran.mahasiswa_id'=>$id,'pembayaran.status'=>'Belum Lunas'))->from('pembayaran')->join('type_pembayaran','pembayaran.type_pembayaran_id = type_pembayaran.id')->get()->result();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($get));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function pelajaranid($id)
    {
        header('Content-Type: application/json');
        $get = $this->db->get_where('pelajaran', array('id'=>$id))->result();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($get));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function pelajarandosenid($id)
    {
        header('Content-Type: application/json');
        $get = $this->db->select('pelajaran.id as pelajaran_id, class.id as class_id,class.kelas,class.jurusan,pelajaran.pelajaran')->where(array('dosen_id'=>$id))->from('pelajaran')->join('class','pelajaran.class_id = class.id')->get()->result();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'dosen')
        {
            print_r(json_encode($get));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function typepembayaranid($id)
    {
        header('Content-Type: application/json');
        $get = $this->db->get_where('type_pembayaran', array('id'=>$id))->result();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($get));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function typepembayaraninclassid($id)
    {
        $data = $this->db->select('type_pembayaran.id,class.kelas, class.jurusan, type_pembayaran.type, type_pembayaran.price, type_pembayaran.end_date')->where(array('class_id'=>$id))->from('type_pembayaran')->join('class','type_pembayaran.class_id = class.id')->get();
        print_r(json_encode($data->result()));
    }
    function pembayaranid($id)
    {
        header('Content-Type: application/json');
        $get = $this->db->select('pembayaran.id, mahasiswa.id as mahasiswa_id, type_pembayaran.id as type_pembayaran_id, mahasiswa.class_id, users.name, type_pembayaran.type, type_pembayaran.price as total, pembayaran.price, pembayaran.tgl_pembayaran, pembayaran.status, pembayaran.month, pembayaran.tahun_ajaran, type_pembayaran.end_date')
        ->from('pembayaran')
        ->where(array('pembayaran.id'=>$id))
        ->join('type_pembayaran','pembayaran.type_pembayaran_id = type_pembayaran.id')
        ->join('mahasiswa', 'pembayaran.mahasiswa_id = mahasiswa.id')
        ->join('users','mahasiswa.user_id = users.id')
        ->get();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($get->result()));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function pembayaranmahasiswaid($id)
    {
        header('Content-Type: application/json');
        $get = $this->db->select('users.name, type_pembayaran.type, type_pembayaran.price as total, pembayaran.price, pembayaran.tgl_pembayaran, pembayaran.status, type_pembayaran.end_date')
        ->from('pembayaran')
        ->where(array('pembayaran.mahasiswa_id'=>$id))
        ->join('type_pembayaran','pembayaran.type_pembayaran_id = type_pembayaran.id')
        ->join('mahasiswa', 'pembayaran.mahasiswa_id = mahasiswa.id')
        ->join('users','mahasiswa.user_id = users.id')
        ->get();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($get->result()));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function submenuid($id)
    {
        header('Content-Type: application/json');
        $get = $this->db->where(array('menu_id'=>$id))->from('sub_menu')->get()->result();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($get));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function newsmanagementid($id)
    {
        header('Content-Type: application/json');
        $get = $this->db->where(array('id'=>$id))->from('news')->get()->result();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($get));
        }
        else{
            redirect(base_url('404'));
        }
    }
    /*ALL DATA*/
    function allclass()
    {
        header('Content-Type: application/json');
        $this->db->cache_on();
        $this->db->order_by('kelas', 'ASC');
        $get = $this->db->get('class')->result();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($get));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function alldosen()
    {
        header('Content-Type: application/json');
        /*$this->db->cache_on();*/
        /*$this->db->order_by('gelar', 'ASC');*/
        $get = $this->db->select('dosen.id as id, dosen.user_id, dosen.gelar, dosen.universitas, dosen.phone, users.name, users.avatar, users.locate')->from('dosen')->order_by('gelar', 'ASC')->join('users','dosen.user_id = users.id')->get()->result();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($get));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function allmahasiswa()
    {
        header('Content-Type: application/json');
        /*$this->db->cache_on();*/
        $this->db->order_by('class_id', 'ASC');
        $get = $this->db->get('mahasiswa')->result();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($get));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function alltypepembayaran()
    {
        header('Content-Type: application/json');
        /*$this->db->cache_on();*/
        $this->db->order_by('class_id', 'ASC');
        $get = $this->db->get('type_pembayaran')->result();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($get));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function allmenu()
    {
        header('Content-Type: application/json');
        /*$this->db->cache_on();*/
        $get = $this->db->get('menu')->result();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($get));
        }
        else{
            redirect(base_url('404'));
        }
    }
    /*PAGINATE DATA*/
    function paginateclass()
    {
        header('Content-Type: application/json');
        $this->load->library('pagination');
        $this->load->model('paginate');
        $config['base_url'] = base_url().'api/class/page/';
        $config['total_rows'] = $this->db->get('class')->num_rows();
        $config['per_page'] = 10;
        $from = $this->uri->segment(3);
        $this->pagination->initialize($config);     
        $data['user'] = $this->paginate->get('class', $config['per_page'],$from);
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($data));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function paginatenews()
    {
        header('Content-Type: application/json');
        $this->load->library('pagination');
        $this->load->model('paginate');
        $config['base_url'] = base_url().'test/';
        $config['total_rows'] = $this->db->get('news')->num_rows();
        $config['per_page'] = 10;
        $from = $this->uri->segment(3);
        $this->pagination->initialize($config);     
        $data['news'] = $this->paginate->get('news', $config['per_page'],$from);
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik' || $this->session->userdata("role") == 'keuangan' || $this->session->userdata("role") == 'dosen' || $this->session->userdata("role") == 'mahasiswa' || $this->session->userdata("role") == 'pengunjung')
        {
            print_r(json_encode($data));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function paginatelatestmahasiswa()
    {
        header('Content-Type: application/json');
        $data = $this->db->limit('20')->select('mahasiswa.id as id, users.id as user_id, users.active, users.name, users.nisnik, users.avatar')->order_by('active', 'DESC')->from('mahasiswa')->join('users', 'mahasiswa.user_id = users.id')->get()->result();
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik' || $this->session->userdata("role") == 'keuangan' || $this->session->userdata("role") == 'dosen' || $this->session->userdata("role") == 'mahasiswa' || $this->session->userdata("role") == 'pengunjung')
        {
            print_r(json_encode($data));
        }
        else{
            redirect(base_url('404'));
        }
    }
    function paginatenewsmanagement()
    {
        header('Content-Type: application/json');
        $this->load->library('pagination');
        $this->load->model('paginate');
        $config['base_url'] = base_url().'api/news-management/page/';
        $config['total_rows'] = $this->db->get('news')->num_rows();
        $config['per_page'] = 20;
        $from = $this->uri->segment(3);
        $this->pagination->initialize($config);     
        $data['news'] = $this->paginate->get('news', $config['per_page'],$from);
        //MIDDLEWARE AUTENTIKASI USER
        if($this->session->userdata("role") == 'admin' || $this->session->userdata("role") == 'akademik')
        {
            print_r(json_encode($data));
        }
        else{
            redirect(base_url('404'));
        }
    }
}