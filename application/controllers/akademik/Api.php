<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * System aplikasi school management
 *
 * @author Ferdiansyah
 * @link https://github.com/ferdiansyah0611/Website-Manajemen-Sekolahan
 * @copyright Copyright (c) 2014-2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license http://opensource.org/licenses/MIT MIT License
 * @link https://codeigniter.com
 */
class Api extends CI_Controller{
    /**
    * Construct Class Api
    */
	function __construct()
	{
		parent::__construct();
        $this->load->helper('security');
		$this->load->library(array('session', 'form_validation', 'email'));
		$this->load->model(array('user','allprogress'));
		$this->random = rand(1000000,10000000);
		$this->post = $this->input->post();
		$this->timestamp = date("Y:m:d h:i:s");
	}
	/**
    * Datatable Mahasiswa
    * @return select = id, avatar, name, asal_sekolah, nisnik, phone, active
    **/
    function DatatableMahasiswa()
	{
		$this->load->library('datatables');
        $this->datatables->select('users.id as id, users.avatar, users.name, mahasiswa.asal_sekolah,users.nisnik, mahasiswa.phone, users.active');
        $this->datatables->from('users');
        $this->datatables->where('role','mahasiswa');
        $this->datatables->join('mahasiswa', 'users.id = mahasiswa.user_id');
        $this->datatables->add_column('action', '<button class="btn btn-info mahasiswa-edit" data-id="$1" data-name="$2"><i class="fa fa-pencil-square-o "></i></button><button class="btn btn-danger mahasiswa-delete" data-id="$1"><i class="fa fa-trash"></i></button>', 'id');
        return print_r($this->datatables->generate());
	}
    /**
    * Create Data Mahasiswa with tabel users
    * Use Model Allprogress, library form_validation, timestamp
    **/
	function AddMahasiswa()
    {
        //Validation
    	$this->form_validation->set_rules('name', 'Name', 'required');  
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[15]');
		$this->form_validation->set_rules('confirmed_password', 'Password Confirmation', 'required|matches[password]');
		$this->form_validation->set_rules('locate', 'Locate', 'required');
		$this->form_validation->set_rules('nisnik', 'Nisnik', 'required|is_unique[users.nisnik]');
		//Array Users to insert
        $users = array(
			'id' => $this->random,
			'name' => $this->post['name'],
			'email' => $this->post['email'],
			'password' => password_hash($this->post['password'], PASSWORD_DEFAULT),
			'locate' => $this->post['locate'],
			'nisnik' => $this->post['nisnik'],
			'born' => $this->post['born'],
			'role' => 'mahasiswa',
			'active'=> $this->timestamp,
			'created_at' => $this->timestamp,
			'updated_at' => $this->timestamp,
		);
        //Array Mahasiswa to insert
		$mahasiswa = array(
			'id' => $this->random,
			'user_id' => $this->random,
			'class_id' => $this->post['kelas'],
			'asal_sekolah' => $this->post['asal_sekolah'],
    		'phone' => $this->post['phone'],
    		'father' => $this->post['father'],
    		'mother' => $this->post['mother'],
    		'no_father' => $this->post['phone_father'],
    		'no_mother' => $this->post['phone_mother'],
    		'updated_at' => $this->timestamp,
    		'created_at' => $this->timestamp,
		);
        $users = $this->security->xss_clean($users);
        $mahasiswa = $this->security->xss_clean($mahasiswa);
        //Insert mahasiswa
		$this->db->insert('mahasiswa', $mahasiswa);
		return $this->allprogress->insdata('users', $users, 'Successfully add new data');
    }
    /**
    * Update Data Mahasiswa with user_id
    * Use Model Allprogress, library form_validation, timestamp
    * @return Allprogress uppdate
    **/
    function UpdateMahasiswa()
    {
        //Validation
    	$this->form_validation->set_rules('name', 'Name', 'required');
    	$this->form_validation->set_rules('email', 'Email', 'required');
    	$this->form_validation->set_rules('nisnik', 'Nisnik', 'required');
    	$this->form_validation->set_rules('born', 'Born', 'required');
        //Array Users to update
    	$users = array(
    		'name' => $this->post['name'],
    		'email' => $this->post['email'],
    		'nisnik' => $this->post['nisnik'],
    		'born' => $this->post['born'],
    		'updated_at' => $this->timestamp,
    	);
       //Array Mahasiswa to update 
    	$mahasiswa = array(
    		'asal_sekolah' => $this->post['asal_sekolah'],
    		'class_id' => $this->post['kelas'],
    		'phone' => $this->post['phone'],
    		'father' => $this->post['father'],
    		'mother' => $this->post['mother'],
    		'no_father' => $this->post['phone_father'],
    		'no_mother' => $this->post['phone_mother'],
    		'updated_at' => $this->timestamp,
    	);
        $users = $this->security->xss_clean($users);
        $mahasiswa = $this->security->xss_clean($mahasiswa);
        //Update Mahasiswa
    	$this->db->where('user_id', $this->post['id']);
    	$this->db->update('mahasiswa', $mahasiswa);
    	return $this->allprogress->upddata('id', $this->post['id'], 'users', $users, 'Successfully update data');
    }
    /**
    * Delete data mahasiswa in tabel mahasiswa and tabel users
    * Use Model Allprogress
    **/
    function DeleteMahasiswa()
    {
    	$this->db->where('id',$this->input->post('data'))->delete('mahasiswa');
    	return $this->allprogress->deldata('id', $this->input->post('data'), 'users', 'Data successfully delete');
    }
    /**
    * Datatable Class
    * @button lihat mahasiswa|edit|delete
    * @return select = id, kelas, jurusan
    **/
    function DatatableClass()
	{
		$this->load->library('datatables');
        $this->datatables->select('id, kelas, jurusan');
        $this->datatables->from('class');
        $this->datatables->add_column('action', '<button class="btn btn-primary  view-mahasiswa mr-1" data-id="$1" data-class="$2 $3">Lihat Mahasiswa</button><button class="btn btn-info class-edit" data-id="$1" data-class="$2 $3"><i class="fa fa-pencil-square-o "></i></button><button class="btn btn-danger class-delete ml-1" data-id="$1"><i class="fa fa-trash"></i></button>', 'id,kelas,jurusan');
        /**
        * @return json
        **/
        return print_r($this->datatables->generate());
	}
    /**
    * Create data class with tabel class
    * Use Model Allprogress, library form_validation, timestamp
    * @return allprogress insdata
    **/
    function AddClass()
    {
        //Validation
    	$this->form_validation->set_rules('kelas', 'Kelas', 'required|trim');
    	$this->form_validation->set_rules('jurusan', 'Jurusan', 'required|trim');
    	//Array class to insert
        $array = array(
    		'id' => $this->random,
    		'kelas' => $this->post['kelas'],
    		'jurusan' => $this->post['jurusan'],
    		'created_at' => $this->timestamp,
    		'updated_at' => $this->timestamp,
    	);
    	return $this->allprogress->insdata('class', $array, 'Successfully add new data');
    }
    /**
    * Update data class with tabel class
    * Use Model Allprogress, library form_validation, timestamp
    * @return allprogress upddate
    **/
    function UpdateClass()
    {
        //Array Class to insert
    	$array = array(
    		'kelas' => $this->post['kelas'],
    		'jurusan' => $this->post['jurusan'],
    		'updated_at' => $this->timestamp,
    	);
        $array = $this->security->xss_clean($array);
    	return $this->allprogress->upddata('id', $this->post['id'], 'class', $array, 'Successfully update data');
    }
    /**
    * Delete data class
    * @return allprogress deldata
    **/
    function DeleteClass()
    {
    	return $this->allprogress->deldata('id', $this->post['data'], 'class', 'Successfully delete data');
    }
    /**
    * Datatable Dosen
    * @button edit|delete
    * @return select = id, name, gelar, phone, universitas
    **/
    function DatatableDosen()
	{
		$this->load->library('datatables');
        $this->datatables->select('users.id as id, users.name, dosen.gelar, dosen.phone, dosen.universitas');
        $this->datatables->from('dosen');
        $this->datatables->join('users','dosen.user_id = users.id');
        $this->datatables->add_column('action', '<button class="btn btn-info dosen-edit" data-id="$1"><i class="fa fa-pencil-square-o "></i></button><button class="btn btn-danger dosen-delete" data-id="$1"><i class="fa fa-trash"></i></button>', 'id');
        return print_r($this->datatables->generate());
	}
    /**
    * Create data dosen with tabel dosen and tabel users
    * Use Model Allprogress, library form_validation, timestamp
    * @return allprogress insdata2
    **/
    function AddDosen()
    {
        //Validation
    	$this->form_validation->set_rules('name', 'Name', 'required');
    	$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
    	$this->form_validation->set_rules('password', 'Password', 'required|min_length[10]|max_length[20]');
    	$this->form_validation->set_rules('locate', 'Loocate', 'required');
    	$this->form_validation->set_rules('nisnik', 'NIS/NIK/NIP', 'required');
    	$this->form_validation->set_rules('born', 'Born', 'required');
    	$this->form_validation->set_rules('gelar', 'Gelar', 'required');
    	$this->form_validation->set_rules('universitas', 'Universitas', 'required');
    	$this->form_validation->set_rules('phone', 'Phone', 'required');
    	//Array Users to insert
        $users = array(
			'id' => $this->random,
			'name' => $this->post['name'],
			'email' => $this->post['email'],
			'password' => password_hash($this->post['password'], PASSWORD_DEFAULT),
			'locate' => $this->post['locate'],
			'nisnik' => $this->post['nisnik'],
			'born' => $this->post['born'],
			'role' => 'dosen',
			'avatar' => 'user-default.png',
			'active'=> $this->timestamp,
			'created_at' => $this->timestamp,
			'updated_at' => $this->timestamp,
		);
        //Array Dosen to insert
		$dosen = array(
			'id' => $this->random,
			'user_id' => $this->random,
			'gelar' => $this->post['gelar'],
    		'universitas' => $this->post['universitas'],
    		'phone' => $this->post['phone'],
    		'updated_at' => $this->timestamp,
    		'created_at' => $this->timestamp,
		);
        $users = $this->security->xss_clean($users);
        $dosen = $this->security->xss_clean($dosen);
    	return $this->allprogress->insdata2('users', $users, 'dosen', $dosen, 'Successfully add new data');
    }
    /**
    * Update data dosen with tabel dosen and tabel users
    * Use Model Allprogress, library form_validation, timestamp
    * @return allprogress upddate2
    **/
    function UpdateDosen()
    {
        //Validation
    	$this->form_validation->set_rules('name', 'Name', 'required');
    	$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
    	$this->form_validation->set_rules('password', 'Password', 'required|min_length[10]|max_length[20]');
    	$this->form_validation->set_rules('locate', 'Loocate', 'required');
    	$this->form_validation->set_rules('nisnik', 'NIS/NIK/NIP', 'required');
    	$this->form_validation->set_rules('born', 'Born', 'required');
    	$this->form_validation->set_rules('gelar', 'Gelar', 'required');
    	$this->form_validation->set_rules('universitas', 'Universitas', 'required');
    	$this->form_validation->set_rules('phone', 'Phone', 'required');
    	//Array Users to update
        $users = array(
			'name' => $this->post['name'],
			'email' => $this->post['email'],
			'locate' => $this->post['locate'],
			'nisnik' => $this->post['nisnik'],
			'born' => $this->post['born'],
			'updated_at' => $this->timestamp,
		);
        //Array Dosen to updaye
		$dosen = array(
			'gelar' => $this->post['gelar'],
    		'universitas' => $this->post['universitas'],
    		'phone' => $this->post['phone'],
    		'updated_at' => $this->timestamp,
		);
        $users = $this->security->xss_clean($users);
        $dosen = $this->security->xss_clean($dosen);
    	return $this->allprogress->upddata2('user_id', $this->post['id'], 'dosen', $dosen,'id',$this->post['id'], 'users',$users, 'Successfully update data');
    }
    /**
    * Delete data dosen
    * @return allprogress deldata2
    **/
    function DeleteDosen()
    {
    	return $this->allprogress->deldata2('user_id', $this->post['data'], 'dosen', 'id', $this->post['data'], 'users','Successfully delete data');
    }
    /**
    * Datatable Pelajaran
    * @button edit|delete
    * @return select = id, pelajaran, hari, mulai, akhir
    **/
    function DatatablePelajaran()
	{
		$this->load->library('datatables');
        $this->datatables->select('pelajaran.id as id, class.id as class_id, class.kelas, class.jurusan, pelajaran.pelajaran,pelajaran.hari,pelajaran.mulai,pelajaran.akhir, users.name');
        $this->datatables->from('pelajaran');
        $this->datatables->join('class', 'pelajaran.class_id = class.id');
        $this->datatables->join('dosen', 'pelajaran.dosen_id = dosen.id');
        $this->datatables->join('users', 'dosen.user_id = users.id');
        $this->datatables->add_column('action', '<button class="btn btn-info pelajaran-edit" data-id="$1"><i class="fa fa-pencil-square-o "></i></button><button class="btn btn-danger pelajaran-delete" data-id="$1"><i class="fa fa-trash"></i></button>', 'id');
        return print_r($this->datatables->generate());
	}
    /**
    * Create data pelajaran
    * Use Model Allprogress, library form_validation, timestamp
    * @return allprogress insdata
    **/
    function AddPelajaran()
    {
        //Validation
    	$this->form_validation->set_rules('class_id', 'Class', 'required');
    	$this->form_validation->set_rules('pelajaran', 'Pelajaran', 'required');
    	$this->form_validation->set_rules('hari', 'Day', 'required');
    	$this->form_validation->set_rules('mulai', 'Start Time', 'required');
    	$this->form_validation->set_rules('akhir', 'End Time', 'required');
    	//Array Pelajaran to insert
        $pelajaran = array(
			'id' => $this->random,
			'class_id' => $this->post['kelas'],
            'dosen_id' => $this->post['dosen'],
			'pelajaran' => $this->post['pelajaran'],
			'hari' => $this->post['hari'],
			'mulai' => $this->post['start_hour'].'.'.$this->post['start_minute'],
			'akhir' => $this->post['end_hour'].'.'.$this->post['end_minute'],
			'created_at' => $this->timestamp,
			'updated_at' => $this->timestamp,
		);
        $pelajaran = $this->security->xss_clean($pelajaran);
    	return $this->allprogress->insdata('pelajaran', $pelajaran, 'Successfully add new data');
    }
    /**
    * Update data pelajaran
    * Use Model Allprogress, library form_validation, timestamp
    * @return allprogress upddate
    **/
    function UpdatePelajaran()
    {
        //Validation
    	$this->form_validation->set_rules('kelas', 'Class', 'required');
    	$this->form_validation->set_rules('pelajaran', 'Pelajaran', 'required');
    	$this->form_validation->set_rules('hari', 'Day', 'required');
    	$this->form_validation->set_rules('mulai', 'Start Time', 'required');
    	$this->form_validation->set_rules('akhir', 'End Time', 'required');
        //Array Pelajaran to insert    	
        $pelajaran = array(
			'class_id' => $this->post['kelas'],
            'dosen_id' => $this->post['dosen'],
			'pelajaran' => $this->post['pelajaran'],
			'hari' => $this->post['hari'],
			'mulai' => $this->post['mulai'],
			'akhir' => $this->post['akhir'],
			'updated_at' => $this->timestamp,
		);
        $pelajaran = $this->security->xss_clean($pelajaran);
    	return $this->allprogress->upddata('id', $this->post['id'], 'pelajaran', $pelajaran, 'Successfully update data');
    }
    /**
    * Delete data pelajaran
    * @return allprogress deldata
    **/
    function DeletePelajaran()
    {
    	return $this->allprogress->deldata('id', $this->post['data'], 'pelajaran','Successfully delete data');
    }
}