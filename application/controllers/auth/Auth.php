<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * System aplikasi school management
 *
 * @author Ferdiansyah
 * @link https://github.com/ferdiansyah0611/Website-Manajemen-Sekolahan
 * @copyright Copyright (c) 2014-2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license http://opensource.org/licenses/MIT MIT License
 * @link https://codeigniter.com
 */
class Auth extends CI_Controller{
	/**
    * Construct Class Auth
    */
	function __construct()
	{
		parent::__Construct();
		$this->load->helper(array('form', 'security'));
		$this->load->library(array('session', 'form_validation', 'email'));
		$this->load->model('user');
		$this->random = rand(1000000,10000000);
		$this->post = $this->input->post();
		$this->timestamp = date("Y:m:d h:i:s");
	}
	/**
	* Auth Registration
	* @return get|post|redirect
	**/
	function register()
	{
		$this->form_validation->set_rules('name', 'Name', 'required');  
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[10]|max_length[20]');
		$this->form_validation->set_rules('confirmed_password', 'Password Confirmation', 'required|matches[password]');
		$this->form_validation->set_rules('locate', 'Locate', 'required');
		$this->form_validation->set_rules('born', 'Born', 'required');
		$this->form_validation->set_rules('nisnik', 'NIS or NIK', 'required|is_unique[users.nisnik]');
		if($this->form_validation->run() != false)
		{
			$register = array(
				'id' => rand(100000,1000000),
				'name' => $this->post['name'],
				'email' => $this->post['email'],
				'password' => password_hash($this->post['password'], PASSWORD_DEFAULT),
				'locate' => $this->post['locate'],
				'nisnik' => $this->post['nisnik'],
				'born' => $this->post['born'],
				'role' => 'pengunjung',
				'avatar' => 'user-default.png',
				'active' => $this->timestamp,
				'updated_at' => $this->timestamp,
				'created_at' => $this->timestamp,
			);
			$this->session->set_userdata(array(
				'name' => $this->post['name'],
				'email' => $this->post['email'],
				'locate' => $this->post['locate'],
				'nisnik' => $this->post['nisnik'],
				'born' => $this->post['born'],
				'role' => 'pengunjung',
				'password' => password_hash($this->post['password'], PASSWORD_DEFAULT),
				'avatar' => 'user-default.png',
				'active' => $this->timestamp,
				'updated_at' => $this->timestamp,
				'created_at' => $this->timestamp,
			));
			$register = $this->security->xss_clean($register);
			$this->user->register($register);
			echo 'Waiting to redirect url...<script>setTimeout(function(){window.location.href="/"},3000)</script>';
		}
		else{
			$data['title'] = 'Register Account';
			$this->load->view('auth/register', $data);
		}
	}
	function login()
	{
		$this->form_validation->set_rules('nisnik', 'NIS or NIK or NIP', 'required|is_natural');
		$this->form_validation->set_rules('password', 'Password', 'required');
		if($this->form_validation->run() == true)
		{
			$user = $this->user->check($this->input->post('nisnik'), $this->input->post('password'));
			if($user != FALSE)
			{
				foreach($user as $get)
				{
					if($get->role == 'mahasiswa')
					{
						$mahasiswa = $this->db->where(array('user_id' => $get->id))->from('mahasiswa')->get()->result();
						foreach ($mahasiswa as $key => $val)
						{
							$this->session->set_userdata(array(
								'id' => $get->id,
								'name' => $get->name,
								'email' => $get->email,
								'locate' => $get->locate,
								'nisnik' => $get->nisnik,
								'born' => $get->born,
								'password' => $get->password,
								'role' => $get->role,
								'avatar' => $get->avatar,
								'mahasiswa_id' => $val->id,
								'class_id' => $val->class_id,
								'languange' => $get->languange,
								'created_at' => $get->created_at,
								'updated_at' => $get->updated_at,
							));
						}
					}
					if($get->role == 'dosen')
					{
						$dosen = $this->db->where(array('user_id' => $get->id))->from('dosen')->get()->result();
						foreach($dosen as $dosens)
						{
							$this->session->set_userdata(array(
								'id' => $get->id,
								'name' => $get->name,
								'email' => $get->email,
								'locate' => $get->locate,
								'nisnik' => $get->nisnik,
								'born' => $get->born,
								'password' => $get->password,
								'role' => $get->role,
								'avatar' => $get->avatar,
								'languange' => $get->languange,
								'gelar' => $dosens->gelar,
								'universitas' => $dosens->universitas,
								'phone' => $dosens->phone,
								'dosen_id' => $dosens->id,
								'created_at' => $get->created_at,
								'updated_at' => $get->updated_at,
							));
						}
					}
					if($get->role == 'keuangan' || $get->role == 'akademik' || $get->role == 'admin')
					{
						$this->session->set_userdata(array(
							'id' => $get->id,
							'name' => $get->name,
							'email' => $get->email,
							'locate' => $get->locate,
							'languange' => $get->languange,
							'nisnik' => $get->nisnik,
							'born' => $get->born,
							'password' => $get->password,
							'role' => $get->role,
							'avatar' => $get->avatar,
							'created_at' => $get->created_at,
							'updated_at' => $get->updated_at,
						));
					}
					$this->db->where(array('id', $get->id))->update('users', array('active'=> $this->timestamp));
				}
				echo 'Waiting to redirect url...<script>speechSynthesis.speak(new SpeechSynthesisUtterance("Waiting to redirect url")); setTimeout(function(){window.location.href="/"},3000)</script>';
			}
			else{
				$data['title'] = 'Register Account';
				$data['error_form'] =  'Sorry NIS or NIK or NIP or password maybe wrong. Please fill in the data correctly !';
				$this->load->view('auth/login', $data);
			}
		}
		else{
			$data['title'] = 'Login to SMK Letris Indonesia 1';
			$this->load->view('auth/login', $data);
		}
	}
	function request_logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}
}