<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		$this->layout = $this->db->where(array('user_id'=>$this->session->userdata("id")))->from('layout')->get()->result();
		$this->menu = $this->db->from('menu')->get()->result();
	}
	function index()
	{
		if($this->session->userdata("id") == false)
		{
			$data['title'] = 'System School';
			$data['users'] = $this->layout;
			$data['menu'] = $this->menu;
			$this->load->view('test', $data);
		}
		else{
			$this->lang->load('menu', $this->session->userdata('languange'));
			$data['title'] = 'System School';
			$data['users'] = $this->layout;
			$this->load->view('layout/header', $data);
			$this->load->view('page/home');
			$this->load->view('layout/footer');
		}
	}
	function test()
	{
		header('Content-Type: text/html');
		$data['title'] = 'System School';
		$data['users'] = $this->layout;
		$data['menu'] = $this->menu;
		$this->load->view('test', $data);
	}
}
