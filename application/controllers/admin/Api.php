<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * System aplikasi school management
 *
 * @author Ferdiansyah
 * @link https://github.com/ferdiansyah0611/Website-Manajemen-Sekolahan
 * @copyright Copyright (c) 2014-2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license http://opensource.org/licenses/MIT MIT License
 * @link https://codeigniter.com
 */
class Api extends CI_Controller{
    /**
    * Construct Class Api
    */
	function __construct()
	{
		parent::__Construct();
        /*HELPER*/
		$this->load->helper(array('form','security'));
        /*LIBRARY*/
		$this->load->library(array('session', 'form_validation', 'email'));
        /*MODEL*/
		$this->load->model(array('user','allprogress'));
        /*MORE*/
        $this->random = rand(1000000,10000000);
        $this->post = $this->input->post();
        $this->timestamp = date("Y:m:d h:i:s");
	}
    /**
    * Datatable User
    * @return select = id, name, email, locate, nisnik
    **/
	function DatatablesUser()
	{
        $this->load->library('datatables');
        $this->datatables->select('id,name,email,locate,nisnik');
        $this->datatables->from('users');
        $this->datatables->add_column('action', '<button class="btn btn-info user-edit" data-id="$1" data-name="$2"><i class="fa fa-pencil-square-o "></i></button><button class="btn btn-danger user-delete" data-id="$1"><i class="fa fa-trash"></i></button>', 'id');
        return print_r($this->datatables->generate());
    }
    /**
    * Create Data User with tabel users
    * Use Model Allprogress, library form_validation, timestamp
    **/
    function AddUser()
    {	
    	header('Content-Type: application/json');
    	$this->form_validation->set_rules('name', 'Name', 'required');  
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[users.email]');
		$this->form_validation->set_rules('password', 'Password', 'required|min_length[6]|max_length[15]');
		$this->form_validation->set_rules('confirmed_password', 'Password Confirmation', 'required|matches[password]');
		$this->form_validation->set_rules('locate', 'Locate', 'required');
		$this->form_validation->set_rules('nisnik', 'Nisnik', 'required|is_unique[users.nisnik]');
		$array = array(
			'id' => rand(100000,1000000),
			'name' => $this->post['name'],
			'email' => $this->post['email'],
			'password' => password_hash($this->post['password'], PASSWORD_DEFAULT),
			'locate' => $this->post['locate'],
			'nisnik' => $this->post['nisnik'],
			'born' => $this->post['born'],
			'role' => $this->post['role'],
			'updated_at' => $this->timestamp,
			'created_at' => $this->timestamp,
		); 
        $array = $this->security->xss_clean($array);
		$this->user->register($array);
		print_r(json_encode(array('success'=>'Successfully add new data')));
    }
    function UpdateUser()
    {
    	header('Content-Type: application/json');
    	$array = array(
    		'name' => $this->post['name'],
    		'email' => $this->post['email'],
    		'nisnik' => $this->post['nisnik'],
    		'born' => $this->post['born'],
    		'role' => $this->post['role'],
    		'updated_at' => $this->timestamp,
    	);
        $array = $this->security->xss_clean($array);
    	$this->db->where('id',$this->post['id']);
    	$this->db->update('users', $array);
    	print_r(json_encode(array('success'=>'Successfully update data')));
    }
    function DeleteUser()
    {
    	header('Content-Type: application/json');
    	$id = $this->input->post('data');//id
    	$this->db->where('id',$id);
    	$this->db->delete('users');
    	print_r(json_encode(array('success'=>'Data successfully delete')));
    }
    function AddNews()
    {
        //Validation Upload
        $config['upload_path'] = './storage/img';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['file_name'] = 'news-'.rand(1000000,10000000);
        $config['overwrite'] = true;
        $config['max_size'] = 10000;
        $this->load->library('upload', $config);
        if($this->post['status'] == 'publish')
        {
            if($this->upload->do_upload('file'))
            {
                $this->upload->data();
                $data = array(
                    'id' => $this->random,
                    'user_id' => $this->session->userdata("id"),
                    'title' => $this->post['title'],
                    'content' => $this->post['content'],
                    'status' => 'publish',
                    'file' => $this->upload->data()['file_name'],
                    'category' => $this->post['category'],
                    'updated_at' => $this->timestamp,
                    'created_at' => $this->timestamp,
                );
                $data = $this->security->xss_clean($data);
                $this->allprogress->insdata('news',$data,'success');
            }
        }
        else
        {
            if($this->upload->do_upload('file'))
            {
                $this->upload->data();
                $data = array(
                    'id' => $this->random,
                    'user_id' => $this->session->userdata("id"),
                    'title' => $this->post['title'],
                    'content' => $this->post['content'],
                    'status' => 'draft',
                    'file' => $this->upload->data()['file_name'],
                    'category' => $this->post['category'],
                    'updated_at' => $this->timestamp,
                    'created_at' => $this->timestamp,
                );
                $data = $this->security->xss_clean($data);
                $this->allprogress->insdata('news',$data,'success');
            }
        }
    }
    function UpdateNews()
    {
        //Validation Upload
        $config['upload_path'] = './storage/img';
        $config['allowed_types'] = 'jpg|png|jpeg';
        $config['file_name'] = 'news-'.rand(1000000,10000000);
        $config['overwrite'] = true;
        $config['max_size'] = 10000;
        $this->load->library('upload', $config);
        if($this->post['status'] == 'publish')
        {
            if($this->upload->do_upload('file'))
            {
                $this->upload->data();
                $array = array(
                    'title' => $this->post['title'],
                    'content' => $this->post['content'],
                    'status' => 'publish',
                    'file' => $this->upload->data()['file_name'],
                    'category' => $this->post['category'],
                    'updated_at' => $this->timestamp,
                );
                $array = $this->security->xss_clean($array);
                $this->allprogress->upddata('id',$this->post['id'],'news',$array,'success');
            }
            else{
                $array = array(
                    'title' => $this->post['title'],
                    'content' => $this->post['content'],
                    'status' => 'publish',
                    'category' => $this->post['category'],
                    'updated_at' => $this->timestamp,
                );
                $array = $this->security->xss_clean($array);
                $this->allprogress->upddata('id',$this->post['id'],'news',$array,'success');
            }
        }
        else
        {
            if($this->upload->do_upload('file'))
            {
                $this->upload->data();
                $array = array(
                    'id' => $this->random,
                    'user_id' => $this->session->userdata("id"),
                    'title' => $this->post['title'],
                    'content' => $this->post['content'],
                    'status' => 'draft',
                    'file' => $this->upload->data()['file_name'],
                    'category' => $this->post['category'],
                    'updated_at' => $this->timestamp,
                    'created_at' => $this->timestamp,
                );
                $array = $this->security->xss_clean($array);
                $this->allprogress->insdata('news',$array,'success');
            }
        }
    }
}