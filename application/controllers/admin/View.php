<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * System aplikasi school management
 *
 * @author Ferdiansyah
 * @link https://github.com/ferdiansyah0611/Website-Manajemen-Sekolahan
 * @copyright Copyright (c) 2014-2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license http://opensource.org/licenses/MIT MIT License
 * @link https://codeigniter.com
 */
class View extends CI_Controller{
	/**
    * Construct Class Api
    */
	function __construct()
	{
		parent::__construct();
		/*RESOURCES*/
		$this->jquery = '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>';
		$this->js_bootstrap = '<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>';
		$this->tinymce = '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.1/tinymce.min.js"></script>';
		$this->animate = '<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">';
		$this->css_datatables_bs4 = '<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/dataTables.bootstrap4.min.css">';
		$this->css_jquery_datatables = '<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/jquery.dataTables.min.css">';
		$this->js_jquery_datatables = '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/jquery.dataTables.min.js"></script>';
		$this->js_datatables_bs4 = '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/dataTables.bootstrap4.min.js"></script>';
		/*LAYOUTS*/
		$this->layout = $this->db->where(array('user_id'=>$this->session->userdata("id")))->from('layout')->get()->result();
		/*LANGUANGE*/
		$this->lang->load('menu', $this->session->userdata('languange'));
		/*SECURITY*/
		$this->csrf = '<input type="hidden" name="'.$this->security->get_csrf_token_name().'" value="'.$this->security->get_csrf_hash().'">';
		/*CHECK AUTHENTICATE AND MIDDLEWARE*/
		$role = $this->session->userdata("role");
		if($this->session->userdata("id") == false){
			redirect(base_url('login'));
		}
		/*if($role !== 'admin'){
			redirect(base_url('404'));
		}*/
	}
	function manage_users()
	{
		$data['title'] = 'Management Data User';
		$data['users'] = $this->layout;
		$data['csrf'] = $this->csrf;
		$this->load->view('layout/header', $data);
		$this->load->view('page/admin/manage_user', $data);
		$this->load->view('layout/footer');
	}
	function create_news()
	{
		$data['title'] = 'Create News';
		$data['users'] = $this->layout;
		$data['csrf'] = $this->csrf;
		$this->load->view('layout/header', $data);
		$this->load->view('page/admin/create_news', $data);
		$this->load->view('layout/footer');
	}
	function management_news()
	{
		$data['title'] = 'Management News';
		$data['users'] = $this->layout;
		$data['csrf'] = $this->csrf;
		$this->load->view('layout/header', $data);
		$this->load->view('page/admin/manage_news', $data);
		$this->load->view('layout/footer');
	}
	function management_news_id()
	{
		$data['title'] = 'Management News';
		$data['users'] = $this->layout;
		$data['csrf'] = $this->csrf;
		$this->load->view('layout/header', $data);
		$this->load->view('page/admin/manage_news_id', $data);
		$this->load->view('layout/footer');
	}
}