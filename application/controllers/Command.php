<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * System aplikasi school management
 *
 * @author Ferdiansyah
 * @link https://github.com/ferdiansyah0611/Website-Manajemen-Sekolahan
 * @copyright Copyright (c) 2014-2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license http://opensource.org/licenses/MIT MIT License
 * @link https://codeigniter.com
 */
class Command extends CI_Controller{
	function drop()
	{
		$data = $this->db->get('users')->result();
		foreach($data as $d){
			echo ''.$d->id.'|'.$d->name.''.PHP_EOL;
		}
	}
}