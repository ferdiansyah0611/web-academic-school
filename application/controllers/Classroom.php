<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * System aplikasi school management
 *
 * @author Ferdiansyah
 * @link https://github.com/ferdiansyah0611/Website-Manajemen-Sekolahan
 * @copyright Copyright (c) 2014-2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license http://opensource.org/licenses/MIT MIT License
 * @link https://codeigniter.com
 */
class Classroom extends CI_Controller{
	/**
    * Construct Class Api
    */
	function __construct()
	{
		parent::__construct();
		/*RESOURCES*/
		$this->css_materialize = '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">';
		$this->icon_materialize = '<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">';
		$this->js_materialize = 'https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js';
		$this->jquery = '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>';
		$this->js_bootstrap = '<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>';
		$this->tinymce = '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.1/tinymce.min.js"></script>';
		$this->animate = '<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">';
		$this->css_datatables_bs4 = '<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/dataTables.bootstrap4.min.css">';
		$this->css_jquery_datatables = '<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/jquery.dataTables.min.css">';
		$this->js_jquery_datatables = '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/jquery.dataTables.min.js"></script>';
		$this->js_datatables_bs4 = '<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/js/dataTables.bootstrap4.min.js"></script>';
		/*LAYOUTS*/
		$this->layout = $this->db->where(array('user_id'=>$this->session->userdata("id")))->from('layout')->get()->result();
		/*LANGUANGE*/
		$this->lang->load('menu', $this->session->userdata('languange'));
		/*SECURITY*/
		$this->csrf = '<input type="hidden" name="'.$this->security->get_csrf_token_name().'" value="'.$this->security->get_csrf_hash().'">';
		/*HELPER*/
		$this->load->helper(array('form','security'));
        /*LIBRARY*/
		$this->load->library(array('session', 'form_validation', 'email'));
        /*MODEL*/
		$this->load->model(array('user','allprogress'));
        /*MORE*/
        $this->random = rand(1000000,10000000);
        $this->post = $this->input->post();
        $this->timestamp = date("Y:m:d h:i:s");
		/*CHECK AUTHENTICATE AND MIDDLEWARE*/
		$role = $this->session->userdata("role");
		if($this->session->userdata("id") == false){
			redirect(base_url('login'));
		}
		/*if($role !== 'admin'){
			redirect(base_url('404'));
		}*/
	}
	/*FUNCTION VIEW*/
	function indexclass()
	{
		$data['csrf'] = $this->csrf;
		$data['jquery'] = $this->jquery;
		$this->load->view('classroom/class_list_test', $data);
	}
	/*FUNCTION MAHASISWA*/
	/*REQUEST*/
	function addshare()
	{
		$data = [];
      	$count_file = count($_FILES['filedata']['name']);
      	$count_link = count($_POST['link']);
      	for($i=0;$i<$count_file;$i++)
      	{
        	if(!empty($_FILES['filedata']['name'][$i]))
        	{
          		$_FILES['file']['name'] = $_FILES['filedata']['name'][$i];
          		$_FILES['file']['type'] = $_FILES['filedata']['type'][$i];
          		$_FILES['file']['tmp_name'] = $_FILES['filedata']['tmp_name'][$i];
          		$_FILES['file']['error'] = $_FILES['filedata']['error'][$i];
          		$_FILES['file']['size'] = $_FILES['filedata']['size'][$i];
          		$config['upload_path'] = './storage/classroom';
				$config['allowed_types'] = 'jpg|png|jpeg|doc|docx|xls|xlsx|ppt|pptx';
				$config['max_size'] = 25000;
          		$this->load->library('upload',$config); 
          		if($this->upload->do_upload('file'))
          		{
          			$this->upload->data();
            		$upload = $this->upload->data();
            		$name = $upload['file_name'];
            		$data['totalFiles'][] = $name;
            		print_r($this->upload->data());
            		$array = array(
            			'id' => rand(1000000,10000000),
            			'clr_post_id' => $this->random,
            			'name' => $this->upload->data()['file_name'],
            			'type' => $this->upload->data()['file_type'],
            			'size' => $this->upload->data()['file_ext'],
            			'updated_at' => $this->timestamp,
						'created_at' => $this->timestamp,
            		);
            		$array = $this->security->xss_clean($array);
            		$this->db->insert('clr_file', $array);
            		echo 'berhasil';
          		}
        	}
      	}
      	for($i=0;$i<$count_link;$i++)
      	{
      		$array = array(
      			'id' => rand(1000000,10000000),
      			'class_id' => $this->post['class'],
      			'link' => $_POST['link'][$i],
      			'updated_at' => $this->timestamp,
				'created_at' => $this->timestamp,
      		);
      		$array = $this->security->xss_clean($array);
            $this->db->insert('clr_link', $array);
      	}
      	$explode = explode(' ', $this->post['content']);
      	$data = array(
      		'id' => $this->random,
      		'user_id' => $this->session->userdata('id'),
      		'class_id' => $this->post['class'],
      		'title' => $explode[0],
      		'content' => $this->post['content'],
      		'updated_at' => $this->timestamp,
			'created_at' => $this->timestamp,
      	);
      	$data = $this->security->xss_clean($data);
        $this->db->insert('clr_post', $data);
	}
	/////////////////////////////////////////////////////////////////////////
	/*FUNCTION API*/
	function apiclass()
	{
		$data = $this->db->select('pelajaran.id as id, users.id as user_id, pelajaran.id as pelajaran_id, dosen.id as dosen_id, class.id as class_id, class.jurusan, class.kelas, users.name, dosen.gelar, pelajaran.pelajaran, pelajaran.hari,users.nisnik, users.avatar')->where(array('class.id'=>$this->session->userdata('class_id')))->from('class')->join('pelajaran', 'class.id = pelajaran.class_id')->join('dosen', 'pelajaran.dosen_id = dosen.id')->join('users', 'dosen.user_id = users.id')->get()->result();
		echo json_encode($data);
	}
	function apipost($class)
	{
		$data = $this->db->select('*')->where(array('clr_post.class_id'=>$class))->from('clr_post')->join('users','clr_post.user_id = users.id')->get()->result();
		echo json_encode($data);
	}
	function apiclassid($id)
	{
		$data = $this->db->select('*')->from('class')->where(array('pelajaran.id'=>$id))->join('pelajaran', 'class.id = pelajaran.class_id')->get()->result();
		echo json_encode($data);
	}
}