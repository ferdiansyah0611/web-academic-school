<?php
/**
 * System aplikasi school management
 *
 * @author Ferdiansyah
 * @link https://github.com/ferdiansyah0611/Website-Manajemen-Sekolahan
 * @copyright Copyright (c) 2014-2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license http://opensource.org/licenses/MIT MIT License
 * @link https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['my_dashboard']		= 'Dashboard Saya';
$lang['article_news']		= 'Artikel Berita';
$lang['create_content']		= 'Buat Konten';
$lang['role_access_account']= 'Hak Akses Akun';
$lang['management']			= 'Manajemen';
$lang['class']				= 'Kelas';
$lang['study']				= 'Pelajaran';
$lang['finance']			= 'Keuangan';
$lang['payment']			= 'Pembayaran';
$lang['type']				= 'Tipe';
$lang['bill']				= 'Tagihan';
$lang['student']			= 'Siswa';
$lang['teacher']			= 'Guru';
$lang['visitor']			= 'Pengunjung';
$lang['users']				= 'Pengguna';
$lang['subjects']			= 'Mata Pelajaran';
$lang['this_day']			= 'Hari Ini';
$lang['tomorrow']			= 'Besok';
$lang['week']				= 'Seminggu';
$lang['teaching_study']		= 'Jadwal Mengajar';
$lang['student_assessment']	= 'Penilaian Siswa';
$lang['odd_tests']			= 'Ulangan Ganjil';
$lang['semester_1']			= 'Semester 1';
$lang['semester_2']			= 'Semester 2';
$lang['input']				= 'Masukkan';
$lang['score']				= 'Nilai';
$lang['even_test']			= 'Ulangan Genap';
$lang['national_exam']		= 'Ujian Nasional';
$lang['tryout_demo']		= 'Tryout Percobaan';
$lang['more']				= 'Lainnya';
$lang['classroom']			= 'Classroom';
$lang['library']			= 'Perpustakaan';
$lang['config']				= 'Konfigurasi';
$lang['report']				= 'Lapor';
$lang['account'] 			= 'Akun';