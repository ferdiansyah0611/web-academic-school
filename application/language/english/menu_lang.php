<?php
/**
 * System aplikasi school management
 *
 * @author Ferdiansyah
 * @link https://github.com/ferdiansyah0611/Website-Manajemen-Sekolahan
 * @copyright Copyright (c) 2014-2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license http://opensource.org/licenses/MIT MIT License
 * @link https://codeigniter.com
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['my_dashboard']		= 'My Dashboard';
$lang['article_news']		= 'Article News';
$lang['create_content']		= 'Create Content';
$lang['role_access_account']= 'Hak Akses Akun';
$lang['management']		= 'Management';
$lang['class']	= 'Class';
$lang['study']		= 'Study';
$lang['finance']		= 'Finance';
$lang['payment']		= 'Payment';
$lang['type']		= 'Type';
$lang['bill']		= 'Bill';
$lang['student']		= 'Student';
$lang['teacher']		= 'Teacher';
$lang['visitor']		= 'Visitor';
$lang['users']		= 'Users';
$lang['subjects']		= 'Subjects';
$lang['this_day']		= 'This Day';
$lang['tomorrow']		= 'Tommorow';
$lang['week']		= 'Week';
$lang['teaching_study']		= 'Teaching Study';
$lang['student_assessment']		= 'Student Assessment';
$lang['odd_tests']		= 'Odd Tests';
$lang['semester_1']		= 'Semester 1';
$lang['semester_2']		= 'Semester 2';
$lang['input']		= 'Input';
$lang['score']		= 'Score';
$lang['even_test']		= 'Even Test';
$lang['national_exam']		= 'National Exam';
$lang['tryout_demo']		= 'Tryout Demo';
$lang['more']		= 'More';
$lang['classroom']		= 'Classroom';
$lang['library']		= 'Library';
$lang['config']		= 'Config';
$lang['report']		= 'Report';
$lang['account'] 			= 'Akun';