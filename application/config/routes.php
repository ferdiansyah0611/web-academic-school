<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * System aplikasi school management
 *
 * @author Ferdiansyah
 * @link https://github.com/ferdiansyah0611/Website-Manajemen-Sekolahan
 * @copyright Copyright (c) 2014-2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license http://opensource.org/licenses/MIT MIT License
 * @link https://codeigniter.com
 */
$route['dashboard'] = 'profile/dashboard';
$route['test'] = 'welcome/test';
/*START ROUTE WEB*/
$route['default_controller'] = 'welcome';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
///////////////////////////////////////////////////////////////////////////////////
/*ERROR*/
$route['404'] = 'errordata/e404';
///////////////////////////////////////////////////////////////////////////////////
/*AUTENTIKASI*/
$route['register']/*['get']*/ = 'auth/auth/register';
$route['auth/register']['post'] = 'auth/auth/request_register';
$route['login'] = 'auth/auth/login';
$route['auth/login']['post'] = 'auth/auth/request_login';
$route['auth/logout']['get'] = 'auth/auth/request_logout';
/*PAGE ARTICLE*/
$route['news/create'] = 'admin/view/create_news';
$route['news/add'] = 'admin/api/addnews';
$route['news/management'] = 'admin/view/management_news';
$route['news/management/update'] = 'admin/api/UpdateNews';
$route['news/management/data/(:num)'] = 'admin/view/management_news_id';
/*SETUP ACCOUNT USER*/
$route['account/setting'] = 'profile/setting';
$route['account/set/nav'] = 'profile/layout_nav';
$route['account/set/sidebar'] = 'profile/layout_sidebar';
/*SETUP WEB*/
$route['web/setup'] = 'web/config';
$route['tiny/upload'] = 'web/tiny';
$route['web/set/menu'] = 'web/menu';
$route['web/set/sub-menu'] = 'web/sub_menu';
/*ADMIN*/
	/*DATATABLES*/
	$route['admin/datatables-users'] = 'admin/api/datatablesuser';
	/*VIEW*/
	$route['admin/manage-users'] = 'admin/view/manage_users';
	/*REQUEST*/
	$route['admin/user/update']['post'] = 'admin/api/updateuser';
	$route['admin/user/delete']['post'] = 'admin/api/deleteuser';
	$route['admin/user/add']['post'] = 'admin/api/adduser';
///////////////////////////////////////////////////////////////////////////////////
/*DOSEN/GURU = INFORMASI JADWAL MENGAJAR, INPUT NILAI*/
	$route['jadwal-mengajar/(:any)'] = 'dosen/view/pengajarandosenday/$1';
	$route['add-task-day'] = 'dosen/view/task_day';
///////////////////////////////////////////////////////////////////////////////////
/*KEUANGAN = PEMBAYARAN+JENIS+TAGIHAN*/
	/*DATATABLES*/
	$route['keuangan/datatables-type-pembayaran'] = 'keuangan/api/datatabletypepembayaran';
	$route['keuangan/datatables-pembayaran'] = 'keuangan/api/datatablepembayaran';
	$route['keuangan/datatables-tagihan'] = 'keuangan/api/DatatableTagihan';
	/*VIEW*/
	$route['keuangan/manage-type-pembayaran'] = 'keuangan/view/manage_type_pembayaran';
	$route['keuangan/manage-pembayaran'] = 'keuangan/view/manage_pembayaran';
	$route['keuangan/manage-tagihan'] = 'keuangan/view/manage_tagihan';
	/*REQUEST*/
	$route['keuangan/type-pembayaran/update']['post'] = 'keuangan/api/updatetypepembayaran';
	$route['keuangan/type-pembayaran/delete']['post'] = 'keuangan/api/deletetypepembayaran';
	$route['keuangan/type-pembayaran/add']['post'] = 'keuangan/api/addtypepembayaran';//
	$route['keuangan/pembayaran/update']['post'] = 'keuangan/api/updatepembayaran';
	$route['keuangan/pembayaran/delete']['post'] = 'keuangan/api/deletepembayaran';
	$route['keuangan/pembayaran/add']['post'] = 'keuangan/api/addpembayaran';//
///////////////////////////////////////////////////////////////////////////////////
/*AKADEMIK = DATA [KELAS],[DOSEN],[PELAJARAN], [MAHASISWA]*/
	/*DATATABLES*/
	$route['akademik/datatables-mahasiswa'] = 'akademik/api/datatablemahasiswa';
	$route['akademik/datatables-class'] = 'akademik/api/datatableclass';
	$route['akademik/datatables-dosen'] = 'akademik/api/datatabledosen';
	$route['akademik/datatables-pelajaran'] = 'akademik/api/datatablepelajaran';
	/*VIEW*/
	$route['akademik/manage-mahasiswa'] = 'akademik/view/manage_mahasiswa';
	$route['akademik/manage-class'] = 'akademik/view/manage_class';
	$route['akademik/manage-dosen'] = 'akademik/view/manage_dosen';
	$route['akademik/manage-pelajaran'] = 'akademik/view/manage_pelajaran';
	/*REQUEST*/
	$route['akademik/mahasiswa/update']['post'] = 'akademik/api/updatemahasiswa';
	$route['akademik/mahasiswa/delete']['post'] = 'akademik/api/deletemahasiswa';
	$route['akademik/mahasiswa/add']['post'] = 'akademik/api/addmahasiswa';//
	$route['akademik/class/update']['post'] = 'akademik/api/updateclass';
	$route['akademik/class/delete']['post'] = 'akademik/api/deleteclass';
	$route['akademik/class/add']['post'] = 'akademik/api/addclass';//
	$route['akademik/dosen/update']['post'] = 'akademik/api/updatedosen';
	$route['akademik/dosen/delete']['post'] = 'akademik/api/deletedosen';
	$route['akademik/dosen/add']['post'] = 'akademik/api/adddosen';//
	$route['akademik/pelajaran/update']['post'] = 'akademik/api/updatepelajaran';
	$route['akademik/pelajaran/delete']['post'] = 'akademik/api/deletepelajaran';
	$route['akademik/pelajaran/add']['post'] = 'akademik/api/addpelajaran';//
///////////////////////////////////////////////////////////////////////////////////
/*MAHASISWA = INFO TAGIHAN, INFO HASIL STUDY*/
	$route['pelajaran/(:any)'] = 'mahasiswa/view/PelajaranClassDay/$1';
///////////////////////////////////////////////////////////////////////////////////
/*PENGUNJUNG*/
///////////////////////////////////////////////////////////////////////////////////
/*API*/
//DATA ID
$route['api/users/(:num)']['get'] = 'api/userid/$1';
$route['api/class/(:num)']['get'] = 'api/classid/$1';
$route['api/dosen/(:num)']['get'] = 'api/dosenid/$1';
$route['api/mahasiswa/(:num)']['get'] = 'api/mahasiswaid/$`';
$route['api/mahasiswa-user-id/(:num)']['get'] = 'api/mahasiswauserid/$1';
$route['api/mahasiswa/class/(:num)']['get'] = 'api/mahasiswainclassid/$1';
/*$route['api/mahasiswa/class/tagihan/(:num)']['get'] = 'api/mahasiswainclassidtagihan/$1';*/
$route['api/tagihan-mahasiswa/(:num)']['get'] = 'api/tagihanmahasiswaid/$1';
$route['api/pelajaran/(:num)']['get'] = 'api/pelajaranid/$1';
$route['api/pelajaran-dosen-id/(:num)']['get'] = 'api/pelajarandosenid/$1';
$route['api/type-pembayaran/(:num)']['get'] = 'api/typepembayaranid/$1';
$route['api/type-pembayaran-class-id/(:num)']['get'] = 'api/typepembayaraninclassid/$1';
$route['api/pembayaran/(:num)']['get'] = 'api/pembayaranid/$1';
$route['api/pembayaran-mahasiswa-id/(:num)']['get'] = 'api/pembayaranmahasiswaid/$1';
$route['api/sub-menu/(:num)']['get'] = 'api/submenuid/$1';
$route['api/news-management-id/(:num)']['get'] = 'api/newsmanagementid/$1';
//DATA ALL
$route['api/users']['get'] = 'api/allusers';
$route['api/class']['get'] = 'api/allclass';
$route['api/dosen']['get'] = 'api/alldosen';
$route['api/mahasiswa']['get'] = 'api/allmahasiswa';
$route['api/type-pembayaran']['get'] = 'api/alltypepembayaran';
$route['api/menu']['get'] = 'api/allmenu';
//DATA PAGINATE
$route['api/class/page']['get'] = 'api/paginateclass';
$route['api/news/page']['get'] = 'api/paginatenews';
$route['api/news-management/page']['get'] = 'api/paginatenewsmanagement';
$route['api/mahasiswa/latest']['get'] = 'api/paginatelatestmahasiswa';
////////////////////////////////////////////////////////////////////////////////////////////
//CLASSROOM
$route['classroom']['get'] = 'classroom/indexclass';
$route['classroom/(:any)']['get'] = 'classroom/indexclass';
$route['classrooms/api/class'] = 'classroom/apiclass';
$route['classrooms/api/post/(:num)'] = 'classroom/apipost/$1';
$route['classrooms/api/class/(:num)'] = 'classroom/apiclassid/$1';
$route['classroom/upload-share']['post'] = 'classroom/addshare';