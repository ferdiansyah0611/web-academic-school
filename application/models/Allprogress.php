<?php
defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * System aplikasi school management
 *
 * @author Ferdiansyah
 * @link https://github.com/ferdiansyah0611/Website-Manajemen-Sekolahan
 * @copyright Copyright (c) 2014-2019, British Columbia Institute of Technology (https://bcit.ca/)
 * @license http://opensource.org/licenses/MIT MIT License
 * @link https://codeigniter.com
 */
class AllProgress extends CI_Model{
	/**
    * Construct Class AllProgress
    */
	function __construct()
	{
		parent::__construct();
	}
	///////////////////////////////REQUEST DATA////////////////////////////////////
	public function insdata($table, $data, $message)
	{
		header('Content-Type: application/json');;
		$this->db->insert($table,$data);
		print_r(json_encode(array('success'=>$message)));
	}
	public function insdata2($table, $data, $table2, $data2, $message)
	{
		header('Content-Type: application/json');;
		$this->db->insert($table,$data);
		$this->db->insert($table2,$data2);
		print_r(json_encode(array('success'=>$message)));
	}
	public function deldata($key, $val, $tabel, $message)
	{
		header('Content-Type: application/json');;
    	$this->db->where($key,$val);
    	$this->db->delete($tabel);
    	print_r(json_encode(array('success'=>$message)));
	}
	public function deldata2($key, $val, $tabel, $key2, $val2, $tabel2, $message)
	{
		header('Content-Type: application/json');;
    	$this->db->where($key,$val);
    	$this->db->delete($tabel);
    	$this->db->where($key2,$val2);
    	$this->db->delete($tabel2);
    	print_r(json_encode(array('success'=>$message)));
	}
	public function upddata($key, $val, $table, $data, $message)
	{
		header('Content-Type: application/json');;
		$this->db->where($key,$val);
    	$this->db->update($table, $data);
    	print_r(json_encode(array('success'=>$message)));
	}
	public function upddata2($key, $val, $table, $data, $key2, $val2, $table2, $data2, $message)
	{
		header('Content-Type: application/json');;
		$this->db->where($key,$val);
    	$this->db->update($table, $data);
    	$this->db->where($key2,$val2);
    	$this->db->update($table2, $data2);
    	print_r(json_encode(array('success'=>$message)));
	}
	public function getwheredata($table, $data)
	{
		return $this->db->get_where($table, $data);
	}
}