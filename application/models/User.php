<?php
defined('BASEPATH') OR exit('No direct script access allowed');  
class User extends CI_Model{
	public function register($data)
	{
		return $this->db->insert('users', $data);
	}
	public function login($data)
	{
		return $this->db->get_where('users',$data)->num_rows();
		
	}
	public function id($id)
	{
		return $this->db->get_where('users', $id);
	}
	public function check($nisnik,$password)
	{
		$data = $this->db->get_where('users', array('nisnik'=>$nisnik));
		foreach($data->result() as $results)
		{
			if(password_verify($password, $results->password))
			{
				return $data->result();
			}
			else{
				return FALSE;
			}
		}
	}
}