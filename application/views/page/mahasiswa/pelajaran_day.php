<div class="col-xl-12">
<?php
foreach ($pelajaran as $key => $value) {
	$rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
$color = '#'.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];?>
	<div class="col-xl-4 card float-left"style="background:<?php echo $color; ?> !important;" >
		<div class="card-header">
			<h5 class="card-title mb-0"><?php echo $value->pelajaran;?></h5>
		</div>
		<div class="card-body">
			<p class="text-white text-uppercase">Mulai pada jam   : <?php echo $value->mulai ?></p>
			<p class="text-white text-uppercase">Selesai pada jam : <?php echo $value->akhir ?></p>
		</div>
	</div>
<?php
}
 ?>
</div>