<?php 
$role  = $this->session->userdata('role');
if($role == 'admin'){
?>
<div class="col-12">
	<div class="col-lg-6 col-xl-4 float-left">
        <div class="card mb-3 widget-content">
            <div class="widget-content-wrapper">
                <div class="widget-content-left">
                    <div class="widget-heading">Total Mahasiswa</div>
                    <div class="widget-subheading">Mahasiswa registered</div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-success"><span><?php echo $count_mahasiswa; ?></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-xl-4 float-left">
        <div class="card mb-3 widget-content">
            <div class="widget-content-wrapper">
                <div class="widget-content-left">
                    <div class="widget-heading">Total Guru</div>
                    <div class="widget-subheading">Guru registered</div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-primary"><span><?php echo $count_dosen; ?></span></div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6 col-xl-4 float-left">
        <div class="card mb-3 widget-content">
            <div class="widget-content-wrapper">
                <div class="widget-content-left">
                    <div class="widget-heading">Total Class</div>
                    <div class="widget-subheading">Kelas dan Jurusan</div>
                </div>
                <div class="widget-content-right">
                    <div class="widget-numbers text-warning"><span><?php echo $count_class; ?></span></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-xl-12">
	<div class="card m-3">
		<div class="card-header">
			<h5 class="card-title mb-0">Latest Active Mahasiswa</h5>
		</div>
		<div class="card-body data-latest-mahasiswa">
			
		</div>
	</div>
</div>
<?php echo $jquery; ?>
<script type="text/javascript">
$(function(){
	function get()
	{
		$.get("<?php echo base_url();?>api/mahasiswa/latest", function(data) {
			$.each(data, function(index, val) {
				if(val.avatar == null)
				{
				 $('.data-latest-mahasiswa').append('<div class="col-xl-3 float-left"><h5 class="card-title" style="text-align:center;">'+val.name+'</h5><img class="rounded-circle" style="width:100%;" src="<?php echo base_url();?>storage/img/user-default.png"><p class="text-center">'+val.active+'</p></div>');
				};
			});
		});
	};
	get();
});
</script>
<?php
}
if($role == 'akademik'){
?>

<?php } /*AKADEMIK*/
if($role == 'keuangan')
{ ?>

<?php } /*KEUANGAN*/
if($role == 'dosen')
{ ?>

<?php } /*DOSEN*/
if($role == 'mahasiswa')
{ ?>

<?php } ?> <!-- MAHASISWA -->