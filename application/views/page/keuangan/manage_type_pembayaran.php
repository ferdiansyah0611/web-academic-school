<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/datatables/media/css/jquery.dataTables.min.css">
<div class="col-xl-12">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title" style="margin-bottom: 0;">Manajement Tipe Pembayaran</h4>
		</div>
		<div class="card-body">
			<button type="button" class="type-pembayaran-add btn btn-primary mb-3">Tambah data baru</button>
			<div class="table-responsive">
				<table class="table table-inverse table-bordered table-hover table-light" id="table-type-pembayaran">
					<thead class="bg-info">
						<tr>
							<th>Kelas</th>
							<th>Jurusan</th>
							<th>Type</th>
							<th>Harga</th>
							<th>Tanggal Terakhir</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-edit">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Tipe Pembayaran</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<form id="form-edit-type-pembayaran">
				<?php echo $csrf; ?>
				<div class="modal-body">
					<input type="hidden" name="id" value="" id="data-id">
					<label>Kelas</label>
					<select class="select-class custom-select" name="class" id="data-class"></select>
					<label>Tipe</label>
					<input type="text" name="type" class="form-control mb-1" id="data-type" required="">
					<label>Harga</label>
					<input type="text" name="price" class="form-control mb-1" id="data-price" required="">
					<label>Tanggal Terakhir</label>
					<input type="text" name="end_date" class="form-control mb-1" id="data-end-date" required="">
					<label>Bulan Validasi</label>
					<select class="custom-select" name="month">
						<option value="January">January</option>
						<option value="February">February</option>
						<option value="March">March</option>
						<option value="April">April</option>
						<option value="May">May</option>
						<option value="June">June</option>
						<option value="July">July</option>
						<option value="August">August</option>
						<option value="September">September</option>
						<option value="October">October</option>
						<option value="November">November</option>
						<option value="December">December</option>
					</select>
				</div>
				<div class="modal-footer">
					<input type="reset" class="btn btn-danger" data-dismiss="modal" value="Reset">
					<input type="submit" class="btn btn-info" value="Save Changes">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-delete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="form-delete-user">
				<?php echo $csrf; ?>
				<input type="hidden" name="data">
			<div class="modal-body" style="text-align: center;">
				<p>Apakah kamu yakin ingin menghapus data yang dipilih ?</p>
				<input type="submit" name="submit" class="btn btn-danger" value="Yes">
				<input type="reset" name="reset" class="btn btn-info" value="No" data-dismiss="modal">
			</div> 
		</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-add">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah tipe pembayaran</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<form id="form-new-type-pembayaran">
				<?php echo $csrf; ?>
				<div class="modal-body">
					<label>Kelas</label>
					<select class="select-class custom-select" name="class"></select>
					<label>Tipe</label>
					<input type="text" name="type" class="form-control mb-1" required="">
					<label>Harga</label>
					<input type="text" name="price" class="form-control mb-1" required="">
					<label>Tanggal Terakhir</label>
					<input type="date" name="end_date" class="form-control mb-1">
					<label>Bulan Validasi</label>
					<select class="custom-select" name="month">
						<option value="January">January</option>
						<option value="February">February</option>
						<option value="March">March</option>
						<option value="April">April</option>
						<option value="May">May</option>
						<option value="June">June</option>
						<option value="July">July</option>
						<option value="August">August</option>
						<option value="September">September</option>
						<option value="October">October</option>
						<option value="November">November</option>
						<option value="December">December</option>
					</select>
				</div>
				<div class="modal-footer">
					<input type="reset" class="btn btn-danger" data-dismiss="modal" value="Reset">
					<input type="submit" class="btn btn-info" value="Simpan Sekarang">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/jquery-ui/jquery.js"></script>
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap4.min.js"></script>
<script>
$(function(){
	var type_pembayaran_table = $('#table-type-pembayaran').DataTable({
		processing:true,
		serverSide:true,
		responsive:true,
		ajax:{
			'url': "<?php echo base_url();?>keuangan/datatables-type-pembayaran",
			'type':'post',
			"data": {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>"},
		},
		columns:[
			{data:'kelas', name:'kelas'},
			{data:'jurusan', name:'jurusan'},
			{data:'type', name:'type'},
			{data:'price', name:'price'},
			{data:'end_date', name:'end_date'},
			{data:'action', name:'action'},
		]
	});
	function reload()
	{
		$('.reload-table').click(function(){
			type_pembayaran_table.ajax.reload(null,false);
		})
	};
	function message($data)
	{
		var alert = '<div class="alert alert-info alert-dismissible fade show alert-data" role="alert">'+
           	'<button type="button" class="close" aria-label="Close"><span aria-hidden="true">×</span></button>'+
           	$data+'</div>';
        $('.notification-data').empty();
		setTimeout(function(){
			$(alert).appendTo('.notification-data');
			$('.alert-data').fadeIn('slow');
			$('.alert-data').fadeOut(7000);
		}, 500);
	};
	function select()
	{
		$.get("<?php echo base_url();?>api/class", function(data) {
			$.each(data, function(index, val) {
				 $('.select-class').append('<option value="'+val.id+'" data-id="'+val.id+'">'+val.kelas+' '+val.jurusan+'</option>');
			});
		});
	};
	function create()
	{
		$('body').on('click', '.type-pembayaran-add', function(){
			$('#modal-add').modal('show');
		});
		$('body').on('submit', '#form-new-type-pembayaran', function(e){
			e.preventDefault();
			$.ajax({
				url: "<?php echo base_url();?>keuangan/type-pembayaran/add",
				type:'post',
				data: $('#form-new-type-pembayaran').serialize(),
				success:function(response)
				{
					message(response.success);
					type_pembayaran_table.ajax.reload(null,false);
					$('#form-new-type-pembayaran').trigger('reset');
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	function update()
	{
		$('body').on('click', '.type-pembayaran-edit', function(){
			var id = $(this).data('id');
			$('#modal-edit').modal('show');
			$.get("<?php echo base_url();?>api/type-pembayaran"+'/'+id, function(data) {
				$.each(data, function(index, val) {
					 $('#data-id').val(val.id);
					 $('#data-class').val(val.class_id).change();
					 $('#data-type').val(val.type);
					 $('#data-price').val(val.price);
					 $('#data-end-date').val(val.end_date);
				});
			});
		});
		$('body').on('submit', '#form-edit-type-pembayaran', function(e){
			e.preventDefault();
			var id = $('#data-id').val();
			$.ajax({
				url:"<?php echo base_url();?>keuangan/type-pembayaran/update",
				method: 'post',
				data: $('#form-edit-type-pembayaran').serialize(),
				success:function(response)
				{
					$('#modal-edit').modal('hide');
					type_pembayaran_table.ajax.reload(null,false);
					message(response.success);
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	function deleted()
	{
		$('body').on('click', '.type-pembayaran-delete', function(){
			$('#modal-delete').modal('show');
			$('input[name=data]').val($(this).data('id'));
		});
		$('body').on('submit', '#form-delete-user', function(e){
			e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>keuangan/type-pembayaran/delete",
				method:'post',
				data:$('#form-delete-user').serialize(),
				success:function(response)
				{
					message(response.success);
					$('#modal-delete').modal('hide');
					type_pembayaran_table.ajax.reload(null,false);
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	/*MODAL TO APPEND*/
	$('#modal-add').appendTo('.modal-data');
	$('#modal-edit').appendTo('.modal-data');
	$('#modal-delete').appendTo('.modal-data');
	/*CALLING FUNCTION*/
	select();
	reload();
	create();
	update();
	deleted();
});
</script>