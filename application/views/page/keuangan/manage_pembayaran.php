<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/datatables/media/css/jquery.dataTables.min.css">
<div class="col-xl-12">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title" style="margin-bottom: 0;">Manajement Pembayaran</h4>
		</div>
		<div class="card-body">
			<button type="button" class="pembayaran-add btn btn-primary mb-3">Tambah data baru</button>
			<div class="table-responsive">
				<table class="table table-inverse table-bordered table-hover table-light" id="table-type-pembayaran">
					<thead class="bg-info">
						<tr>
							<th>Nama</th>
							<th>Tipe</th>
							<th>Total</th>
							<th>Bayar</th>
							<th>Tanggal Pembayaran</th>
							<th>Status</th>
							<th>Tenggat</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-edit">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Pembayaran</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<form id="form-edit-pembayaran">
				<?php echo $csrf; ?>
				<div class="modal-body">
					<input type="hidden" name="id" value="" id="data-id">
					<label>Kelas</label>
					<select class="select-class custom-select" id="data-kelas"></select>
					<label>Mahasiswa</label>
					<select class="select-mahasiswa custom-select" name="mahasiswa" id="data-mahasiswa"></select>
					<label>Tipe Pembayaran</label>
					<select class="select-type-pembayaran custom-select" name="pembayaran" id="data-type-pembayaran"></select>
					<label>Pembayaran</label>
					<input type="number" name="price" class="form-control mb-1" id="data-price">
					<label>Tanggal Pembayaran</label>
					<input type="date" name="tgl_payment" class="form-control mb-1" required="" id="data-tgl-payment">
					<label>Bulan</label>
					<input type="text" name="month" class="form-control mb-1" id="data-month">
					<label>Status</label>
					<select class="custom-select" name="status" id="data-status">
						<option value="Lunas">Lunas</option>
						<option value="Belum Lunas">Belum Lunas</option>
					</select>
					<label>Tahun</label>
					<input type="text" name="tahun" class="form-control mb-1" id="data-tahun">
				</div>
				<div class="modal-footer">
					<input type="reset" class="btn btn-danger" data-dismiss="modal" value="Reset">
					<input type="submit" class="btn btn-info" value="Save Changes">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-delete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="form-delete-user">
				<?php echo $csrf; ?>
				<input type="hidden" name="data">
			<div class="modal-body" style="text-align: center;">
				<p>Apakah kamu yakin ingin menghapus data yang dipilih ?</p>
				<input type="submit" name="submit" class="btn btn-danger" value="Yes">
				<input type="reset" name="reset" class="btn btn-info" value="No" data-dismiss="modal">
			</div> 
		</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-add">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah pembayaran</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<form id="form-new-pembayaran">
				<?php echo $csrf; ?>
				<div class="modal-body">
					<label>Kelas</label>
					<select class="select-class custom-select"></select>
					<label>Mahasiswa</label>
					<select class="select-mahasiswa custom-select" name="mahasiswa"></select>
					<label>Tipe Pembayaran</label>
					<select class="select-type-pembayaran custom-select" name="pembayaran"></select>
					<label>Pembayaran</label>
					<input type="number" name="price" class="form-control mb-1">
					<label>Tanggal Pembayaran</label>
					<input type="date" name="tgl_payment" class="form-control mb-1" required="">
					<label>Bulan</label>
					<input type="text" name="month" class="form-control mb-1">
					<label>Status</label>
					<select class="custom-select" name="status">
						<option value="Lunas">Lunas</option>
						<option value="Belum Lunas">Belum Lunas</option>
					</select>
					<label>Tahun</label>
					<input type="text" name="tahun" class="form-control mb-1">
				</div>
				<div class="modal-footer">
					<input type="reset" class="btn btn-danger" data-dismiss="modal" value="Reset">
					<input type="submit" class="btn btn-info" value="Simpan Sekarang">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/jquery-ui/jquery.js"></script>
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap4.min.js"></script>
<script>
$(function(){
	var type_pembayaran_table = $('#table-type-pembayaran').DataTable({
		processing:true,
		serverSide:true,
		responsive:true,
		ajax:{
			'url': "<?php echo base_url();?>keuangan/datatables-pembayaran",
			'type':'post',
			"data": {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>"},
		},
		columns:[
			{data:'name', name:'name'},
			{data:'type', name:'type'},
			{data:'total', name:'total'},
			{data:'price', name:'price'},
			{data:'tgl_pembayaran', name:'tgl_pembayaran'},
			{data:'status', name:'status'},
			{data:'end_date', name:'end_date'},
			{data:'action', name:'action'},
		]
	});
	function reload()
	{
		$('.reload-table').click(function(){
			type_pembayaran_table.ajax.reload(null,false);
		});
	};
	function message($data)
	{
		var alert = '<div class="alert alert-info alert-dismissible fade show alert-data" role="alert">'+
           	'<button type="button" class="close" aria-label="Close"><span aria-hidden="true">×</span></button>'+
           	$data+'</div>';
        $('.notification-data').empty();
		setTimeout(function(){
			$(alert).appendTo('.notification-data');
			$('.alert-data').fadeIn('slow');
			$('.alert-data').fadeOut(7000);
		}, 500);
	};
	function select()
	{
		$.get("<?php echo base_url();?>api/class", function(data) {
			$.each(data, function(index, val) {
				 $('.select-class').append('<option value="'+val.id+'" data-id="'+val.id+'">'+val.kelas+' '+val.jurusan+'</option>');
			});
		});
		
		$('body').on('change', '.select-class', function(){
			var valdata = $('.select-class').children('option:selected').val();
			$.get("<?php echo base_url();?>api/mahasiswa/class"+'/'+valdata, function(data) {
				var parse = $.parseJSON(data);
				$.each(parse, function(index, val) {
					$('.select-mahasiswa').append('<option value="'+val.id+'">'+val.name+'</option>');
				});
			});
			$.get("<?php echo base_url();?>api/type-pembayaran-class-id"+'/'+valdata, function(data) {
				var parse = $.parseJSON(data);
				$.each(parse, function(index, val) {
					$('.select-type-pembayaran').append('<option value="'+val.id+'">'+val.kelas+' '+val.jurusan+' '+val.type+'</option>');
				});
			});
		});
	};
	function create()
	{
		$('body').on('click', '.pembayaran-add', function(){
			$('#modal-add').modal('show');
		});
		$('body').on('submit', '#form-new-pembayaran', function(e){
			e.preventDefault();
			$.ajax({
				url: "<?php echo base_url();?>keuangan/pembayaran/add",
				type:'post',
				data: $('#form-new-pembayaran').serialize(),
				success:function(response)
				{
					message(response.success);
					type_pembayaran_table.ajax.reload(null,false);
					$('#form-new-pembayaran').trigger('reset');
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	function update()
	{
		$('body').on('click', '.pembayaran-edit', function(){
			var id = $(this).data('id');
			$('#modal-edit').modal('show');
			$.get("<?php echo base_url();?>api/pembayaran"+'/'+id, function(data) {
				$.each(data, function(index, val) {
					 $('#data-id').val(val.id);
					 $('#data-kelas').val(val.class_id).change();
					 $('#data-mahasiswa').val(val.mahasiswa_id).change();
					 $('#data-type-pembayaran').val(val.type_pembayaran_id).change();
					 $('#data-price').val(val.price);
					 $('#data-tgl-pembayaran').val(val.tgl_pembayaran);
					 $('#data-status').val(val.status).change();
					 $('#data-month').val(val.month).change();
					 $('#data-tahun').val(val.tahun_ajaran);
					 $('#data-end-date').val(val.end_date);
					 $.get("<?php echo base_url();?>api/mahasiswa/class"+'/'+val.class_id, function(data) {
						var parse = $.parseJSON(data);
						$.each(parse, function(index, val) {
							$('.select-mahasiswa').append('<option value="'+val.id+'">'+val.name+'</option>');
						});
					});
					$.get("<?php echo base_url();?>api/type-pembayaran-class-id"+'/'+val.class_id, function(data) {
						var parse = $.parseJSON(data);
						$.each(parse, function(index, val) {
							$('.select-type-pembayaran').append('<option value="'+val.id+'">'+val.kelas+' '+val.jurusan+' '+val.type+'</option>');
						});
					});
				});
			});
		});
		$('body').on('submit', '#form-edit-pembayaran', function(e){
			e.preventDefault();
			var id = $('#data-id').val();
			$.ajax({
				url:"<?php echo base_url();?>keuangan/pembayaran/update",
				method: 'post',
				data: $('#form-edit-pembayaran').serialize(),
				success:function(response)
				{
					$('#modal-edit').modal('hide');
					type_pembayaran_table.ajax.reload(null,false);
					message(response.success);
					$('#form-edit-pembayaran').trigger('reset');
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	function deleted()
	{
		$('body').on('click', '.pembayaran-delete', function(){
			$('#modal-delete').modal('show');
			$('input[name=data]').val($(this).data('id'));
		});
		$('body').on('submit', '#form-delete-user', function(e){
			e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>keuangan/pembayaran/delete",
				method:'post',
				data:$('#form-delete-user').serialize(),
				success:function(response)
				{
					message(response.success);
					$('#modal-delete').modal('hide');
					type_pembayaran_table.ajax.reload(null,false);
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	/*MODAL TO APPEND*/
	$('#modal-add').appendTo('.modal-data');
	$('#modal-edit').appendTo('.modal-data');
	$('#modal-delete').appendTo('.modal-data');
	/*CALLING FUNCTION*/
	select();
	reload();
	create();
	update();
	deleted();
});
</script>