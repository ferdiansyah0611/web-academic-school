<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/datatables/media/css/jquery.dataTables.min.css">
<div class="col-xl-12">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title" style="margin-bottom: 0;">Manajement Tagihan</h4>
		</div>
		<div class="card-body">
			<button type="button" class="pembayaran-add btn btn-primary mb-3">Tambah data baru</button>
			<div class="table-responsive">
				<table class="table table-inverse table-bordered table-hover table-light" id="table-class">
					<thead class="bg-info">
						<tr>
							<th>#</th>
							<th>Kelas</th>
							<th>Jurusan</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-view-mahasiswa">
	<div class="modal-dialog modal-xl" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title title-view-mahasiswa"></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
				<table class="table table-inverse table-hover table-bordered">
					<thead class="bg-info">
						<tr>
							<th>#</th>
							<th>Nama</th>
							<th>NIS</th>
							<th>Tinggal</th>
							<th>Ayah</th>
							<th>Ibu</th>
							<th>Aktif</th>
							<th>Tagihan</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody id="table-view-mahasiswa">
					</tbody>
				</table>
			</div>
				<div id="tagihan-data"></div>

			</div>
			<div class="modal-footer">
				<button class="btn btn-primary">Check Total Tagihan Kelas</button>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/jquery-ui/jquery.js"></script>
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap4.min.js"></script>
<script>
$(function(){
	var class_table = $('#table-class').DataTable({
		processing:true,
		serverSide:true,
		responsive:true,
		ajax:{
			'url': "<?php echo base_url();?>keuangan/datatables-tagihan",
			'type':'post',
			"data": {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>"},
		},
		columns:[
			{data:'id', name:'id'},
			{data:'kelas', name:'kelas'},
			{data:'jurusan', name:'jurusan'},
			{data:'action', name:'action'},
		]
	});
	function reload()
	{
		$('.reload-table').click(function(){
			type_pembayaran_table.ajax.reload(null,false);
		});
	};
	function viewmahasiswa()
	{
		$('body').on('click', '.view-mahasiswa',function(){
			var id = $(this).data('id');
			var kelas = $(this).data('class');
			$('.title-view-mahasiswa').text('Mahasiswa Kelas '+kelas);
			$('#table-view-mahasiswa').empty();
			$('#modal-view-mahasiswa').modal('show');
			$.get("<?php echo base_url();?>api/mahasiswa/class/"+id, function(data) {
				var parse = $.parseJSON(data);
				$.each(parse, function(index, val) {
					var ids = val.id;
					$('#table-view-mahasiswa').append('<tr>'+
					 	'<td>'+val.id+'</td>'+
					 	'<td>'+val.name+'</td>'+
					 	'<td>'+val.nisnik+'</td>'+
					 	'<td>'+val.locate+'</td>'+
					 	'<td>'+val.no_father+'</td>'+
					 	'<td>'+val.no_mother+'</td>'+
					 	'<td>'+val.active+'</td>'+
					 	'<td class="view-tagihan-'+val.id+'"></td>'+
					 	'<td><button type="button" class="btn btn-danger">Send Mail</button><td'+
					 '</tr>');
					$.get("<?php echo base_url();?>api/tagihan-mahasiswa/"+val.id, function(data) {
						$.each(data, function(index, val) {
							console.log(val.tagihan);
							$('.view-tagihan-'+ids).text('Rp. '+val.tagihan);
						});
					});
				});
			});
		});
	};
	function message($data)
	{
		var alert = '<div class="alert alert-info alert-dismissible fade show alert-data" role="alert">'+
           	'<button type="button" class="close" aria-label="Close"><span aria-hidden="true">×</span></button>'+
           	$data+'</div>';
        $('.notification-data').empty();
		setTimeout(function(){
			$(alert).appendTo('.notification-data');
			$('.alert-data').fadeIn('slow');
			$('.alert-data').fadeOut(7000);
		}, 500);
	};
	/*MODAL TO APPEND*/
	$('#modal-add').appendTo('.modal-data');
	$('#modal-edit').appendTo('.modal-data');
	$('#modal-delete').appendTo('.modal-data');
	$('#modal-view-mahasiswa').appendTo('.modal-data');
	/*CALLING FUNCTION*/
	reload();
	viewmahasiswa();
});
</script>