<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/datatables/media/css/jquery.dataTables.min.css">
<div class="col-xl-12">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title" style="margin-bottom: 0;">Manajemen Mahasiswa</h4>
		</div>
		<div class="card-body">
			<button type="button" class="dosen-add btn btn-primary mb-3">Tambah data</button>
			<div class="table-responsive">
				<table class="table table-inverse table-bordered table-hover table-light" id="table-dosen">
					<thead class="bg-info">
						<tr>
							<th>#</th>
							<th>Name</th>
							<th>Telepon</th>
							<th>Gelar</th>
							<th>Lulusan</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-edit">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Dosen</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<form id="form-edit-dosen">
				<?php echo $csrf; ?>
				<div class="modal-body">
					<input type="hidden" name="id" value="" id="data-id">
					<label>Nama</label>
					<input type="text" name="name" class="form-control mb-1" id="data-name" required="">
					<label>Email</label>
					<input type="text" name="email" class="form-control mb-1" id="data-email" required="">
					<label>Tempat Tinggal</label>
					<input type="text" name="locate" class="form-control mb-1" id="data-locate" required="">
					<label>NIS/NIK/NIP</label>
					<input type="text" name="nisnik" class="form-control mb-1" id="data-nisnik" required="">
					<label>Tanggal Lahir</label>
					<input type="text" name="born" class="form-control mb-1" id="data-born" required="">
					<label>Gelar</label>
					<input type="text" name="gelar" class="form-control mb-1" id="data-gelar" required="">
					<label>Universitas</label>
					<input type="text" name="universitas" class="form-control mb-1" id="data-universitas" required="">
					<label>Telepon</label>
					<input type="text" name="phone" class="form-control mb-1" id="data-phone" required="">
				</div>
				<div class="modal-footer">
					<input type="reset" class="btn btn-danger" data-dismiss="modal" value="Reset">
					<input type="submit" class="btn btn-info" value="Save Changes">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-delete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="form-delete-dosen">
				<?php echo $csrf; ?>
				<input type="hidden" name="data">
			<div class="modal-body" style="text-align: center;">
				<p>Apakah kamu yakin ingin menghapus data yang dipilih ?</p>
				<input type="submit" name="submit" class="btn btn-danger" value="Ya">
				<input type="reset" name="reset" class="btn btn-info" value="Tidak" data-dismiss="modal">
			</div> 
		</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-add">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah data dosen baru</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<form id="form-new-dosen">
				<?php echo $csrf; ?>
				<div class="modal-body">
					<div class="col-xl-6 float-left col-xm-12 col-md-12">
						<label>Nama</label>
					<input type="text" name="name" class="form-control mb-1" required="">
					</div>
					<div class="col-xl-6 float-left col-xm-12 col-md-12">
						<label>Email</label>
					<input type="text" name="email" class="form-control mb-1" required="">
					</div>
					<div class="col-xl-6 float-left col-xm-12 col-md-12">
						<label>NIS/NIK/NIP</label>
					<input type="text" name="nisnik" class="form-control mb-1" required="">
					</div>
					<div class="col-xl-6 float-left col-xm-12 col-md-12">
						<label>Tempat Tinggal</label>
					<input type="text" name="locate" class="form-control mb-1" required="">
					</div>
					<div class="col-xl-6 float-left col-xm-12 col-md-12">
						<label>Tanggal Lahir</label>
					<input type="text" name="born" class="form-control mb-1" required="">
					</div>
					<div class="col-xl-6 float-left col-xm-12 col-md-12">
						<label>Gelar</label>
					<input type="text" name="gelar" class="form-control mb-1" required="">
					</div>
					<div class="col-xl-6 float-left col-xm-12 col-md-12">
						<label>Universitas</label>
					<input type="text" name="universitas" class="form-control mb-1" required="">
					</div>
					<div class="col-xl-6 float-left col-xm-12 col-md-12">
						<label>Telepon</label>
					<input type="text" name="phone" class="form-control mb-1" required="">
					</div>
					<div class="col-12 float-left">
						<label>Password</label>
					<input type="password" name="password" class="form-control mb-1" required="">
					</div>
					<div class="col-12 float-left">
						<label>Konfirmasi Password</label>
					<input type="password" name="confirmed_password" class="form-control mb-1" required="">
					</div>
					
					
				</div>
				<div class="modal-footer">
					<input type="reset" class="btn btn-danger" data-dismiss="modal" value="Reset">
					<input type="submit" class="btn btn-info" value="Save Changes">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/jquery-ui/jquery.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap4.min.js"></script>
<script>
$(function(){
	var dosen_table = $('#table-dosen').DataTable({
		processing:true,
		serverSide:true,
		ajax:{
			'url': "<?php echo base_url();?>akademik/datatables-dosen",
			'type':'post',
			"data": {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>"},
		},
		columns:[
			{data:'id', name:'id'},
			{data:'name', name:'name'},
			{data:'phone', name:'phone'},
			{data:'gelar', name:'gelar'},
			{data:'universitas', name:'universitas'},
			{data:'action', name:'action'},
		]
	});
	function reload()
	{
		$('.reload-table').click(function(){
			dosen_table.ajax.reload(null,false);
		});
	};
	function message($data)
	{
		var alert = '<div class="alert alert-info alert-dismissible fade show alert-data" role="alert"><button type="button" class="close" aria-label="Close"><span aria-hidden="true">×</span></button>'+$data+'</div>';
        $('.notification-data').empty();
		setTimeout(function(){
			$(alert).appendTo('.notification-data');
			$('.alert-data').fadeIn('slow');
			$('.alert-data').fadeOut(7000);
		}, 500);
	};
	function create()
	{
		$('body').on('click', '.dosen-add', function(){
			$('#modal-add').modal('show');
		});
		$('body').on('submit', '#form-new-dosen', function(e){
			e.preventDefault();
			$.ajax({
				url: "<?php echo base_url();?>akademik/dosen/add",
				type:'post',
				data: $('#form-new-dosen').serialize(),
				success:function(response)
				{
					message(response.success);
					dosen_table.ajax.reload(null,false);
					$('#form-new-dosen').trigger('reset');
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	function update()
	{
		$('body').on('click', '.dosen-edit', function(){
			var id = $(this).data('id');
			$('#modal-edit').modal('show');
			$.get("<?php echo base_url();?>api/users"+'/'+id, function(data) {
				$.each(data, function(index, val) {
					 $('#data-id').val(val.id);
					 $('#data-name').val(val.name);
					 $('#data-email').val(val.email);
					 $('#data-locate').val(val.locate);
					 $('#data-nisnik').val(val.nisnik);
					 $('#data-born').val(val.born);
				});
			});
			$.get("<?php echo base_url();?>api/dosen"+'/'+id, function(data) {
				$.each(data, function(index, val) {
					 $('#data-gelar').val(val.gelar);
					 $('#data-universitas').val(val.universitas);
					 $('#data-phone').val(val.phone);
				});
			});
		});
		$('body').on('submit', '#form-edit-dosen', function(e){
			e.preventDefault();
			var id = $('#data-id').val();
			$.ajax({
				url:"<?php echo base_url();?>akademik/dosen/update",
				method: 'post',
				data: $('#form-edit-dosen').serialize(),
				success:function(response)
				{
					$('#modal-edit').modal('hide');
					dosen_table.ajax.reload(null,false);
					message(response.success);
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	function deleted()
	{
		$('body').on('click', '.dosen-delete', function(){
			$('#modal-delete').modal('show');
			$('input[name=data]').val($(this).data('id'));
		});
		$('body').on('submit', '#form-delete-dosen', function(e){
			e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>akademik/dosen/delete",
				method:'post',
				data:$('#form-delete-dosen').serialize(),
				success:function(response)
				{
					message(response.success);
					$('#modal-delete').modal('hide');
					dosen_table.ajax.reload(null,false);
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	/*MODAL TO APPEND*/
	$('#modal-add').appendTo('.modal-data');
	$('#modal-edit').appendTo('.modal-data');
	$('#modal-delete').appendTo('.modal-data');
	/*CALLING FUNCTION*/
	reload();
	create();
	update();
	deleted();
});
</script>