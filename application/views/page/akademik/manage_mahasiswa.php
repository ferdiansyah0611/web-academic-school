<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/datatables/media/css/jquery.dataTables.min.css">
<div class="col-xl-12">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title" style="margin-bottom: 0;">Manajemen Mahasiswa</h4>
		</div>
		<div class="card-body">
			<button type="button" class="mahasiswa-add btn btn-primary mb-3">Tambah data</button>
			<div class="table-responsive">
				<table class="table table-inverse table-bordered table-hover table-light" id="table-users">
					<thead class="bg-info">
						<tr>
							<th>#</th>
							<th>Foto</th>
							<th>Nama</th>
							<th>Asal Sekolah</th>
							<th>NIS</th>
							<th>Telepon</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-edit">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Mahasiswa</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<form id="form-edit-user">
				<?php echo $csrf; ?>
				<div class="modal-body">
					<input type="hidden" name="id" value="" id="data-id">
					<label>Nams</label>
					<input type="text" name="name" class="form-control mb-1" id="data-name" required="">
					<label>Kelas</label>
					<select class="select-class custom-select" name="kelas" id="data-kelas"></select>
					<label>Email</label>
					<input type="text" name="email" class="form-control mb-1" id="data-email" required="">
					<label>Asal Sekolah</label>
					<input type="text" name="asal_sekolah" class="form-control mb-1" id="data-asal-sekolah" required="">
					<label>NIS/NIK/NIP</label>
					<input type="text" name="nisnik" class="form-control mb-1" id="data-nisnik" required="">
					<label>Telepon</label>
					<input type="text" name="phone" class="form-control mb-1" id="data-phone" required="">
					<label>Tanggal Lahir</label>
					<input type="text" name="born" class="form-control mb-1" id="data-born" required="">
					<label>Nama Ayah</label>
					<input type="text" name="father" class="form-control mb-1" id="data-father" required="">
					<label>Nama Ibu</label>
					<input type="text" name="mother" class="form-control mb-1" id="data-mother" required="">
					<label>Telepon Ayah</label>
					<input type="text" name="phone_father" class="form-control mb-1" id="data-phone-father" required="">
					<label>Telepon Ayah</label>
					<input type="text" name="phone_mother" class="form-control mb-1" id="data-phone-mother" required="">
				</div>
				<div class="modal-footer">
					<input type="reset" class="btn btn-danger" data-dismiss="modal" value="Reset">
					<input type="submit" class="btn btn-info" value="Simpan Perubahan">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-delete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="form-delete-user">
				<?php echo $csrf; ?>
				<input type="hidden" name="data">
			<div class="modal-body" style="text-align: center;">
				<p>Apakah kamu yakin ingin menghapus data yang dipilih ?</p>
				<input type="submit" name="submit" class="btn btn-danger" value="Ya">
				<input type="reset" name="reset" class="btn btn-info" value="Tidak" data-dismiss="modal">
			</div> 
		</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-add">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah data mahasiswa</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<form id="form-new-user">
				<?php echo $csrf; ?>
				<div class="modal-body">
					<input type="hidden" name="id">
					<label>Nama</label>
					<input type="text" name="name" class="form-control mb-1" required="">
					<label>Email</label>
					<input type="text" name="email" class="form-control mb-1" required="">
					<label>Kelas</label>
					<select class="select-class custom-select" name="kelas"></select>
					<label>Asal Sekolah</label>
					<input type="text" name="asal_sekolah" class="form-control mb-1" required="">
					<label>NIS/NIK/NIP</label>
					<input type="text" name="nisnik" class="form-control mb-1" required="">
					<label>Telepon</label>
					<input type="text" name="phone" class="form-control mb-1" required="">
					<label>Tempat Tinggal</label>
					<input type="text" name="locate" class="form-control mb-1" required="">
					<label>Tanggal Lahir</label>
					<input type="text" name="born" class="form-control mb-1" required="">
					<label>Nama Ayah</label>
					<input type="text" name="father" class="form-control mb-1" required="">
					<label>Nama Ibu</label>
					<input type="text" name="mother" class="form-control mb-1" required="">
					<label>Telepon Ayah</label>
					<input type="text" name="phone_father" class="form-control mb-1" required="">
					<label>Telepon Ibu</label>
					<input type="text" name="phone_mother" class="form-control mb-1" required="">
					<label>Password</label>
					<input type="password" name="password" class="form-control mb-1" required="">
					<label>Konfirmasi Password</label>
					<input type="password" name="confirmed_password" class="form-control mb-1" required="">
				</div>
				<div class="modal-footer">
					<input type="reset" class="btn btn-danger" data-dismiss="modal" value="Reset">
					<input type="submit" class="btn btn-info" value="Simpan Perubahan">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/jquery-ui/jquery.js"></script>
<script src="<?php echo base_url();?>assets/vendor/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap4.min.js"></script>
<script>
$(function(){
	var mahasiswa_table = $('#table-users').DataTable({
		processing:true,
		serverSide:true,
		ajax:{
			'url': "<?php echo base_url();?>akademik/datatables-mahasiswa",
			'type':'post',
			"data": {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>"},
		},
		columns:[
			{data:'id', name:'id'},
			{data:'avatar', name:'avatar', 'render':function(data){
				var df = '<img width="50px" height="50px" src="<?php echo base_url();?>storage/img/'+data+'">';
				return df;	
			}},
			{data:'name', name:'name'},
			{data:'asal_sekolah', name:'asal_sekolah'},
			{data:'nisnik', name:'nisnik'},
			{data:'phone', name:'phone'},
			{data:'action', name:'action'},
		]
	});
	function reload()
	{
		$('.reload-table').click(function(){
			mahasiswa_table.ajax.reload(null,false);
		});
	};
	function message($data)
	{
		var alert = '<div class="alert alert-info alert-dismissible fade show alert-data" role="alert"><button type="button" class="close" aria-label="Close"><span aria-hidden="true">×</span></button>'+$data+'</div>';
        $('.notification-data').empty();
		setTimeout(function(){
			$(alert).appendTo('.notification-data');
			$('.alert-data').fadeIn('slow');
			$('.alert-data').fadeOut(7000);
		}, 500);		
	};
	function select()
	{
		$.get("<?php echo base_url();?>api/class", function(data) {
			$.each(data, function(index, val) {
				 $('.select-class').append('<option value="'+val.id+'" data-id="'+val.id+'">'+val.kelas+' '+val.jurusan+'</option>');
			});
		});
	};
	function create()
	{
		$('body').on('click', '.mahasiswa-add', function(){
			$('#modal-add').modal('show');
		});
		$('body').on('submit', '#form-new-user', function(e){
			e.preventDefault();
			$.ajax({
				url: "<?php echo base_url();?>akademik/mahasiswa/add",
				type:'post',
				data: $('#form-new-user').serialize(),
				success:function(response)
				{
					message(response.success);
					mahasiswa_table.ajax.reload(null,false);
					$('#form-new-user').trigger('reset');
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	function update()
	{
		$('body').on('click', '.mahasiswa-edit', function(){
			var id = $(this).data('id');
			$('#modal-edit').modal('show');
			$.get("<?php echo base_url();?>api/users"+'/'+id, function(data) {
				$.each(data, function(index, val) {
					 $('#data-id').val(val.id);
					 $('#data-name').val(val.name);
					 $('#data-email').val(val.email);
					 $('#data-nisnik').val(val.nisnik);
					 $('#data-born').val(val.born);
				});
			});
			$.get("<?php echo base_url();?>api/mahasiswa-user-id"+'/'+id, function(data) {
				$.each(data, function(index, val) {
					 $('#data-asal-sekolah').val(val.asal_sekolah);
					 $('#data-kelas').val(val.class_id).change();
					 $('#data-phone').val(val.phone);
					 $('#data-father').val(val.father);
					 $('#data-mother').val(val.mother);
					 $('#data-phone-father').val(val.no_father);
					 $('#data-phone-mother').val(val.no_mother);
				});
			});
		});
		$('body').on('submit', '#form-edit-user', function(e){
			e.preventDefault();
			var id = $('#data-id').val();
			$.ajax({
				url:"<?php echo base_url();?>akademik/mahasiswa/update",
				method: 'post',
				data: $('#form-edit-user').serialize(),
				success:function(response)
				{
					$('#modal-edit').modal('hide');
					mahasiswa_table.ajax.reload(null,false);
					message(response.success);
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	function deleted()
	{
		$('body').on('click', '.mahasiswa-delete', function(){
			$('#modal-delete').modal('show');
			$('input[name=data]').val($(this).data('id'));
		});
		$('body').on('submit', '#form-delete-user', function(e){
			e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>akademik/mahasiswa/delete",
				method:'post',
				data:$('#form-delete-user').serialize(),
				success:function(response)
				{
					message(response.success);
					$('#modal-delete').modal('hide');
					mahasiswa_table.ajax.reload(null,false);
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	/*MODAL TO APPEND*/
	$('#modal-add').appendTo('.modal-data');
	$('#modal-edit').appendTo('.modal-data');
	$('#modal-delete').appendTo('.modal-data');
	/*CALLING FUNCTION*/
	reload();
	create();
	update();
	deleted();
	select();
});
</script>