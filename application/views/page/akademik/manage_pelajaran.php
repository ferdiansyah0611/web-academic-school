<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/datatables/media/css/jquery.dataTables.min.css">
<div class="col-xl-12">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title" style="margin-bottom: 0;">Manajemen Pelajaran</h4>
		</div>
		<div class="card-body">
			<button type="button" class="pelajaran-add btn btn-primary mb-3">Tambah data</button>
			<div class="table-responsive">
				<table class="table table-inverse table-bordered table-hover table-light" id="table-pelajaran">
					<thead class="bg-info">
						<tr>
							<th>#</th>
							<th>Kelas</th>
							<th>Jurusan</th>
							<th>Pelajaran</th>
							<th>Hari</th>
							<th>Mulai</th>
							<th>Akhir</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-edit">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Pelajaran</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<form id="form-edit-pelajaran">
				<?php echo $csrf; ?>
				<div class="modal-body">
					<input type="hidden" name="id" value="" id="data-id">
					<label>Kelas</label>
					<select name="kelas" class="select-class custom-select"></select>
					<label>Guru / Dosen</label>
					<select name="dosen" class="select-dosen custom-select"></select>
					<label>Pelajaran</label>
					<input type="text" name="pelajaran" class="form-control mb-1" id="data-pelajaran" required="">
					<label>Hari</label>
					<select class="custom-select mb-1" name="hari">
						<option value="Monday">Monday</option>
						<option value="Tuesday">Tuesday</option>
						<option value="Wednesday">Wednesday</option>
						<option value="Thursday">Thursday</option>
						<option value="Friday">Friday</option>
					</select>
					<label>Mulai</label>
					<input type="text" name="mulai" class="form-control mb-1" id="data-mulai" required="">
					<label>Akhir</label>
					<input type="text" name="akhir" class="form-control mb-1" id="data-akhir" required="">
				</div>
				<div class="modal-footer">
					<input type="reset" class="btn btn-danger" data-dismiss="modal" value="Reset">
					<input type="submit" class="btn btn-info" value="Simpan Perubahan">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-delete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="form-delete-pelajaran">
				<?php echo $csrf; ?>
				<input type="hidden" name="data">
			<div class="modal-body" style="text-align: center;">
				<p>Apakah kamu yakin ingin menghapus data yang dipilih ?</p>
				<input type="submit" name="submit" class="btn btn-danger" value="Ya">
				<input type="reset" name="reset" class="btn btn-info" value="Tidak" data-dismiss="modal">
			</div> 
		</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-add">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Tambah data pelajaran</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<form id="form-new-pelajaran">
				<?php echo $csrf; ?>
				<div class="modal-body">
					<label>Kelas</label>
					<select name="kelas" class="select-class custom-select"></select>
					<label>Guru / Dosen</label>
					<select name="dosen" class="select-dosen custom-select"></select>
					<label>Pelajaran</label>
					<input type="text" name="pelajaran" class="form-control mb-1" required="">
					<label>Hari</label>
					<select class="custom-select mb-1" name="hari">
						<option value="Monday">Monday</option>
						<option value="Tuesday">Tuesday</option>
						<option value="Wednesday">Wednesday</option>
						<option value="Thursday">Thursday</option>
						<option value="Friday">Friday</option>
					</select>
					<label>Mulai</label>
					<input class="form-control" placeholder="Hour" type="number" name="start_hour" max="24">
					<input class="form-control mb-1" placeholder="Minute" type="number" name="start_minute" max="60">
					<label>Akhir</label>
					<input class="form-control" placeholder="Hour" type="number" name="end_hour" max="24">
					<input class="form-control mb-1" placeholder="Minute" type="number" name="end_minute" max="60">
				</div>
				<div class="modal-footer">
					<input type="reset" class="btn btn-danger" data-dismiss="modal" value="Reset">
					<input type="submit" class="btn btn-info" value="Simpan Perubahan">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-view-class">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Lihat Kelas</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<div class="modal-body" id="data-body-class">
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/jquery-ui/jquery.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap4.min.js"></script>
<script>
$(function(){
	var pelajaran_table = $('#table-pelajaran').DataTable({
		processing:true,
		serverSide:true,
		ajax:{
			'url': "<?php echo base_url();?>akademik/datatables-pelajaran",
			'type':'post',
			"data": {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>"},
		},
		columns:[
			{data:'id', name:'id'},
			{data:'kelas', name:'kelas'},
			{data:'jurusan', name:'jurusan'},
			{data:'pelajaran', name:'pelajaran'},
			{data:'hari', name:'hari'},
			{data:'mulai', name:'mulai'},
			{data:'akhir', name:'akhir'},
			{data:'action', name:'action'},
		]
	});
	function reload()
	{
		$('.reload-table').click(function(){
			pelajaran_table.ajax.reload(null,false);
		});
	};
	function message($data)
	{
		var alert = '<div class="alert alert-info alert-dismissible fade show alert-data" role="alert"><button type="button" class="close" aria-label="Close"><span aria-hidden="true">×</span></button>'+$data+'</div>';
        $('.notification-data').empty();
		setTimeout(function(){
			$(alert).appendTo('.notification-data');
			$('.alert-data').fadeIn('slow');
			$('.alert-data').fadeOut(7000);
		}, 500);
	};
	function viewclass()
	{
		$('body').on('click','.data-view-class', function(){
			var id = $(this).data('id');
			$('#modal-view-class').modal('show');
			$.get("<?php echo base_url();?>api/class"+'/'+id, function(data) {
				$.each(data, function(index, val) {
					$('#data-body-class').empty();
					$('#data-body-class').append('<p>Kelas : '+val.kelas+'</p><p>Jurusan : '+val.jurusan+'</p>');
				});
			});
		});
	};
	function select()
	{
		$.get("<?php echo base_url();?>api/class", function(data) {
			$.each(data, function(index, val) {
				 $('.select-class').append('<option value="'+val.id+'" data-id="'+val.id+'">'+val.kelas+' '+val.jurusan+'</option>');
			});
		});
		$.get("<?php echo base_url();?>api/dosen", function(data) {
			$.each(data, function(index, val) {
				 $('.select-dosen').append('<option value="'+val.id+'" data-id="'+val.id+'">'+val.name+' '+val.gelar+'</option>');
			});
		});
	};
	function create()
	{
		$('body').on('click', '.pelajaran-add', function(){
			$('#modal-add').modal('show');
		});
		$('body').on('submit', '#form-new-pelajaran', function(e){
			e.preventDefault();
			$.ajax({
				url: "<?php echo base_url();?>akademik/pelajaran/add",
				type:'post',
				data: $('#form-new-pelajaran').serialize(),
				success:function(response)
				{
					message(response.success);
					pelajaran_table.ajax.reload(null,false);
					$('#form-new-pelajaran').trigger('reset');
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	function update()
	{
		$('body').on('click', '.pelajaran-edit', function(e){
			var id = $(this).data('id');
			var classid = $(this).data('class');
			$('#modal-edit').modal('show');
			$.get("<?php echo base_url();?>api/pelajaran"+'/'+id, function(data) {
				$.each(data, function(index, value) {
					 $('#data-id').val(value.id);
					 $('.select-dosen').val(value.dosen_id).change();
					 $('.select-class').val(value.class_id).change();
					 $('#data-pelajaran').val(value.pelajaran);
					 $('#data-hari').val(value.hari);
					 $('#data-mulai').val(value.mulai);
					 $('#data-akhir').val(value.akhir);
				});
			});
		});
		$('body').on('submit', '#form-edit-pelajaran', function(e){
			e.preventDefault();
			var id = $('#data-id').val();
			$.ajax({
				url:"<?php echo base_url();?>akademik/pelajaran/update",
				method: 'post',
				data: $('#form-edit-pelajaran').serialize(),
				success:function(response)
				{
					$('#modal-edit').modal('hide');
					pelajaran_table.ajax.reload(null,false);
					message(response.success);
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	function deleted()
	{
		$('body').on('click', '.pelajaran-delete', function(){
			$('#modal-delete').modal('show');
			$('input[name=data]').val($(this).data('id'));
		});
		$('body').on('submit', '#form-delete-pelajaran', function(e){
			e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>akademik/pelajaran/delete",
				method:'post',
				data:$('#form-delete-pelajaran').serialize(),
				success:function(response)
				{
					message(response.success);
					$('#modal-delete').modal('hide');
					pelajaran_table.ajax.reload(null,false);
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	/*MODAL TO APPEND*/
	$('#modal-add').appendTo('.modal-data');
	$('#modal-edit').appendTo('.modal-data');
	$('#modal-delete').appendTo('.modal-data');
	$('#modal-view-class').appendTo('.modal-data');
	/*CALLING FUNCTION*/
	reload();
	create();
	update();
	deleted();
	viewclass();
	select();
});
</script>