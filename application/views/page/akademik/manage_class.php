<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/datatables/media/css/jquery.dataTables.min.css">
<div class="col-xl-12">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title" style="margin-bottom: 0;">Manajement Data Kelas</h4>
		</div>
		<div class="card-body">
			<button type="button" class="class-add btn btn-primary mb-3">Tambah data baru</button>
			<div class="table-responsive">
				<table class="table table-inverse table-bordered table-hover table-light" id="table-class">
					<thead class="bg-info">
						<tr>
							<th>#</th>
							<th>Kelas</th>
							<th>Jurusan</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-edit">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Kelas</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<form id="form-edit-user">
				<?php echo $csrf; ?>
				<div class="modal-body">
					<input type="hidden" name="id" value="" id="data-id">
					<label>Kelas</label>
					<input type="text" name="kelas" class="form-control mb-1" id="data-class" required="">
					<label>Jurusan</label>
					<input type="text" name="jurusan" class="form-control mb-1" id="data-jurusan" required="">
				</div>
				<div class="modal-footer">
					<input type="reset" class="btn btn-danger" data-dismiss="modal" value="Reset">
					<input type="submit" class="btn btn-info" value="Save Changes">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-delete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="form-delete-user">
				<?php echo $csrf; ?>
				<input type="hidden" name="data">
			<div class="modal-body" style="text-align: center;">
				<p>Apakah kamu yakin ingin menghapus data yang dipilih ?</p>
				<input type="submit" name="submit" class="btn btn-danger" value="Yes">
				<input type="reset" name="reset" class="btn btn-info" value="No" data-dismiss="modal">
			</div> 
		</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-add">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah data kelas</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<form id="form-new-class">
				<?php echo $csrf; ?>
				<div class="modal-body">
					<label>Kelas</label>
					<input type="text" name="kelas" class="form-control mb-1" required="">
					<label>Jurusan</label>
					<input type="text" name="jurusan" class="form-control mb-1" required="">
				</div>
				<div class="modal-footer">
					<input type="reset" class="btn btn-danger" data-dismiss="modal" value="Reset">
					<input type="submit" class="btn btn-info" value="Save Changes">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-view-mahasiswa">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title title-view-mahasiswa"></h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="table-responsive">
					<table class="table table-inverse table-hover">
						<thead class="bg-info">
							<tr>
								<th>#</th>
								<th>Nama</th>
								<th>NIS</th>
								<th>Tinggal</th>
								<th>Asal Sekolah</th>
								<th>Ayah</th>
								<th>Ibu</th>
								<th>Aktif</th>
							</tr>
						</thead>
						<tbody id="table-view-mahasiswa">
						</tbody>
					</table>
				</div>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap4.min.js"></script>
<script>
$(function(){
	var class_table = $('#table-class').DataTable({
		processing:true,
		serverSide:true,
		responsive:true,
		ajax:{
			'url': "<?php echo base_url();?>akademik/datatables-class",
			'type':'post',
			"data": {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>"},
		},
		columns:[
			{data:'id', name:'id'},
			{data:'kelas', name:'kelas'},
			{data:'jurusan', name:'jurusan'},
			{data:'action', name:'action'},
		]
	});
	function layout()
	{
		$('body').on('click', '.switch-header-cs-class', function(e){
			var classs = $(this).data('class');
			e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>account/set/nav",
				method:'post',
				data:{
					"<?php echo $this->security->get_csrf_token_name();?>": "<?php echo $this->security->get_csrf_hash();?>",
					'nav': classs,
				},
				success:function(response)
				{
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
		$('body').on('click', '.switch-sidebar-cs-class', function(e){
			var classs = $(this).data('class');
			e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>account/set/sidebar",
				method:'post',
				data:{
					"<?php echo $this->security->get_csrf_token_name();?>": "<?php echo $this->security->get_csrf_hash();?>",
					"sidebar": classs,
				},
				success:function(response)
				{
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
		$('.dropdown-submenu a.test').on("click", function(e){
    		$(this).next('ul').toggle();
    		e.stopPropagation();
    		e.preventDefault();
  		});
	};
	function reload()
	{
		$('.reload-table').click(function(){
			class_table.ajax.reload(null,false);
		})
	};
	function message($data)
	{
		var alert = '<div class="alert alert-info alert-dismissible fade show alert-data" role="alert"><button type="button" class="close" aria-label="Close"><span aria-hidden="true">×</span></button>'+$data+'</div>';
        $('.notification-data').empty();
		setTimeout(function(){
			$(alert).appendTo('.notification-data');
			$('.alert-data').fadeIn('slow');
			$('.alert-data').fadeOut(7000);
		}, 500);
	};
	function viewmahasiswa()
	{
		$('body').on('click', '.view-mahasiswa',function(){
			var id = $(this).data('id');
			var kelas = $(this).data('class');
			$('.title-view-mahasiswa').text('Mahasiswa Kelas '+kelas);
			$('#table-view-mahasiswa').empty();
			$('#modal-view-mahasiswa').modal('show');
			$.get("<?php echo base_url();?>api/mahasiswa/class/"+id, function(data) {
				var parse = $.parseJSON(data);
				$.each(parse, function(index, val) {
					 $('#table-view-mahasiswa').append('<tr>'+
					 	'<td>'+val.id+'</td>'+
					 	'<td>'+val.name+'</td>'+
					 	'<td>'+val.nisnik+'</td>'+
					 	'<td>'+val.locate+'</td>'+
					 	'<td>'+val.asal_sekolah+'</td>'+
					 	'<td>'+val.no_father+'</td>'+
					 	'<td>'+val.no_mother+'</td>'+
					 	'<td>'+val.active+'</td>'+
					 	'</tr>');
				});
			});
		});
	};
	function create()
	{
		$('body').on('click', '.class-add', function(){
			$('#modal-add').modal('show');
		});
		$('body').on('submit', '#form-new-class', function(e){
			e.preventDefault();
			$.ajax({
				url: "<?php echo base_url();?>akademik/class/add",
				type:'post',
				data: $('#form-new-class').serialize(),
				success:function(response)
				{
					message(response.success);
					class_table.ajax.reload(null,false);
					$('#form-new-class').trigger('reset');
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	function update()
	{
		$('body').on('click', '.class-edit', function(){
			var id = $(this).data('id');
			$('#modal-edit').modal('show');
			$.get("<?php echo base_url();?>api/class"+'/'+id, function(data) {
				$.each(data, function(index, val) {
					 $('#data-id').val(val.id);
					 $('#data-class').val(val.kelas);
					 $('#data-jurusan').val(val.jurusan);
				});
			});
		});
		$('body').on('submit', '#form-edit-user', function(e){
			e.preventDefault();
			var id = $('#data-id').val();
			$.ajax({
				url:"<?php echo base_url();?>akademik/class/update",
				method: 'post',
				data: $('#form-edit-user').serialize(),
				success:function(response)
				{
					$('#modal-edit').modal('hide');
					class_table.ajax.reload(null,false);
					message(response.success);
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	function deleted()
	{
		$('body').on('click', '.class-delete', function(){
			$('#modal-delete').modal('show');
			$('input[name=data]').val($(this).data('id'));
		});
		$('body').on('submit', '#form-delete-user', function(e){
			e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>akademik/class/delete",
				method:'post',
				data:$('#form-delete-user').serialize(),
				success:function(response)
				{
					message(response.success);
					$('#modal-delete').modal('hide');
					class_table.ajax.reload(null,false);
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	/*MODAL TO APPEND*/
	$('#modal-add').appendTo('.modal-data');
	$('#modal-edit').appendTo('.modal-data');
	$('#modal-delete').appendTo('.modal-data');
	$('#modal-view-mahasiswa').appendTo('.modal-data');
	/*CALLING FUNCTION*/
	layout();
	viewmahasiswa();
	reload();
	create();
	update();
	deleted();
});
</script>