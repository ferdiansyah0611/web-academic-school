<?php 
$command = escapeshellcmd(base_url().'assets/jarvis.py');
$output = shell_exec($command);
shell_exec("python ".base_url()."assets/jarvis.py");
echo $output;
 ?>
<div class="col-xl-12">
	<div class="card">
		<div class="card-header">
			<h5 class="card-title mb-0">Add new data</h5>
		</div>
		<div class="card-body">
			<form>
				<?php echo $csrf; ?>
				<div class="col-12 float-left">
					<input type="text" name="title" placeholder="Task title" class="form-control">
				</div>
				<div class="col-xl-6 float-left">
					<label>Tipe Tugas</label>
					<select name="type_task" class="custom-select type-pelajaran">
						<option value="pg" selected="">Tugas PG</option>
						<option value="isian">Tugas Isian</option>
						<option value="pg_isian">Tugas PG dan Isian</option>
					</select>
				</div>
				<div class="col-xl-6 float-left div-select-pelajaran">
					<label>Choose Pelajaran</label>
					<select name="choose_study" class="select-pelajaran custom-select"></select>
				</div>
				<div class="col-xl-6 float-left count-pg-div">
					<label>Jumlah Soal PG</label>
					<input type="number" name="count_pg" class="form-control count-pg">
				</div>
				<div class="col-xl-6 float-left count-isian-div">
					<label>Jumlah Soal Isian</label>
					<input type="number" name="count_isian" class="form-control count-isian">
				</div>
				<div class="col-xl-6 float-left score-pg">
					<label>Nilai PG Benar</label>
					<input type="number" name="pg_score" class="form-control pg-score" placeholder="PG yang benar">
				</div>
				<div class="col-xl-6 float-left score-isian">
					<label>Nilai isian yg benar</label>
					<input type="number" name="isian_score" class="form-control isian-score" placeholder="Isian yang benar">
				</div>
				<div class="col-xl-6 float-left total-penilaian-pg">
					<label>Total penilaian PG</label>
					<input type="number" name="score_pg" class="form-control" placeholder="Total penilaian">
				</div>
				<div class="col-xl-6 float-left total-penilaian-isian">
					<label>Total penilaian isian</label>
					<input type="number" name="score_isian" class="form-control" placeholder="Total penilaian">
				</div>
				<div class="col-xl-6 float-left">
					<label>Choose Mahasiswa</label>
					<select name="mahasiswa" class="select-mahasiswa custom-select"></select>
				</div>
				<div class="col-12 float-left mt-3">
					<input type="submit" class="btn btn-primary">
				</div>
			</form>
		</div>
	</div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
$(function(){
	function select()
	{
		$('.div-select-pelajaran').css('display','none');
		$.get("<?php echo base_url('api/pelajaran-dosen-id/'.$this->session->userdata('dosen_id'));?>", function(data) {
			$('.div-select-pelajaran').css('display','block');
			$.each(data, function(index, val) {
				console.log(val);
				var dataclass = val.class_id;
				$('.select-pelajaran').append('<option value="'+val.pelajaran_id+'" data-id="'+val.class_id+'">'+val.kelas+' '+val.jurusan+' '+val.pelajaran+'</option>');
				$.get("<?php echo base_url();?>api/mahasiswa/class/"+dataclass, function(datas) {
					var parse = JSON.parse(datas);
					$.each(parse, function(index, val) {
						 console.log(val);
						 var datacls = $('.select-pelajaran').data('id');
						 $('.select-mahasiswa').append('<option value="'+val.id+'">'+val.name+'</option>');
					});
				});
			});
		});
	};
	function keyword()
	{
		$('.pg-score').on('keyup', function(e){
			var soal_pg = $('.count-pg').val();
			var trues_pg = $(this).val();
			var score = trues_pg * (100/soal_pg);
			$('input[name=score_pg]').val(score);
		});
		$('.isian-score').on('keyup', function(e){
			var soal_isian = $('.count-isian').val();
			var trues_isian = $(this).val();
			var score = trues_isian * (100/soal_isian);
			$('input[name=score_isian]').val(score);
		});
	};
	function change()
	{
		$('.total-penilaian-isian').css('display','none');
		$('.score-isian').css('display','none');
		$('.count-isian-div').css('display','none');
		$('.type-pelajaran').on('change', function(){
			var type = $(this).val();
			if(type == 'pg')
			{
				$('.count-pg-div').css('display','block');
				$('.count-isian-div').css('display','none');
				$('.score-isian').css('display','none');
				$('.count').empty();
				$('.count').append(
					'<option>5 Soal PG</option>'+
					'<option>10 Soal PG</option>'+
					'<option>15 Soal PG</option>'+
					'<option>20 Soal PG</option>'+
					'<option>25 Soal PG</option>'+
					'<option>30 Soal PG</option>'+
					'<option>35 Soal PG</option>'+
					'<option>40 Soal PG</option>'+
					'<option>45 Soal PG</option>'+
					'<option>50 Soal PG</option>'
					);
			}
			if(type == 'isian')
			{
				$('.total-penilaian-pg').css('display','none');
				$('.total-penilaian-isian').css('display','block');

				$('.count-pg-div').css('display','none');
				$('.count-isian-div').css('display','block');

				$('.score-pg').css('display','none');
				$('.score-isian').css('display','block');

				$('.count').empty();
				$('.count').append(
					'<option>5 Isian</option>'+
					'<option>10 Isian</option>'+
					'<option>15 Isian</option>'+
					'<option>20 Isian</option>'+
					'<option>25 Isian</option>'+
					'<option>30 Isian</option>'+
					'<option>35 Isian</option>'+
					'<option>40 Isian</option>'+
					'<option>45 Isian</option>'+
					'<option>50 Isian</option>'
					);
			}
			if(type == 'pg_isian')
			{
				$('.total-penilaian-pg').css('display','block');
				$('.total-penilaian-isian').css('display','block');

				$('.count-pg-div').css('display','block');
				$('.count-isian-div').css('display','block');
				$('.score-pg').css('display','block');
				$('.score-isian').css('display','block');
			}
		});
		$('.select-pelajaran').on('change', function(){

		});
	};
	select();
	keyword();
	change();
});
</script>