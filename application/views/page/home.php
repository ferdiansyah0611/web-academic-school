<style type="text/css">
	.date-news{
		display: inline-block;
    margin-bottom: .5rem;
    position: absolute;
    left: 10%;
    top: 89%;
    background: red;
    color: white;
	}
</style>
<div class="col-xl-12">
	<div id="content-news"></div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
$(function(){
	function news()
	{
		setTimeout(function(){
			$.get("<?php echo base_url();?>api/news/page", function(data) {
				console.log(data);
				$.each(data.news, function(index, val) {
					 console.log(val.content);
					 $('#content-news').append('<div class="col-xl-4 float-left card-news">'+
					 	'<h5 class="card-title mb-0"><a href="<?php echo base_url();?>'+val.category+'/'+val.title+'">'+val.title+'</a></h5>'+
					 	'<img class="img-fluid news-img" style="border: 2px solid red;" src="<?php echo base_url('storage/img/');?>'+val.file+'"><label class="date-news">'+val.created_at+'</label></div>');
				});
			});
		}, 1000);
	};
	news();
});
</script>