<div class="card">
		<div class="card-header"><h5 class="card-title mb-1">Setting Akun</h5></div>
		<form id="form-edit-user" enctype="multipart/form-data" method="post" action="<?php echo base_url();?>account/setting">
		<div class="card-body">
			<?php
			if (isset($error_form)){
    			echo "<div class='alert alert-warning'>$error_form</div>";
			}
			if (isset($success_form)){
    			echo "<div class='alert alert-primary'>$success_form</div>";
			}
			?>
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
				<input type="hidden" name="id" value="" id="data-id">
				<div class="col-xl-6 float-left">
					<label>Name</label>
					<input type="text" name="name" class="form-control mb-1" id="data-name" value="<?php echo $this->session->userdata("name");?>">
					<?php if(form_error('name') == true){
						echo '<span class="text-danger">'.form_error('name').'</span>';
					};?>
				</div>
				<div class="col-xl-6 float-left">
					<label>Email</label>
					<input type="text" name="email" class="form-control mb-1" id="data-email" required="" value="<?php echo $this->session->userdata("email");?>">
				</div>
				<div class="col-xl-6 float-left">
					<label>NIS/NIK/NIP</label>
					<input type="text" name="nisnik" class="form-control mb-1" id="data-nisnik" required="" value="<?php echo $this->session->userdata("nisnik");?>">
				</div>
				<div class="col-xl-6 float-left">
					<label>Born</label>
					<input type="text" name="born" class="form-control mb-1" id="data-born" required="" value="<?php echo $this->session->userdata("born");?>">
				</div>
				<div class="col-xl-6 float-left">
					<label>Locate</label>
					<input type="text" name="locate" class="form-control mb-1" id="data-locate" required="" value="<?php echo $this->session->userdata("born");?>">
				</div>
				<div class="col-xl-6 float-left">
					<label>Foto Profil</label>
					<input type="file" name="image" class="form-control">
				</div>
				<div class="col-xl-12 float-left">
					<label>Password</label>
					<input type="text" class="form-control mb-1" id="data-password">
				</div>
				<div class="col-12 float-left">
					<label>Confirmasi</label>
					<input type="text" class="form-control mb-1" id="data-confirm-password" value="Confirmed password">
				</div>
				
				
			
		</div>
		<div class="card-footer">
			<input type="submit" class="btn btn-info ml-3" value="Save Changes">
			<input type="reset" class="btn btn-danger ml-3" data-dismiss="modal" value="Reset">
		</div>
		</form>
	</div>