<div class="col-xl-12">
	<div class="card">
		<div class="card-body">
			<div class="col-xl-6 float-left">
				<form id="first-menu-form">
					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<label>First Menu</label>
					<input type="text" name="menu" class="form-control">
          <label>Tipe</label>
					<select class="custom-select select-type" name="type">
						<option value="content">Content</option>
						<option value="menu" selected="">Menu</option>
					</select>
          <div class="contents-link"></div>
					<input type="submit" class="btn btn-info mt-1" value="Simpan">
				</form>
			</div>
			<div class="col-xl-6 float-left">
				<form id="sub-menu-form">
          <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
					<label>Select First Menu</label>
					<select class="select-menu custom-select" name="menu_id"></select>
					<label>Sub Menu</label>
					<input type="text" name="sub_menu" class="form-control">
          <label>Tipe</label>
          <select class="custom-select select-type" name="type">
            <option value="content">Content</option>
            <option value="menu" selected="">Menu</option>
          </select>
					<input type="submit" class="btn btn-info mt-1" value="Simpan">
				</form>
			</div>
		</div>
	</div>
	<div class="card mt-3">
		<div class="card-header">
			<h5 class="card-title mb-0">Content of Menu</h5>
		</div>
		<form>
		<div class="card-body">
				<div class="col-xl-6 float-left mb-3">
					<label>Judul Content</label>
					<select class="select-menu custom-select"></select>
				</div>	
				<div class="col-xl-6 float-left mb-3">
					<label>Select First Menu</label>
					<select class="select-menu custom-select"></select>
				</div>
				<div id="content"></div>
			<div class="card-footer">
				<input type="submit" name="publish" class="btn btn-info" value="Publikasikan">
				<input type="submit" name="draft" class="btn btn-primary" value="Simpan sebagai draft">
			</div>
    </div>
			</form>
		</div>
	</div>

<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/jquery-ui/jquery.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/5.2.1/tinymce.min.js"></script>
<script type="text/javascript">
$(function(){
	$.ajaxSetup({
          headers: {
              '<?php echo $this->security->get_csrf_hash();?>': '<?php echo $this->security->get_csrf_token_name();?>'
          }
    });
    function select()
    {
    		$.get("<?php echo base_url();?>api/menu", function(data) {
    			$.each(data, function(index, val) {
    				$('.select-menu').append('<option value="'+val.id+'">'+val.menu+'</option>');
            console.log(val.menu);
    			});
    		});
    };
    function change()
    {
      
      $('.select-type').on('change', function(){
        var name = $('.select-type option:selected').val();
        if(name == 'content')
        {
          $('.contents-link').append('<input type="text" class="form-control" placeholder="Link url..."><div class="content"></div>');
        }
        if(name == 'menu')
        {
          $('.contents-link').empty();
        }
      });
    };
    function create()
    {
    	$('body').on('submit', '#first-menu-form', function(e){
    		e.preventDefault();
    		$.ajax({
    			url:"<?php echo base_url();?>web/set/menu",
    			method:"post",
    			data: $('#first-menu-form').serialize(),
    			success:function()
    			{
    				alert('ok');
    			},
    			error:function()
    			{
    				console.log(e);
    			}
    		});
    	});
      $('body').on('submit', '#sub-menu-form', function(e){
        e.preventDefault();
        $.ajax({
          url:"<?php echo base_url();?>web/set/sub-menu",
          method:"post",
          data: $('#sub-menu-form').serialize(),
          success:function()
          {
            alert('ok');
          },
          error:function()
          {
            console.log(e);
          }
        });
      });
    };
    select();
    change();
    create();
	tinymce.init({
    selector: '#content',
    plugins : "advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking save table directionality emoticons template paste textpattern",
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager",
    automatic_uploads: true,
    image_advtab: true,
    images_upload_url: "<?php echo base_url()?>tiny/upload",
    file_picker_types: 'image', 
    paste_data_images:true,
    relative_urls: false,
    remove_script_host: false,
    file_picker_callback: function(cb, value, meta) {
       var input = document.createElement('input');
       input.setAttribute('type', 'file');
       input.setAttribute('accept', 'image/*');
       input.onchange = function() {
          var file = this.files[0];
          var reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = function () {
             var id = 'post-image-' + (new Date()).getTime();
             var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
             var blobInfo = blobCache.create(id, file, reader.result);
             blobCache.add(blobInfo);
             cb(blobInfo.blobUri(), { title: file.name });
          };
       };
       input.click();
    }
  });
})
</script>