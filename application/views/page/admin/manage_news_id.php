<div class="col-xl-12">
	<div class="card">
		<div class="card-header">
			<h5 class="card-title mb-0">Management News</h5>
		</div>
		<form id="form-update-content">
			<div class="card-body" id="news-management">
				<?php echo $csrf; ?>
				<input type="hidden" name="id">
				<div class="col-xl-6 float-left mb-3 p-1">
					<label>Judul</label>
					<input type="text" name="title" class="form-control">
				</div>
				<div class="col-xl-6 float-left mb-3 p-1">
					<label>Category</label>
					<select class="custom-select" name="category">
						<option value="pendidikan">Pendidikan</option>
					</select>
				</div>
				<div class="col-xl-6 float-left mb-3 p-1">
					<label>Status</label>
					<select class="custom-select" name="status">
						<option value="publish">Publish</option>
						<option value="draft">Draft</option>
					</select>
				</div>
				<div class="col-xl-6 float-left mb-3 p-1">
					<label>Image Show</label>
					<input type="file" name="file" class="form-control">
				</div>
				<textarea id="content" name="content"></textarea>
			</div>
		<div class="card-footer">
			<input type="submit" name="publish" value="Publikasikan" class="btn btn-info mt-2">
			<input type="submit" name="draft" value="Simpan Sebagai Draft" class="btn btn-primary mt-2">
		</div>
		</form>
	</div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/tiny/tinymce.min.js"></script>
<script type="text/javascript">
$(function(){
	tinymce.init({
    selector: '#content',
    plugins : "advlist autolink lists link image charmap print preview hr anchor pagebreak searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking save table directionality emoticons template paste textpattern",
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image responsivefilemanager",
    automatic_uploads: true,
    image_advtab: true,
    images_upload_url: "<?php echo base_url()?>tiny/upload",
    file_picker_types: 'image', 
    paste_data_images:true,
    relative_urls: false,
    remove_script_host: false,
    file_picker_callback: function(cb, value, meta) {
       var input = document.createElement('input');
       input.setAttribute('type', 'file');
       input.setAttribute('accept', 'image/*');
       input.onchange = function() {
          var file = this.files[0];
          var reader = new FileReader();
          reader.readAsDataURL(file);
          reader.onload = function () {
             var id = 'post-image-' + (new Date()).getTime();
             var blobCache =  tinymce.activeEditor.editorUpload.blobCache;
             var blobInfo = blobCache.create(id, file, reader.result);
             blobCache.add(blobInfo);
             cb(blobInfo.blobUri(), { title: file.name });
          };
       };
       input.click();
    }
  });

	function get()
	{
		var pageURL = window.location.href;
		var lastURLSegment = pageURL.substr(pageURL.lastIndexOf('/') + 1);
		$.get("<?php echo base_url()?>api/news-management-id/"+lastURLSegment, function(data) {
			$.each(data, function(index, val) {
				 console.log(val);
				 $('#content').html(val.content);
				 $('input[name=id]').val(val.id);
				 $('input[name=title]').val(val.title);
				 $('input[name=category]').val(val.category).change();
				 $('input[name=status]').val(val.status).change();
			});
		});
	};
	function create()
	{
		$('#form-update-content').on('submit', function(e){
			e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>news/management/update",
				method:"post",
				processData:false,
				contentType:false,
				data:new FormData(this),
				success:function(response)
				{
					alert(response.success);
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	create();
	get();
});
</script>