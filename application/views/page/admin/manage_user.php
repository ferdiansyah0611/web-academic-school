<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/datatables/media/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.20/css/jquery.dataTables.min.css">

<div class="col-xl-12">
	<div class="card">
		<div class="card-header">
			<h4 class="card-title" style="margin-bottom: 0;">Manajemen Pengguna</h4>
		</div>
		<div class="card-body">
			<button type="button" class="user-add btn btn-primary mb-3">Tambah data</button>
			<div class="table-responsive">
				<table class="table table-inverse table-bordered table-hover table-light" id="table-users">
					<thead class="bg-info">
						<tr>
							<th>#</th>
							<th>Nama</th>
							<th>Email</th>
							<th>Tempat Tinggal</th>
							<th>NIS/NIK</th>
							<th>Action</th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
<div class="modal fade" id="modal-edit">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit User</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<form id="form-edit-user">
				<?php echo $csrf; ?>
				<div class="modal-body">
					<input type="hidden" name="id" value="" id="data-id">
					<label>Nama</label>
					<input type="text" name="name" class="form-control mb-1" id="data-name" required="">
					<label>Email</label>
					<input type="text" name="email" class="form-control mb-1" id="data-email" required="">
					<label>NIS/NIK/NIP</label>
					<input type="text" name="nisnik" class="form-control mb-1" id="data-nisnik" required="">
					<label>Tanggal Lahir</label>
					<input type="text" name="born" class="form-control mb-1" id="data-born" required="">
					<label>Role</label>
					<select name="role" id="data-role" class="custom-select" required="">
						<option value="admin">Admin</option>
						<option value="akademik">Akademik</option>
						<option value="dosen">Dosen / Guru</option>
						<option value="keuangan">Keuangan</option>
						<option value="mahasiswa">Mahasiswa</option>
					</select>
				</div>
				<div class="modal-footer">
					<input type="reset" class="btn btn-danger" data-dismiss="modal" value="Reset">
					<input type="submit" class="btn btn-info" value="Simpan Perubahan">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-delete">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="form-delete-user">
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
				<input type="hidden" name="data">
			<div class="modal-body" style="text-align: center;">
				<p>Apakah kamu yakin ingin menghapus data yang dipilih ?</p>
				<input type="submit" name="submit" class="btn btn-danger" value="Yes">
				<input type="reset" name="reset" class="btn btn-info" value="No" data-dismiss="modal">
			</div> 
		</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<div class="modal fade" id="modal-add">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Tambah pengguna baru</h4>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					<span class="sr-only">Close</span>
				</button>
			</div>
			<form id="form-new-user">
				<?php echo $csrf; ?>
				<div class="modal-body">
					<label>Nama</label>
					<input type="text" name="name" class="form-control mb-1" required="">
					<label>Email</label>
					<input type="text" name="email" class="form-control mb-1" required="">
					<label>Password</label>
					<input type="password" name="password" class="form-control mb-1" required="">
					<label>Confirmed Password</label>
					<input type="password" name="confirmed_password" class="form-control mb-1" required="">
					<label>NIS/NIK/NIP</label>
					<input type="text" name="nisnik" class="form-control mb-1" required="">
					<label>Tanggal Lahir</label>
					<input type="text" name="born" class="form-control mb-1" required="">
					<label>Tempat Tinggal</label>
					<input type="text" name="locate" class="form-control mb-1" required="">
					<label>Role</label>
					<select name="role" class="custom-select">
						<option value="admin">Admin</option>
						<option value="akademik">Akademik</option>
						<option value="dosen">Dosen / Guru</option>
						<option value="keuangan">Keuangan</option>
						<option value="mahasiswa">Mahasiswa</option>
					</select>
				</div>
				<div class="modal-footer">
					<input type="reset" class="btn btn-danger" data-dismiss="modal" value="Reset">
					<input type="submit" class="btn btn-info" value="Simpan Perubahan">
				</div>
			</form>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/jquery-ui/jquery.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/datatables/media/js/dataTables.bootstrap4.min.js"></script>
<script>
$(function(){
	var users_table = $('#table-users').DataTable({
		processing:true,
		serverSide:true,
		ajax:{
			'url': "<?php echo base_url();?>admin/datatables-users",
			'type':'post',
			"data": {"<?php echo $this->security->get_csrf_token_name(); ?>": "<?php echo $this->security->get_csrf_hash(); ?>"},
		},
		columns:[
			{data:'id', name:'id'},
			{data:'name', name:'name'},
			{data:'email', name:'email'},
			{data:'locate', name:'locate'},
			{data:'nisnik', name:'nisnik'},
			{data:'action', name:'action'},
		]
	});
	function reload()
	{
		$('.reload-table').click(function(){
			users_table.ajax.reload(null,false);
		});
	};
	function message($data)
	{
		var alert = '<div class="alert alert-info alert-dismissible fade show alert-data" role="alert"><button type="button" class="close" aria-label="Close"><span aria-hidden="true">×</span></button>'+$data+'</div>';
        $('.notification-data').empty();
		setTimeout(function(){
			$(alert).appendTo('.notification-data');
			$('.alert-data').fadeIn('slow');
			$('.alert-data').fadeOut(7000);
		}, 500);
	};
	function create()
	{
		$('body').on('click', '.user-add', function(){
			$('#modal-add').modal('show');
		});
		$('body').on('submit', '#form-new-user', function(e){
			e.preventDefault();
			$.ajax({
				url: "<?php echo base_url();?>admin/user/add",
				type:'post',
				data: $('#form-new-user').serialize(),
				success:function(response)
				{
					message(response.success);
					users_table.ajax.reload(null,false);
					$('#form-new-user').trigger('reset');
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	function update()
	{
		$('body').on('click', '.user-edit', function(){
			var id = $(this).data('id');
			$('#modal-edit').modal('show');
			$.get("<?php echo base_url();?>api/users"+'/'+id, function(data) {
				$.each(data, function(index, val) {
					 $('#data-id').val(val.id);
					 $('#data-name').val(val.name);
					 $('#data-email').val(val.email);
					 $('#data-nisnik').val(val.nisnik);
					 $('#data-born').val(val.born);
					 $('#data-role').val(val.role);
				})
			})
		});
		$('body').on('submit', '#form-edit-user', function(e){
			e.preventDefault();
			var id = $('#data-id').val();
			$.ajax({
				url:"<?php echo base_url();?>admin/user/update",
				method: 'post',
				data: $('#form-edit-user').serialize(),
				success:function(response)
				{
					$('#modal-edit').modal('hide');
					users_table.ajax.reload(null,false);
					message(response.success);
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	function deleted()
	{
		$('body').on('click', '.user-delete', function(){
			var id = $(this).data('id');
			$('#modal-delete').modal('show');
			$('input[name=data]').val(id);
		});
		$('body').on('submit', '#form-delete-user', function(e){
			e.preventDefault();
			$.ajax({
				url:"<?php echo base_url();?>admin/user/delete",
				method:'post',
				data:$('#form-delete-user').serialize(),
				success:function(response)
				{
					users_table.ajax.reload(null,false);
					$('#modal-delete').modal('hide');
					message(response.success);
				},
				error:function(e)
				{
					console.log(e);
				}
			});
		});
	};
	/*MODAL TO APPEND*/
	$('#modal-add').appendTo('.modal-data');
	$('#modal-edit').appendTo('.modal-data');
	$('#modal-delete').appendTo('.modal-data');
	/*CALLING FUNCTION*/
	reload();
	create();
	update();
	deleted();
});
</script>