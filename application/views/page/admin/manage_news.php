<div class="col-xl-12">
	<div class="card">
		<div class="card-header mb-0">
			<h5 class="card-title">Management News</h5>
		</div>
		<div class="card-body" id="news-management">
			
		</div>
		<div class="card-footer">
			
		</div>
	</div>
</div>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript">
$(function(){
	function get()
	{
		$.get("<?php echo base_url();?>api/news-management/page", function(data) {
			$.each(data.news, function(index, val) {
				let get = '<div class="col-xl-3 float-left"><p class="text-center text-capitalize"><a href="<?php echo base_url();?>news/management/data/'+val.id+'">'+val.title+'</a></p><img class="img-fluid" src="<?php echo base_url('storage/img/');?>'+val.file+'"></div>';
				$('#news-management').append(get);
			});
		});
	};
	get();
});
</script>