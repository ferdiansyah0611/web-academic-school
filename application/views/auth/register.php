<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<link href="<?php echo base_url();?>assets/css/hfwiufbwe843dhf.css" rel="stylesheet"></head>
<body>
<div class="container content">
	<div class="offset-md-2 offset-sm-1 offset-lg-2" style="margin-top: 30px !important;">
		<div class="card justify-content-center col-xl-10 mt-3">
			<div class="card-header"><h4 class="card-title">Register account</h4></div>
			<form method="post" action="<?php echo base_url();?>register">
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
				<div class="card-body">
					<div class="col-xl-6 float-left">
					<label>Name</label>
					<input type="text" name="name" value="<?php echo set_value('name'); ?>" class="form-control mb-1">
					<?php if(form_error('name') == true){
						echo '<span class="text-danger">'.form_error('name').'</span>';
					};?>
					</div>
					<div class="col-xl-6 float-left">
					<label>Email</label>
					<input type="email" name="email" value="<?php echo set_value('email'); ?>" class="form-control mb-1">
					<?php if(form_error('email') == true){
						echo '<span class="text-danger">'.form_error('email').'</span>';
					};?>
					</div>
					<div class="col-xl-12 float-left">
					<label>Locate</label>
					<input type="text" name="locate" value="<?php echo set_value('locate'); ?>" class="form-control mb-1">
					<?php if(form_error('locate') == true){
						echo '<span class="text-danger">'.form_error('locate').'</span>';
					};?>
					</div>
					<div class="col-xl-6 float-left">
					<label>NIS or NIK</label>
					<input type="number" name="nisnik" value="<?php echo set_value('nisnik'); ?>" class="form-control mb-1">
					<?php if(form_error('nisnik') == true){
						echo '<span class="text-danger">'.form_error('nisnik').'</span>';
					};?>
					</div>
					<div class="col-xl-6 float-left">
					<label>Born</label>
					<input type="date" name="born" <?php echo set_value('born'); ?> class="form-control">
					<?php if(form_error('born') == true){
						echo '<span class="text-danger">'.form_error('born').'</span>';
					};?>
					</div>
					<div class="col-xl-12 float-left">
					<label>Password</label>
					<input type="password" name="password" <?php echo set_value('password'); ?> class="form-control mb-1">
					<?php if(form_error('password') == true){
						echo '<span class="text-danger">'.form_error('password').'</span>';
					};?>
					</div>
					<div class="col-xl-12 float-left">
					<label>Confirmed Password</label>
					<input type="password" name="confirmed_password" <?php echo set_value('confirmed_password'); ?> class="form-control mb-1">
					<?php if(form_error('confirmed_password') == true){
						echo '<span class="text-danger">'.form_error('confirmed_password').'</span>';
					};?>
					</div>
					<div class="col-xl-12 float-left">
					<input class="custom-checkbox mt-3" type="checkbox" name="checkbox" value="<?php echo set_value('checkbox'); ?>">
					<label>Remember password</label>
					</div>
				</div>
					<div class="card-footer float-left">
						<input type="submit" name="submit" class="btn btn-info" value="Daftar">
						<label class="float-right mb-0 text-right">Have a accounts ? <a href="<?php echo base_url();?>login">Login</a></label>
					</div>
			</form>
		</div>
	</div>
</div>
<?php if(form_error('name') == true){ ?>
<script type="text/javascript">
speechSynthesis.speak(new SpeechSynthesisUtterance("<?php echo form_error('name') ?>"));
<?php } ?>
</script>
</body>
</html>