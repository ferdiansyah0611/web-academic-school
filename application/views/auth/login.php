<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $title;?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
<link href="<?php echo base_url();?>assets/css/hfwiufbwe843dhf.css" rel="stylesheet"></head>
<body>
<div class="container content">
	<div class="offset-md-2 offset-sm-1 offset-lg-3" style="margin-top: 80px !important;margin-bottom: 80px !important;">
		<div class="card justify-content-center col-xl-8 mt-3">
			<div class="card-header"><h4 class="card-title">Login account</h4></div>
			<form method="post" action="<?php echo base_url();?>login">
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>" value="<?php echo $this->security->get_csrf_hash(); ?>">
				<div class="card-body">
					<?php
					if (isset($error_form)){
    					echo "<div class='alert alert-warning'>$error_form</div>";
					}
					?>
					<label>NIS atau NIK</label>
					<input type="number" name="nisnik" value="<?php echo set_value('nisnik'); ?>" class="form-control mb-1">
					<?php if(form_error('nisnik') == true){
						echo '<span class="text-danger">'.form_error('nisnik').'</span>';
					};?>
					<label>Password</label>
					<input type="password" name="password" class="form-control mb-1">
					<?php if(form_error('password') == true){
						echo '<span class="text-danger">'.form_error('password').'</span>';
					};?>
				</div>
				<div class="card-footer">
					<input type="submit" name="submit" class="btn btn-info col-5 col-xs-12" value="Masuk">
					<label class="float-right col-7 col-xs-12">Don't have accounts ? <a href="<?php echo base_url();?>register">Register Now</a></label>
				</div>
			</form>
		</div>
	</div>
</div>
<?php if (isset($error_form)){ ?>
<script type="text/javascript">
speechSynthesis.speak(new SpeechSynthesisUtterance("<?php echo $error_form ?>"));
</script>
<?php } ?>
</body>
</html>