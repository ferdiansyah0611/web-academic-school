<sidebar>
<?php 
if($users == false)
{
    echo '<div class="app-sidebar sidebar-shadow">';
}
elseif($users == true)
{
    foreach($users as $us)
    {
        echo '<div class="app-sidebar sidebar-shadow '.$us->sidebar.'">';
    }
}
else{
    echo '<div class="app-sidebar sidebar-shadow">';
}
?>
    <div class="app-header__logo">
        <!-- <div class="logo-src"></div> -->
        <div class="header__pane ml-auto">
            <div>
                <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <div class="app-header__mobile-menu">
        <div>
            <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
        </div>
    </div>
    <div class="app-header__menu">
        <span>
            <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                <span class="btn-icon-wrapper">
                    <i class="fa fa-ellipsis-v fa-w-6"></i>
                </span>
            </button>
        </span>
    </div>
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="app-sidebar__heading">Dashboards</li>
                <li>
                    <a href="<?php echo base_url();?>dashboard">
                        <i class="metismenu-icon pe-7s-graph2"></i>
                        <?php echo $this->lang->line('my_dashboard') ?>
                    </a>
                </li>
                 <li>
                    <a href="#">
                        <i class="metismenu-icon fas fa-newspaper"></i>
                        <?php echo $this->lang->line('article_news') ?>
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url();?>news/create">
                                <i class="metismenu-icon"></i>
                                <?php echo $this->lang->line('create_content') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>news/management">
                                <i class="metismenu-icon"></i>
                                <?php echo $this->lang->line('management') ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon fa fa-unlock-alt"></i>
                        <?php echo $this->lang->line('role_access_account') ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>akademik/manage-class">
                        <i class="metismenu-icon fa fa-server"></i>
                        <?php echo $this->lang->line('management').' '.$this->lang->line('class');?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>akademik/manage-pelajaran">
                        <i class="metismenu-icon fa fa-server"></i>
                        <?php echo $this->lang->line('management').' '.$this->lang->line('study'); ?>
                    </a>
                </li>
                <li class="app-sidebar__heading"><?php echo $this->lang->line('finance') ?></li>
                <li id="menu_keuangan">
                    <a href="#">
                        <i class="metismenu-icon pe-7s-credit"></i>
                        <?php echo $this->lang->line('payment') ?>
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url();?>keuangan/manage-pembayaran">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('management') ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo base_url();?>keuangan/manage-type-pembayaran">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('type') ?> <?php echo $this->lang->line('payment') ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-date"></i>
                        <?php echo $this->lang->line('bill') ?>
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url();?>keuangan/manage-tagihan">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('management') ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="app-sidebar__heading"><?php echo $this->lang->line('account') ?></li>
                <li id="menu_siswa">
                    <a href="#">
                        <i class="metismenu-icon pe-7s-users"></i>
                        <?php echo $this->lang->line('student') ?>
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url();?>akademik/manage-mahasiswa">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('management') ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li id="menu_dosen">
                    <a href="#">
                        <i class="metismenu-icon pe-7s-users"></i>
                        <?php echo $this->lang->line('teacher') ?>
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url();?>akademik/manage-dosen">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('management') ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li id="menu_pengunjung">
                    <a href="#">
                        <i class="metismenu-icon pe-7s-user"></i>
                        <?php echo $this->lang->line('visitor') ?>
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a class="link_route" id="pengunjung_management" data-url="{{url('admin/management/pengunjung')}}" data-link="admin/management/pengunjung" data-title="Manajemen Pengunjung">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('management') ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li id="users_pengunjung">
                    <a href="#">
                        <i class="metismenu-icon pe-7s-user"></i>
                        <?php echo $this->lang->line('users') ?>
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="<?= base_url();?>admin/manage-users">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('management') ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="app-sidebar__heading"><?php echo $this->lang->line('study') ?></li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-news-paper"></i>
                        <?php echo $this->lang->line('subjects') ?>
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url('pelajaran/'.date('l'));?>">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('this_day') ?>
                            </a>
                        </li>
                        <li>
                            <?php
                            if(date('l') == 'Friday' || date('l') == 'Saturday' || date('l') == 'Sunday'){
                                echo "<a href='".base_url('pelajaran/Monday')."'";
                                echo '<i class="metismenu-icon"></i>'.$this->lang->line('tomorrow').'</a>';
                            }
                            else{
                                echo '<a href="'.base_url('pelajaran/'.date('l', strtotime('1 day'))).'"';
                                echo '<i class="metismenu-icon"></i>'.$this->lang->line('tomorrow').'</a>';
                            }
                            ?>
                        </li>
                        <li>
                            <a href="<?php echo base_url('pelajaran/week');?>">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('week') ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-stopwatch"></i>
                        <?php echo $this->lang->line('teaching_study') ?>
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <?php
                            echo "<a href='".base_url('jadwal-mengajar/'.date('l'))."'>".
                                "<i class='metismenu-icon'></i>Hari Ini</a>";
                            ?>
                        </li>
                        <li>
                            <?php
                            if(date('l') == 'Friday' || date('l') == 'Saturday' || date('l') == 'Sunday'){
                                echo "<a href='".base_url('jadwal-mengajar/Monday')."'";
                                echo '<i class="metismenu-icon"></i>'.$this->lang->line('tomorrow').'</a>';
                            }
                            else{
                                echo '<a href="'.base_url('jadwal-mengajar/'.date('l', strtotime('1 day'))).'"';
                                echo '<i class="metismenu-icon"></i>'.$this->lang->line('tomorrow').'</a>';
                            }
                            ?>
                        </li>
                        <li>
                            <a href="<?php echo base_url('jadwal-mengajar/week');?>">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('week') ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="app-sidebar__heading"><?php echo $this->lang->line('student_assessment') ?></li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-note2"></i>
                         <?php echo $this->lang->line('odd_tests') ?>
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('semester_1') ?>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('semester_2') ?>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('input').' '.$this->lang->line('score'); ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-note2"></i>
                         <?php echo $this->lang->line('even_test') ?>
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('semester_1') ?>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('semester_2') ?>
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('input').' '.$this->lang->line('score'); ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-note2"></i>
                        <?php echo $this->lang->line('national_exam') ?>
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="">
                                <i class="metismenu-icon">
                                </i>Hasil Akhir
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="metismenu-icon">
                                </i><?php echo $this->lang->line('tryout_demo') ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#">
                        <i class="metismenu-icon pe-7s-note2"></i>
                        Input Nilai Tugas
                        <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                    </a>
                    <ul>
                        <li>
                            <a href="<?php echo base_url();?>add-task-day">
                                <i class="metismenu-icon">
                                </i>Tugas Harian
                            </a>
                        </li>
                        <li>
                            <a href="">
                                <i class="metismenu-icon">
                                </i>Tugas Deadline
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="app-sidebar__heading"><?php echo $this->lang->line('more') ?></li>
                <li>
                    <a href="">
                        <i class="metismenu-icon fas fa-school"></i><?php echo $this->lang->line('classroom') ?>
                    </a>
                </li>
                <li>
                    <a href="">
                        <i class="metismenu-icon fas fa-book"></i><?php echo $this->lang->line('library') ?>
                    </a>
                </li>
                <li>
                    <a href="<?php echo base_url();?>web/setup">
                        <i class="metismenu-icon fas fa-cogs"></i>Config Website
                    </a>
                </li>
                <li>
                    <a href="javascript:void(0)">
                        <i class="metismenu-icon fas fa-bug"></i>
                        <?php echo $this->lang->line('report') ?> Bugs
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
</sidebar>