<!DOCTYPE html>
<html lang="en" style="overflow-x: hidden;">
<head>
	<link rel="manifest" href="<?php echo base_url();?>assets/manifest.json">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title><?php echo $title;?></title>
    <link rel="shortcut icon" href="<?php echo base_url();?>storage/img/logo.png"/>
    <meta name="<?php echo $this->security->get_csrf_token_name();?>" content="<?php echo $this->security->get_csrf_hash();?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
	<link href="<?php echo base_url();?>assets/css/hfwiufbwe843dhf.css" rel="stylesheet">
	<style type="text/css">
		.app-header__logo .logo-src{
			background: none;
		}
        .dropdown-submenu {
  position: relative;
}
.dropdown-submenu .dropdown-menu {
  top: 0;
  position: fixed;
    left: calc(100% - 640px);
    z-index: 9999;
    padding: 10px;
        width: 400px;
}
	</style>
</head>
<body>
<div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
<?php $this->load->view('layout/navbar');?>
    <div class="app-main">
    <?php $this->load->view('layout/sidebar');;?>
        <div class="app-main__outer">
            <div class="app-main__inner">
                <div class="app-page-title">
                    <div class="page-title-wrapper">
                        <div class="page-title-heading">
                            <div class="page-title-icon" style="padding: 5.4px;">
                                <!-- <i class="pe-7s-car icon-gradient bg-mean-fruit"></i> -->
                                <img width="50px" src="<?php echo base_url();?>storage/img/logo.png">
                            </div>
                            <div><!-- SMK Letris Indonesia 1 -->School Management by Ferdiansyah
                                <div class="page-title-subheading">
                                    <div>
                                        <nav class="" aria-label="breadcrumb">
                                            <ol class="breadcrumb">
                                                <li class="breadcrumb-item linked"><a href="javascript:void(0);"></a></li>
                                                <li class="breadcrumb-item linked" style="display: none;"><a href="javascript:void(0);"></a></li>
                                                <li class="active breadcrumb-item linked" aria-current="page" style="display: none;"></li>
                                            </ol>
                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!--  -->
            <div class="notification-data pl-3 pr-3"></div>
            <div class="row">