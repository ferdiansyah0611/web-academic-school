<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="<?= base_url();?>assets/css/layout.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
</head>
<body>
<nav class="navbar navbar-expand-md navbar-light bg-light mb-4 fixed-top">
	<!-- <a class="navbar-brand" href="#"></a> -->
	<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarCollapse">
		<ul class="navbar-nav mr-auto">
			<div class="menu-content" style="display: inline-flex;">
				
			</div>
		</ul>
		<button class="btn btn-outline-info my-2 my-sm-0" type="button" data-toggle="tooltip" data-placement="buttom" title="Login" onclick="window.location.href='login'"><i class="fas fa-sign-in-alt"></i></button>
	</div>
</nav>
				
<div style="min-height: 660px;text-align: center;" class="parallax-window" data-parallax="scroll" data-image-src="<?= base_url();?>assets/image/walpaper.jpg">
	<p class="animated fadeInLeft delay-2s">
  <span>
    SMK Letris Indonesia 1 
  </span>
</p>
</div>
<div style="min-height: 200px;">
	<div class="card col-xl-9 float-left" style="display: block;">
		<div class="card-body" id="content-news">
			
		</div>
	<div class="card-footer">
		<a href="">Baca Selengkapnya</a>
	</div>
	</div>
	<div class="card col-xl-3 float-left" style="height: 200px;">
	Informasi Sekolah <?php for ($i=0; $i < 2; $i++) { 
		echo date('l');
	} ?>
	</div>
</div>
<div class="col-12 float-left" style="margin-top: 100px;">
	<p class="text-center" style="background: aquamarine;">SMK Letris Indonesia menuju sekolah yang bisa berintegrasi dengan industri. SMK Letris Indonesia merupakan lembaga pendidikan kejuruan yang bertugas meningkatkan Sumber Daya Manusia (SDM) yang sesuai dengan kebutuhan Industri dan Masyarakat.</p>
</div>
<footer>
	
</footer>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script type="text/javascript" src="<?= base_url();?>assets/vendor/jquery-ui/parallax.min.js"></script>
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
<script type="text/javascript">
$(function(){
	function menu()
	{
		$.get("<?= base_url();?>api/menu", function(data) {
			$.each(data, function(index, val) {
				console.log(val);
				if(val.type == 'menu')
				{
					$('.menu-content').append('<li class="nav-item"><div class="dropdown open"><a class="dropdown-toggle nav-link text-light" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">'+val.menu+'</a><div class="dropdown-menu" aria-labelledby="dropdownMenu1"><div class="href-content-'+val.id+'"></div></div></div></li>');
				}
				if(val.type == 'content')
				{

				}
				$.get("<?= base_url();?>api/sub-menu"+'/'+val.id, function(dataget) {
					console.log(dataget);
					$.each(dataget, function(index, val) {
						console.log(val);
						console.log(val.sub_menu);
						$('.href-content-'+val.menu_id+'').append('<a class="dropdown-item" href="#">'+val.sub_menu+'</a>');
					});
				});
			});
		});
	};
	function news()
	{
		setTimeout(function(){
			$.get("<?php echo base_url();?>api/news/page", function(data) {
				console.log(data);
				$.each(data.news, function(index, val) {
					 console.log(val.content);
					 $('#content-news').append('<div class="col-xl-6 float-left card-news">'+
					 	'<h5 class="card-title mb-0"><a href="<?php echo base_url();?>'+val.category+'/'+val.title+'">'+val.title+'</a></h5>'+
					 	'<img class="img-fluid news-img" style="border: 2px solid red;" src="<?php echo base_url('storage/img/');?>'+val.file+'"><label class="date-news">'+val.created_at+'</label></div>');
				});
			});
		}, 2100);
	};
	menu();
	news();
	$(window).scroll(function(){
		var height = $(window).scrollTop();
		if(height > 640){
			$('.navbar').addClass('scroll');
		}
		else{
			$('.navbar').removeClass('scroll');
		}
	});
});
</script>
</body>
</html>