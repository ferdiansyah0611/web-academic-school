<!DOCTYPE html>
<html>
<head>
	<title>Title</title>
	<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
	<style type="text/css">
	@import url('https://fonts.googleapis.com/css2?family=Crete+Round&display=swap');
	@import url('https://fonts.googleapis.com/css2?family=Cuprum&display=swap');
	@import url('https://fonts.googleapis.com/css2?family=Viga&display=swap');
	body{
	    padding:0;
	    margin:0;
	}
	.divider{
	    border-top:1px solid #f5f5f5;
	    padding:0;
	    margin:0;
	    width:100%
	}
	.block{
		display: block;
	}
	.sub-content{
	    background-image: url('https://gstatic.com/classroom/themes/img_cinema.jpg');
    	background-repeat: no-repeat;
    	background-size: 130% 40%;
    	height:280px;
    	border-radius: 10px;
	    margin:1%;
	    width:22.7%;
	    border: 2px solid #e0e0e0;
	}
	.sub-content h5{
		font-size: 20px;
    	color: white;
    	padding: 10px 0 0 17px;
    	margin: 0;
    	text-transform: uppercase;
    	font-family: 'Viga', sans-serif;
	}
	.sub-content p.jurusan{
		margin: 0;
    	padding:10px 0 0 17px;
    	color: white;
    	font-size: 16px;
    	font-family: 'Cuprum', sans-serif;
	}
	.sub-content p{
		margin: 0;
    	color: white;
    	font-size: 18px;
    	padding:10px 0 0 17px;
    	font-family: 'Cuprum', sans-serif;
	}
	.sub-content a{
		    text-decoration: none;
	}
	.sub-content h5:hover{
		    text-decoration: underline;
	}
	.sub-content h5:hover .jurusan{
		    text-decoration: underline;
	}
	.sub-content-footer{
		left: calc(100% - 100.4%);
    position: relative;
    top: calc(100% - 48.8%);
    width: 101%;
    border-top: 2px solid #e0e0e0;
	}
	.sub-content-footer a{
		font-size: 20px;
    	color: black;
    	float: right;
    	padding: 8px;
    	margin-right: 5px;
	}
	.sub-content .href{
		font-size: 20px;
    color: white;
    float: right;
    padding: 20px;
	}
	.profile{
		border-radius: 50%;
		width: 38px;
    	height: 100%;
	}
	#profile{
		position: absolute;
    	right: 0;
    	top: 56px;
    	background: white;
    	padding: 10px;
	}
	/*navbar*/
	nav.navbar-data{
	    position:fixed;
	    z-index:99999;
	    width:100%;
	    display:inline;
	    background:#ffffff;
	    border-bottom: 2px solid #d0d0d0;
	}
	nav.navbar-data i{
	    font-size: 30px;
    	padding: 10px 25px;
    	color: black;
	}
	.content{
	    min-height:200px;
	    position:absolute;
	    margin-top:74px;
	    width:100%;
	    display: flex;
	    flex-wrap: wrap;
	}
	/*menu*/
	.menu-show{
	    display:none;
	    left:-300px;
	    transition:all 2s;
	    overflow:auto;
	    padding:10px;
	    z-index:99999;
	}
	.menu-show img{
	    width:40px;
	    height:40px;
	    border-radius:50%;
	    float:left;
	}
	.menu-show a{
	    padding:10px;
	    width:92%;
	    float:left;
	    text-decoration:none;
	}
	.menu-show a:hover{
	    background: #e2e2e2;
	    border-radius:100px;
	}
	.menu-show span{
	    position:absolute;
	    padding:8px 10px 10px 10px;
	    color:black;
	    font-family: 'Crete Round', serif;
	}
	.menu-left{
	    float:right;
	    padding-right:20px;
	    display: flex;
	}
	.menu-left a{
	    float:right;
	    padding:17px 20px;
	    text-decoration:none;
	    font-family: 'Crete Round', serif;
	    color: black;
	}
	.menu-left a:hover{
		background: aliceblue;
	}
	.menu-left .active{
		border-bottom: 4px solid blue;
	}
	.icon-plus{
		font-size: 40px;
		padding-top: 15px;
		color: #ffffff
	}
	</style>
</head>
<body>
<nav class="navbar-data">
    <a href="javascript:void(0)" class="menu-toggle"><i class="fas fa-bars"></i></a>
    <div class="menu-left">
        <a href="javascipt:void(0);">Forum</a>
        <a href="javascipt:void(0);">Tugas Kelas</a>
        <a href="javascipt:void(0);">Anggota</a>
        <div id="profile"><img class="profile" src="<?php echo base_url('storage/img/'.$this->session->userdata('avatar')); ?>"></div>
    </div>
    <div class="menu-right">
        <div class="menu-show">
            <div class="divider">
            <h3>Terdaftar</h3>
            </div>
            <a href="javascipt:void(0);"><img src="https://gstatic.com/classroom/themes/Chemistry.jpg" alt=""><span>Ferdiansyah</span></a>
            <a href="javascipt:void(0);"><img src="https://gstatic.com/classroom/themes/Chemistry.jpg" alt=""><span>Ferdiansyah</span></a>
            <a href="javascipt:void(0);"><img src="https://gstatic.com/classroom/themes/Chemistry.jpg" alt=""><span>Ferdiansyah</span></a>
            <a href="javascipt:void(0);"><img src="https://gstatic.com/classroom/themes/Chemistry.jpg" alt=""><span>Ferdiansyah</span></a>
            <a href="javascipt:void(0);"><img src="https://gstatic.com/classroom/themes/Chemistry.jpg" alt=""><span>Ferdiansyah</span></a>
            <a href="javascipt:void(0);"><img src="https://gstatic.com/classroom/themes/Chemistry.jpg" alt=""><span>Ferdiansyah</span></a>
            <a href="javascipt:void(0);"><img src="https://gstatic.com/classroom/themes/Chemistry.jpg" alt=""><span>Ferdiansyah</span></a>
            <a href="javascipt:void(0);"><img src="https://gstatic.com/classroom/themes/Chemistry.jpg" alt=""><span>Ferdiansyah</span></a>
            <a href="javascipt:void(0);"><img src="https://gstatic.com/classroom/themes/Chemistry.jpg" alt=""><span>Ferdiansyah</span></a>
            <a href="javascipt:void(0);"><img src="https://gstatic.com/classroom/themes/Chemistry.jpg" alt=""><span>Ferdiansyah</span></a>
        </div>
        </div>
</nav>
<a href="javascript:void(0)" class="plus-menu"><i class="fas fa-plus icon-plus"></i></a>
<div class="content">
    <div class="sub-content">
    	<a href="javascipt:void(0);" class="href hrefs" lv-target="drop1"><i class="fas fa-ellipsis-v"></i></a>
    	<div class="drop-down" id="drop1" style="display: none;">
    		<ul>
    			<li>link 1</li>
    			<li>link 1</li>
    			<li>link 1</li>
    			<li>link 1</li>
    		</ul>
    	</div>
    	<a href="">
    		<h5 class="sub-content-h5">Jurusan</h5>
    		<p class="jurusan">Pelajaran</p>
    		<p>Nama</p>
    	</a>
    	<div class="sub-content-footer">
    		<a href="javascipt:void(0);"><i class="fas fa-tasks"></i></a>
    	</div>
    </div>
    <div class="sub-content">
    	<a href="javascipt:void(0);" class="href hrefs" lv-target="drop2"><i class="fas fa-ellipsis-v"></i></a>
    	<div class="drop-down" id="drop2" style="display: none;">
    		<ul>
    			<li>link 1</li>
    			<li>link 1</li>
    			<li>link 1</li>
    			<li>link 1</li>
    		</ul>
    	</div>
    	<a href="">
    		<h5 class="sub-content-h5">Jurusan</h5>
    		<p class="jurusan">Pelajaran</p>
    		<p>Nama</p>
    	</a>
    	<div class="sub-content-footer">
    		<a href="javascipt:void(0);"><i class="fas fa-tasks"></i></a>
    	</div>
    </div>
    <div class="sub-content"></div>
    <div class="sub-content"></div>
    <div class="sub-content"></div>
    <div class="sub-content"></div>
    <div class="sub-content"></div>
    <div class="sub-content"></div>
    <div class="sub-content"></div>
    <div class="sub-content"></div>
</div>
<?php echo $jquery ?>
<script type="text/javascript">
function menu()
{
    var show = document.getElementsByClassName('menu-show')[0];
    var plus = document.getElementsByClassName('plus-menu')[0];
    var drop_click = document.getElementsByClassName('hrefs');
    var drop_show = document.getElementsByClassName('drop-show');
    var sub_content_h5 = document.getElementsByClassName('sub-content-h5')[0];
    /*MENU EVENTS*/
    document.getElementsByClassName('menu-toggle')[0].addEventListener('click', function(){
        show.style.display = 'block';
        show.style.position = 'fixed';
        show.style.marginLeft = '300px';
        show.style.top = '0px';
        show.style.padding = '10px';
        show.style.width = '300px';
        show.style.height = '100%';
        show.style.backgroundColor = 'whitesmoke';
        show.style.transition = '20s all ease-in-out';
    });
    document.getElementsByClassName('content')[0].addEventListener('click', function(){
        show.style.display = 'none';
    });
    /*MENU PLUS*/
    plus.style.position = 'fixed';
    plus.style.bottom = '70px';
    plus.style.right = '70px';
    plus.style.zIndex = '999999';
    plus.style.backgroundColor = 'blue';
    plus.style.width = '70px';
    plus.style.height = '70px';
    plus.style.borderRadius = '50%';
    plus.style.textAlign = 'center';
    sub_content_h5.addEventListener('hover', function(){
    	document.getElementsByClassName('jurusan')[0].style.textDecoration = 'underline';
    });
    for (var i = 0; i < drop_click.length; i++) {
    	drop_click[i].addEventListener('click', function(){
    		var th = this;
    		var data = this.getAttribute('lv-target');
    		document.getElementById(data).style.display = 'block';
    		document.getElementById(data).classList.add('drop-show');
    		document.getElementById(data).classList.add(data);
    		this.classList.add('drop-show');
    		setTimeout(function(){
    			th.classList.remove('hrefs');
    		});
    	});
    }
    for (var i = 0; i < drop_show.length; i++) {
    	drop_show[i].addEventListener('click', function(){
    		var data = this.classList.split(' ').pop();
    		console.log(this.classList.split(' ').pop());
    		document.getElementById(data).style.display = 'none';
    	});
    }
};
function url()
{

}
menu();
url();
</script>
</body>
</html>