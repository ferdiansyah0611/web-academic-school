<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/materialize/css/materialize.min.css">
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css"> -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/classroom.css">
	<style type="text/css">
	@font-face {
	  font-family: 'Material Icons';
	  font-style: normal;
	  font-weight: 400;
	  src: url('<?php echo base_url(); ?>assets/css/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2') format('woff2');
	}
	
	.material-icons {
	  font-family: 'Material Icons';
	  font-weight: normal;
	  font-style: normal;
	  font-size: 24px;
	  line-height: 1;
	  letter-spacing: normal;
	  text-transform: none;
	  display: inline-block;
	  white-space: nowrap;
	  word-wrap: normal;
	  direction: ltr;
	  -webkit-font-smoothing: antialiased;
	}
	</style>
</head>
<body>
<div class="navbar-fixed">
<nav class="white">
	<div class="nav-wrapper">
      	<a href="#" data-target="slide-out" class="sidenav-trigger black-text" style="display: block;">
      		<i class="material-icons">menu</i></a>
      	<a class="btn-floating pulse">
      		<i class="material-icons" style="margin-top: -10px;">notifications</i>
      	</a>
      	<ul id="nav-mobile" class="right hide-on-med-and-down">
        	<li><a href="" class="black-text">Forum</a></li>
        	<li><a href="" class="black-text">Tugas Kelas</a></li>
        	<li><a href="" class="black-text">Anggota</a></li>
      	</ul>
    </div>
</nav>
</div>
<ul id="slide-out" class="sidenav">
    <li>
    	<div class="user-view">
     		<div class="background">
        		<img src="https://gstatic.com/classroom/themes/Chemistry.jpg">
      		</div>
      		<a href="#user"><img class="circle" src="<?php 
      		if($this->session->userdata('avatar') !== null){echo base_url('storage/img/'.$this->session->userdata('avatar'));}else{echo base_url('storage/img/user-default.png');} ?>"></a>
      		<a href="#name"><span class="white-text name"><?php echo $this->session->userdata('name'); ?></span></a>
      		<a href="#email"><span class="white-text email"><?php echo $this->session->userdata('nisnik'); ?></span></a>
    	</div>
    </li>
    <li><a href="#!"><i class="material-icons">date_range</i>Kalender</a></li>
    <li><a href="#!"><i class="material-icons">home</i>Classes</a></li>
    <li><div class="divider"></div></li>
    
    <div id="registered">
    	<li><a class="subheader">Terdaftar</a></li>
    </div>
    <li><div class="divider"></div></li>
    <li><a class="subheader">More</a></li>
    <li><a class="waves-effect" href="#!"><i class="material-icons">brightness_7</i>Setting</a></li>
    <li><a class="waves-effect" href="#!"><i class="material-icons">power_settings_new</i>Logout</a></li>
</ul>


<div class="content">
 </div>
 <!-- FOR MAHASISWA -->
<!--  <div class="container">
 	<div class="img-data-users">
 		<h5 class="title">XI Teknik komputer</h5>
 		<p class="subtitle">Pelajaran</p>
 		<ul class="collapsible">
    		<li>
    	  	<div class="collapsible-header"><i class="material-icons">expand_less</i></div>
    	  		<div class="collapsible-body">
    	  			<p><b>Mata pelajaran</b> Sistem Hukum dan Peradilan Internasional</p>
    	  			<p><b>Ruang</b> TKJ</p>
    	  		</div>
    		</li>
  		</ul>
	</div>
	<div class="row" style="margin-top: 30px;">
		<div class="col s12 m12 l2 xl3 new-task">
		<p><b>Mendatang</b></p>
		<p class="nothing">Hore, tidak ada tugas yang perlu segera diselesaikan!</p>
	</div>
	<div class="col s12 m12 l10 xl8 offset-xl1 share">
		<div>
			<div class="share-text">
				<img src="<?php echo base_url('storage/img/user-default.png'); ?>" style="border-radius: 50%;width: 40px;">
				<p>Bagikan sesuatu dengan kelas Anda…</p>
			</div>
			<form id="share-form" enctype="multipart/form-data">
				<?php echo $csrf; ?>
				<div class="input-field col s12">
          			<textarea name="content" id="textarea1" class="materialize-textarea"></textarea>
          			<label for="textarea1">Bagikan dengan kelas</label>
          			<div class="allinput">
          			</div>
          			<a class="waves-effect dropdown-trigger waves-light btn" data-target='insertfile'><i class="material-icons left">insert_link</i>Tambahkan</a>
          			<ul id='insertfile' class='dropdown-content'>
    					<li><a href="#modalinsertlink" class="modal-trigger"><i class="material-icons">link</i>Link</a></li>
    					<li>
    						<div class="file-field input-field">
      							<div class="btn" style="background: none;color: #26a69a;">
        							<span><i class="material-icons">attach_file</i>File</span>
        							<input type="file" name="filedata[]" multiple="">
      							</div>
      							<div class="file-path-wrapper">
      							  	<input class="file-path validate" type="text">
      							</div>
    						</div>
    					</li>
    					<li><a href="#!"><i class="material-icons">video_library</i>Youtube</a></li>
  					</ul>
          			<div class="right">
          				<a class="btn share-form-cancel">Batal</a>
          				<button type="submit" class="waves-effect waves-light btn" style="background-color: #b3b3b3;">Posting</button>
          			</div>
        		</div>
			</form>
		</div>
	</div>
	</div>
 </div>
 <div id="modalinsertfile" class="modal">
 	<div class="modal-content">
 		<h4>Modal Header</h4>
 		<p>A bunch of text</p>
 	</div>
 	<div class="modal-footer">
 		<a href="#" class="waves-effect waves-green btn-flat modal-action modal-close">Agree</a>
 	</div>
 </div>
  <div id="modalinsertlink" class="modal" style="width: 25%;">
    <div class="modal-content">
      <h4>Tambahkan link</h4>
      <div class="input-field col s6">
          <input id="linkinput"type="text"class="validate">
          <label for="first_name">Link</label>
        </div>
    </div>
    <div class="modal-footer">
      <button id="btninsertlink1" class="modal-close waves-effect waves-blue btn-flat">Tambahkan link</button>
    </div>
  </div> -->
 <!-- END FOR MAHASISWA -->
<!-- <script type="text/javascript" src="<?php echo base_url();?>assets/vendor/jquery-ui/jquery.js"></script> -->
<?php echo $jquery ?>
<!-- <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/livereload-js@3.2.2/dist/livereload.min.js"></script> -->
<!-- <script src="https://cdn.totaljs.com/livereload.js"></script> -->
<script type="text/javascript" src="<?php echo base_url();?>assets/vendor/materialize/js/materialize.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script> -->
<script type="text/javascript">
$(document).ready(function(){
	window.addEventListener('load', (event) => {
  		if(window.location.hash){
  			var sl = window.location.hash.split('/');
  			viewclass(atob(sl[2]));
  		}
  		else{console.log('page is fully loaded');}
	});

	$('.dropdown-menu').css('display','none');
	$('.dropdown').hover(function() {
  		$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
	}, function() {
  		$(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
	});
	function init()
	{
		$('.sidenav').sidenav();
    	$('.dropdown-trigger').dropdown();
    	$('div.sub-content').find('a.dropdown-custom').dropdown();
    	$('.tooltipped').tooltip();
    	$('.collapsible').collapsible();
    	$('.modal').modal();
	};
	function updatepost()
	{

	}
	function update()
	{
		if(window.location.hash){
		$.get("<?php echo base_url();?>classrooms/api/class", function(data) {
			$('#registered').empty();
			$('#registered').append('<li><a class="subheader">Terdaftar</a></li>');
			$.each(JSON.parse(data), function(index, val) {
				$('#registered').css('display','block');
				$('#registered').append('<li><a class="lv-route-class" data-id="'+val.id+'" data-title="'+val.pelajaran+'" href="javascript:void(0)"><i class="material-icons">date_range</i>'+val.pelajaran+'</a></li>');
			});
		});
		}
		else{
		$('#registered').css('display','none');
		$('#registered').empty();
		setTimeout(function(){
			$.get("<?php echo base_url();?>classrooms/api/class", function(data) {
				$('#registered').append('<li><a class="subheader">Terdaftar</a></li>');
				$.each(JSON.parse(data), function(index, val) {
					$('#registered').css('display','block');
					$('#registered').append('<li><a class="lv-route-class" data-id="'+val.id+'" data-title="'+val.pelajaran+'" href="javascript:void(0)"><i class="material-icons">date_range</i>'+val.pelajaran+'</a></li>');
					if(val.avatar == null)
					{
						$('.content').append('<div class="sub-content"><a class="dropdown-custom href" href="#" data-target="dropdown-'+val.name+'"><i class="material-icons">more_vert</i></a><a href="javascipt:void(0)" class="lv-route-class" data-id="'+val.pelajaran_id+'" data-title="'+val.pelajaran+'"><h5 class="sub-content-h5">'+val.kelas+' '+val.jurusan+'</h5><p class="jurusan">'+val.pelajaran+'</p><p>'+val.name+' '+val.gelar+'</p></a><img src="<?php echo base_url('storage/img/user-default.png'); ?>" class="profile"><div class="sub-content-footer"><a href="javascipt:void(0);" class="tooltipped" data-position="bottom" data-tooltip="Buka tugas untuk kelas jurusan"><i class="material-icons">assignment_ind</i></a></div><ul id="dropdown-'+val.name+'" class="dropdown-content"><li><a href="#!">Pindahkan</a></li><li><a href="#!">Batalkan Pendaftaran</a></li><li class="divider" tabindex="-1"></li><li><a href="#!">Laporkan Penyalahgunaan</a></li></ul></div>');
					}
					else if(val.avatar != null)
					{
						$('.content').append('<div class="sub-content"><a class="dropdown-custom href" href="#" data-target="dropdown-'+val.name+'"><i class="material-icons">more_vert</i></a><a href="javascipt:void(0)" class="lv-route-class" data-id="'+val.pelajaran_id+'" data-title="'+val.pelajaran+'"><h5 class="sub-content-h5">'+val.kelas+' '+val.jurusan+'</h5><p class="jurusan">'+val.pelajaran+'</p><p>'+val.name+' '+val.gelar+'</p></a><img src="'+window.location.origin+'/storage/img/'+val.avatar+'" class="profile"><div class="sub-content-footer"><a href="javascipt:void(0);" class="tooltipped" data-position="bottom" data-tooltip="Buka tugas untuk kelas jurusan"><i class="material-icons">assignment_ind</i></a></div><ul id="dropdown-'+val.name+'" class="dropdown-content"><li><a href="#!">Pindahkan</a></li><li><a href="#!">Batalkan Pendaftaran</a></li><li class="divider" tabindex="-1"></li><li><a href="#!">Laporkan Penyalahgunaan</a></li></ul></div>');
					}

					$('div.sub-content').find('a.dropdown-custom').dropdown();
					$('.tooltipped').tooltip();
				});
			});
		},400);
		}/*else*/
	};
	function click()
	{
		$('body').on('click', '.share-text',function(){
			$('body').find('#share-form').stop(true, true).delay(200).fadeIn(500);
			$('body').find('.share-text').stop(true, true).delay(100).fadeOut(300);
			$('body').find('.share').css('min-height', '220px');
		});
		$('body').on('click', '.share-form-cancel',function(){
			$('body').find('#share-form').stop(true, true).delay(500).fadeOut(600);
			$('body').find('#share-form').css('display', 'none');
			$('body').find('.share-text').css('display', 'flex');
			$('body').find('.share').css('min-height', '0px');
		});
	};
	/*VIEW CLASS*/
	function viewclass(pelajaran_id)
	{
		$('.content').empty();

		var nav = '<div class="mobile-view-class"><a href=""><i class="material-icons">message</i></a><a href=""><i class="material-icons">assignment</i></a><a href=""><i class="material-icons">people</i></a></div>';
		$('body').append(nav);
		update();
		$.get("<?php echo base_url(); ?>classrooms/api/class/"+pelajaran_id, function(data) {
			console.log(data);
			$.each(JSON.parse(data), function(index, val) {
				/*POST*/
				$.get("<?php echo base_url(); ?>classrooms/api/post/"+val.class_id, function(data) {
					$.each(JSON.parse(data), function(index, val) {
						console.log(val);
						const monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
						var post = '<div style="display: contents;padding: 0;margin: 0;"><div class="head-postdata"><img src="<?php echo base_url('storage/img/user-default.png'); ?>"><p class="namedata">'+val.name+'</p><p class="datedata">'+new Date(val.created_at).getDay()+' '+monthNames[new Date(val.created_at).getMonth()]+'</p><p>'+val.content+'</p></div><div class="divider"></div><form class="form-comment-input"><img src="<?php echo base_url('storage/img/user-default.png'); ?>"><input class="comment-input" type="text" placeholder="Tambahkan komentar kelas"><button type="submit"><i class="material-icons prefix">send</i></button></form><div class="comment-info">Add comment to class</div></div>';
						$('body').find('.postdata').append(post);
					});
				});
				/*APP*/
				var app = ' <div class="container">'+
 			'<div class="img-data-users">'+
' 				<h5 class="title">'+val.kelas+' '+val.jurusan+'</h5>'+
' 				<p class="subtitle">'+val.pelajaran+'</p>'+
' 				<ul class="collapsible">'+
'    				<li>'+
'    			  	<div class="collapsible-header"><i class="material-icons">expand_less</i></div>'+
'    			  		<div class="collapsible-body">'+
'    			  			<p><b>Mata pelajaran</b> Sistem Hukum dan Peradilan Internasional</p>'+
'    			  			<p><b>Ruang</b> TKJ</p>'+
'    			  		</div>'+
'    				</li>'+
'  				</ul>'+
'			</div>'+
'			<div class="row">'+
'				<div class="col s12 m12 l12 xl3 new-task">'+
'				<p><b>Mendatang</b></p>'+
'				<p class="nothing">Hore, tidak ada tugas yang perlu segera diselesaikan!</p>'+
'			</div>'+
'			<div class="col s12 m12 l12 xl8 offset-xl1 share">'+
'				<div>'+
'					<div class="share-text">'+
'						<img src="<?php echo base_url('storage/img/user-default.png'); ?>">'+
'						<p>Bagikan sesuatu dengan kelas Anda…</p>'+
'					</div>'+
'					<form id="share-form" enctype="multipart/form-data">'+
'						<?php echo $csrf; ?>'+
'						<input type="hidden" value="'+val.class_id+'" name="class">'+
'						<div class="input-field col s12">'+
'          					<textarea name="content" id="textarea1" class="materialize-textarea"></textarea>'+
'          					<label for="textarea1">Bagikan dengan kelas</label>'+
'          					<div class="allinput">'+
'          					</div>'+
'          					<a class="waves-effect dropdown-trigger waves-light btn" data-target="insertfile"><i class="material-icons left">insert_link</i>Tambahkan</a>'+
'          					<ul id="insertfile" class="dropdown-content">'+
'    							<li><a href="#modalinsertlink" class="modal-trigger"><i class="material-icons">link</i>Link</a></li>'+
'    							<li>'+
'    								<div class="file-field input-field">'+
'      									<div class="btn" style="background: none;color: #26a69a;">'+
'        									<span><i class="material-icons">attach_file</i>File</span>'+
'        									<input type="file" name="filedata[]" multiple="">'+
'      									</div>'+
'      									<div class="file-path-wrapper">'+
'      									  	<input class="file-path validate" type="text">'+
'      									</div>'+
'    								</div>'+
'    							</li>'+
'    							<li><a href="#!"><i class="material-icons">video_library</i>Youtube</a></li>'+
'  							</ul>'+
'          					<div class="right">'+
'          						<a class="btn share-form-cancel">Batal</a>'+
'          						<button type="submit" class="waves-effect waves-light btn" style="background-color: #b3b3b3;">Posting</button>'+
'          					</div>'+
'        				</div>'+
'					</form>'+
'				</div>'+/*div*/
'			</div>'+/*col s12 m12 l10 xl8 offset-xl1 share*/
'			<div class="col s12 m12 l12 xl8 offset-xl4 postdata">'+
'			</div>'+
'		</div>'+
' 	</div>'+
' 	<div id="modalinsertfile" class="modal">'+
' 		<div class="modal-content">'+
' 			<h4>Modal Header</h4>'+
' 			<p>A bunch of text</p>'+
' 		</div>'+
' 		<div class="modal-footer">'+
' 			<a href="#" class="waves-effect waves-green btn-flat modal-action modal-close">Agree</a>'+
' 		</div>'+
' 	</div>'+
'  	<div id="modalinsertlink" class="modal" style="width: 25%;">'+
'  	  <div class="modal-content">'+
'  	    <h4>Tambahkan link</h4>'+
'  	    <div class="input-field col s6">'+
'  	        <input id="linkinput"type="text"class="validate">'+
'  	        <label for="first_name">Link</label>'+
'  	      </div>'+
'  	  </div>'+
'  	  <div class="modal-footer">'+
'  	    <button id="btninsertlink1" class="modal-close waves-effect waves-blue btn-flat">Tambahkan link</button>'+
'  	  </div>'+
  	'</div>';
  		$('.content').append(app);
  		$('.sidenav').sidenav();
    	$('.dropdown-trigger').dropdown();
    	$('div.sub-content').find('a.dropdown-custom').dropdown();
    	$('.tooltipped').tooltip();
    	$('.collapsible').collapsible();
    	$('.modal').modal();
			});/*each*/
		});/*get*/
	}
	/*ROUTE*/
	function url()
	{
		$('body').on('click', '.lv-route-class', function(){
			var id = $(this).data('id');
			var title = $(this).data('title');
			window.history.replaceState({page_id: 1},'as','/classroom/#/me/'+btoa(id));
			viewclass(id);
			window.onpopstate = function () {
        		history.go(1);
    		};
		});
	};
	function readURL(input) {
  		if (input.files && input.files[0]) {
    		var reader = new FileReader();
    		reader.onload = function(e) {
      			$('#blah').attr('src', e.target.result);
      			console.log(e.target.result);
    		};
    		reader.readAsDataURL(input.files[0]);
  		};
	};
	/*INSERT DATA IN VIEW CLASS*/
	function insert()
	{
		$('body').on('click', '#btninsertlink1', function(){
			var link = $('#linkinput').val();
			var random = Math.floor(Math.LN10 * 999 * Math.random());
			var dt = '<div class="link" id="'+random+'"><input type="hidden" name="link[]" value="'+link+'"><p>'+link+'</p><i class="material-icons closelink" data-id="'+random+'">close</i></div>';
			$('.allinput').append(dt);
		});
		$('body').on('click', '.closelink', function(){
			var id = $(this).data('id');
			$('body').find('#'+id).remove();
		});
		$('input[type="file"]').change(function(e){
            for (var i = this.files.length - 1; i >= 0; i--) {
            	var name = this.files[i].name;
            	var random = Math.floor(Math.LN10 * 999 * Math.random());
            	var reader = new FileReader();
            	readURL(this);
            	if(this.files){
            		reader.onload = function(e) {
      					console.log(e.target.result);
    				};
            	}
            	var dt = '<div class="link" id="'+random+'"><input type="hidden" name="link[]" value="'+name+'"><p>'+name+'</p><i class="material-icons closelink" data-id="'+random+'">close</i></div>';
            	$(dt).appendTo('.allinput');
            }
        });
        $('body').on('submit', '#share-form', function(e){
        	e.preventDefault();
        	$.ajax({
        		xhr: function() {
        		var xhr = new window.XMLHttpRequest();
		        	xhr.upload.addEventListener("progress", function(evt){
		        	    if (evt.lengthComputable) {
		        	        var percentComplete = evt.loaded / evt.total;
		        	        console.log(percentComplete);
		        	    }
		       		}, false);
		       		xhr.addEventListener("progress", function(evt){
		        	   if (evt.lengthComputable) {
		        	       var percentComplete = evt.loaded / evt.total;
		        	       console.log(percentComplete);
		        	   }
		       		}, false);
       			return xhr;
    			},
        		url: '<?php echo base_url();?>classroom/upload-share',
        		type:'post',
        		processData:false,
        		contentType:false,
        		data: new FormData(this),
        		success:function(){
        			$('.allinput').empty();
        			$('body').find('#share-form').trigger('reset');
        			new Notification('Data add successfully');
        		},
        		error:function(e){
        			console.log(e);
        		}
        	});
        });
	};
	function notifyMe() {
  		if (!("Notification" in window)) {
    		console.log("This browser does not support desktop notification");
  		}
  		else if (Notification.permission === "granted") {
    		var notification = new Notification("Hi there!");
  		}
  		else if (Notification.permission !== 'denied' || Notification.permission === "default") {
    		Notification.requestPermission(function (permission) {
    	  		if (permission === "granted") {
    	    		var notification = new Notification("Hi there!");
    	  		}
    		});
  		}
	};
	url();
    init();
    update();
    click();
    insert();
});
</script>
</body>
</html>