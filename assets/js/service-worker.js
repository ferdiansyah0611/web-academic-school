const name = "cache-required"
var cached = [
    "/assets/css/hfwiufbwe843dhf.css",
    "/assets/js/hfwiufbwe843dhf.js",
    "/assets/vendor/jquery-ui/parallax.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js",
    "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css",
    "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css",
    "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
];

self.addEventListener("install", function(event){
    event.waitUntil(
        caches.open(name).then(function (cache) {
            return cache.addAll(cached)
        })
    );
});