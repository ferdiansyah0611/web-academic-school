CREATE TABLE clr_post(
	id bigint(255) UNSIGNED NOT NULL,
	class_id bigint(255) NOT NULL,
	user_id bigint(255) NOT NULL,
	title varchar(255) NOT NULL,
	content longtext NOT NULL,
	created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id)
);