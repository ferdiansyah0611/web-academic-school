CREATE TABLE news (
    id bigInt(255) UNSIGNED NOT NULL UNIQUE,
    user_id bigInt(255) UNSIGNED NOT NULL,
    title varchar(255) NOT NULL,
    content LONGTEXT NOT NULL,
    status varchar(255) NOT NULL,
    category varchar(255) NOT NULL,
    file varchar(255) NULL,
    category varchar(255) NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id)
);