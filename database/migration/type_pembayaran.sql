CREATE TABLE type_pembayaran (
    id bigInt(255) UNSIGNED NOT NULL UNIQUE,
    class_id varchar(255) NOT NULL,/*number or all*/
    type varchar(255) NOT NULL,/*nama*/
    price bigInt(255) NOT NULL,
    end_date date NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id)
);
/*User_id Harga Pembayaran TGL*/