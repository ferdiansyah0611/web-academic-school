CREATE TABLE layout (
    id bigInt(255) UNSIGNED NOT NULL UNIQUE,
    user_id bigInt(255) UNSIGNED NOT NULL UNIQUE,
    nav varchar(255) NULL,
    sidebar varchar(255) NULL,
    background varchar(255) NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id)
);