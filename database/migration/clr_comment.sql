CREATE TABLE clr_comment(
	id bigint(255) UNSIGNED NOT NULL,
	clr_post_id bigint(255) NOT NULL,
	user_id bigint(255) NOT NULL,
	comment varchar(255) NOT NULL,
	size float NOT NULL,
	created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id)
);