CREATE TABLE clr_file(
	id bigint(255) UNSIGNED NOT NULL,
	clr_post_id bigint(255) NOT NULL,
	name varchar(255) NOT NULL,
	type varchar(255) NOT NULL,
	size float NOT NULL,
	created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id)
);