CREATE TABLE class (
    id bigInt NOT NULL UNIQUE,
    kelas varchar(255) NOT NULL,
    jurusan varchar(255) NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id)
);