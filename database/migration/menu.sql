CREATE TABLE menu (
    id bigInt(255) UNSIGNED NOT NULL UNIQUE,
    menu varchar(255) NOT NULL,
    type varchar(255) NULL, /*menu or content*/
    content LONGTEXT NULL,/*if content*/
    link varchar(255) NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id)
);