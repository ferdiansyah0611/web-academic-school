CREATE TABLE mahasiswa (
    id bigInt(255) UNSIGNED NOT NULL UNIQUE,
    user_id bigInt(255) UNSIGNED NOT NULL UNIQUE,
    class_id bigInt(255) NOT NULL,
    asal_sekolah varchar(255) NOT NULL,
    kelas varchar(255) NOT NULL,
    phone bigInt(255) NOT NULL,
    father varchar(255) NOT NULL,
    mother varchar(255) NOT NULL,
    no_father varchar(255) NOT NULL,
    no_mother varchar(255) NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id)
);