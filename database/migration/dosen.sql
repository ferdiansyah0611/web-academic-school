CREATE TABLE dosen (
    id bigInt(255) UNSIGNED NOT NULL UNIQUE,
    user_id bigInt(255) UNSIGNED NOT NULL UNIQUE,
    gelar varchar(255) NOT NULL,
    universitas varchar(255) NOT NULL,
    phone bigInt(255) NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id)
);