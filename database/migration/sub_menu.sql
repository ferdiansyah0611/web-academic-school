CREATE TABLE sub_menu (
    id bigInt(255) UNSIGNED NOT NULL UNIQUE,
    menu_id bigInt(255) UNSIGNED NOT NULL,
    sub_menu VARCHAR(255) NOT NULL,
    type varchar(255) NULL, /*menu or content*/
    content VARCHAR(255) NULL,
    link varchar(255) NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id)
);