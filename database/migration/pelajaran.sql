CREATE TABLE pelajaran (
    id bigInt NOT NULL UNIQUE,
    class_id bigInt(255) NOT NULL UNIQUE,
    dosen_id bigInt(255) NOT NULL UNIQUE,
    pelajaran varchar(255) NOT NULL,
    hari varchar(255) NOT NULL,
    mulai varchar(255) NOT NULL,
    akhir varchar(255) NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id)
);