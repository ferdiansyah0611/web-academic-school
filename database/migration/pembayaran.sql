CREATE TABLE pembayaran (
    id bigInt(255) UNSIGNED NOT NULL UNIQUE,
    mahasiswa_id varchar(255) NOT NULL,
    type_pembayaran_id varchar(255) NOT NULL,
    tgl_pembayaran varchar(255) NOT NULL,/*nama*/
    price varchar(255) NOT NULL,
    month varchar(255) NOT NULL,
    status varchar(255) NOT NULL,/*lunas or*/
    tahun_ajaran varchar(255) NULL,/*month evvent*/
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id)
);
/*nama type total price tgl_pembayaran status end_date*/