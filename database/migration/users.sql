CREATE TABLE users (
    id bigInt NOT NULL UNIQUE,
    name varchar(255) NOT NULL,
    email varchar(255) NOT NULL,
    password varchar(255) NOT NULL,
    role varchar(255) NOT NULL,
    nisnik varchar(255) NOT NULL,
    locate varchar(255) NOT NULL,
    born date NOT NULL,
    avatar varchar(255) NOT NULL,
    active datetime NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id)
);