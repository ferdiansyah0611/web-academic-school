CREATE TABLE activity (
    id bigInt NOT NULL UNIQUE,
    user_id bigInt NOT NULL,
    activity varchar(255) NOT NULL,
    created_at datetime NOT NULL,
    updated_at datetime NOT NULL,
    PRIMARY KEY (id)
);